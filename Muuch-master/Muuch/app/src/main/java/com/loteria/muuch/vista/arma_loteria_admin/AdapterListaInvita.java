package com.loteria.muuch.vista.arma_loteria_admin;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.ingreso.AvatarAdapter;

import java.util.ArrayList;

/**
 * Created by hgome on 06/11/2017.
 */

public class AdapterListaInvita extends BaseAdapter {

    private Context context;
    private ArrayList<Usuario> listaUsuarios;

    public AdapterListaInvita(Context context, ArrayList<Usuario> listaUsuarios){
        this.context=context;
        this.listaUsuarios = listaUsuarios;

    }

    @Override
    public int getCount() {
        return listaUsuarios.size();
    }

    @Override
    public Object getItem(int i) {
        return listaUsuarios.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Usuario actual = listaUsuarios.get(i);
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.adapter_amigos_armaloteria, null);
        TextView nombre = (TextView) v.findViewById(R.id.nombre_amigo);
        nombre.setText(actual.getNombre());
        ImageView imageView = (ImageView) v.findViewById(R.id.avatar_amigo);
        //imageView.setImageResource(actual.getAvatar());
        imageView.setImageResource(AvatarAdapter.getAvatar(actual));
        return v;
    }
}
