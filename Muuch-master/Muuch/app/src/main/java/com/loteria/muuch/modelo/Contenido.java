package com.loteria.muuch.modelo;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.Toast;

import com.loteria.muuch.R;

import java.io.File;
import java.io.Serializable;

import static com.loteria.muuch.modelo.Persistencia.cacheFile;
import static com.loteria.muuch.modelo.Persistencia.copiar;
import static com.loteria.muuch.modelo.Persistencia.descargarArchivo;
import static com.loteria.muuch.modelo.Persistencia.getBitMap;
import static com.loteria.muuch.modelo.Persistencia.getResponse;
import static com.loteria.muuch.modelo.Persistencia.internalFile;
import static com.loteria.muuch.modelo.Persistencia.renombrar;
import static com.loteria.muuch.modelo.Persistencia.subirArchivos;

/**
 * @author Luis Angel Patlani Aguilar
 * Clase que representa el contenido de una Tarjeta en la Base de Datos de la aplicación.
 */
public class Contenido implements Serializable{

    /** Datos de la imagen. */
    private File imagen;
    /** Datos originales de la imagen. */
    private File original;
    /** Datos del audio del nombre en español. */
    private File nombreEsp;
    /** Datos del audio del nombre en el lenguaje original. */
    private File nombreLen;
    /** Datos del audio de la descripción en español. */
    private File descEsp;
    /** Datos del audio de la descripción en el lenguaje original. */
    private File descLen;
    /** Variable que asegura la consistencia en la serialización. */
    private static final long serialVersionUID = 1L;

    public static String localDir = "tarjeta/", imgExt = ".jpg", audioExt = ".aac";
    private static String imgDir = "images/tarjeta/", imgPrefix = "tarjeta-";
    private static String espDir = "audio/esp/", nespPrefix = "nombre-esp-", despPrefix = "desc-esp-";
    private static String lenDir = "audio/len/", nlenPrefix = "nombre-len-", dlenPrefix = "desc-len-";
    private static String orgPrefix = "tarjeta-original-";

    public Contenido() {}

    /**
     * Constructor del contenido de una Tarjeta.
     * @param imagen bytes de la imagen.
     * @param nombreEsp bytes del audio del nombre en español.
     * @param nombreLen bytes del audio del nombre en el lenguaje original.
     * @param descEsp bytes del audio de la descripción en español.
     * @param descLen bytes del audio de la descripción en el lenguaje original.
     */
    public Contenido(File nombreEsp, File nombreLen, File descEsp, File descLen, File imagen) {
        this.nombreEsp = nombreEsp;
        this.nombreLen = nombreLen;
        this.descEsp = descEsp;
        this.descLen = descLen;
        this.imagen = imagen;
    }

    public Contenido(File[] files) {
        this.nombreEsp = files[0];
        this.nombreLen = files[1];
        this.descEsp = files[2];
        this.descLen = files[3];
        this.imagen = files[4];
        this.original = files[5];
    }

    public File getOriginal() {
        return original;
    }

    public void setOriginal(File original) {
        this.original = original;
    }

    public File getImagen() {
        return imagen;
    }

    public void setImagen(File imagen) {
        this.imagen = imagen;
    }

    public File getNombreEsp() {
        return nombreEsp;
    }

    public void setNombreEsp(File nombreEsp) {
        this.nombreEsp = nombreEsp;
    }

    public File getNombreLen() {
        return nombreLen;
    }

    public void setNombreLen(File nombreLen) {
        this.nombreLen = nombreLen;
    }

    public File getDescEsp() {
        return descEsp;
    }

    public void setDescEsp(File descEsp) {
        this.descEsp = descEsp;
    }

    public File getDescLen() {
        return descLen;
    }

    public void setDescLen(File descLen) {
        this.descLen = descLen;
    }

    public File[] getFiles() {
        return new File[]{nombreEsp, nombreLen, descEsp, descLen, imagen, original};
    }

    /**
     * Consulta de la imagen de una tarjeta por su id.
     * @param t Tarjeta consultada.
     * @return Devuelve los datos de la imagen de la tarjeta.
     */
    public static File descargarImagen(Tarjeta t) {
        Contenido c = t.getDatos();
        if (c.getImagen() == null) {
            String nombre = imgPrefix + t.getIdtarjeta() + imgExt;
            c.setImagen(descargarArchivo(imgDir, localDir, nombre));
        } return c.getImagen();
    }

    /**
     * Consulta de la imagen original de una tarjeta por su id.
     * @param t Tarjeta consultada.
     * @return Devuelve los datos de la imagen original de la tarjeta.
     */
    public static File descargarOriginal(Tarjeta t) {
        Contenido c = t.getDatos();
        if (c.getOriginal() == null) {
            String nombre = orgPrefix + t.getIdtarjeta() + imgExt;
            c.setOriginal(descargarArchivo(imgDir, localDir, nombre));
        } return c.getOriginal();
    }

    /**
     * Consulta del contenido de una tarjeta por su id.
     * @param t Tarjeta consultada.
     * @return Devuelve los datos de la tarjeta.
     */
    public static Contenido descargarDatos(Tarjeta t, boolean completos) {
        Long idtarjeta = t.getIdtarjeta();
        Contenido c = t.getDatos();
        if (c.getNombreEsp() == null) {
            File nomEsp = descargarArchivo(espDir, localDir, nespPrefix + idtarjeta + audioExt);
            c.setNombreEsp(nomEsp); }
        if (c.getNombreLen() == null) {
            File nomLen = descargarArchivo(lenDir, localDir, nlenPrefix + idtarjeta + audioExt);
            c.setNombreLen(nomLen); }
        if (c.getDescEsp() == null) {
            File descEsp = descargarArchivo(espDir, localDir, despPrefix + idtarjeta + audioExt);
            c.setDescEsp(descEsp); }
        if (c.getDescLen() == null) {
            File descLen = descargarArchivo(lenDir, localDir, dlenPrefix + idtarjeta + audioExt);
            c.setDescLen(descLen); }
        c.setImagen(descargarImagen(t));
        if (completos) c.setOriginal(descargarOriginal(t));
        return c;
    }

    public static Contenido descargarDatos(Tarjeta t) {return descargarDatos(t, false); }

    /**
     * Método que se encarga de subir el contenido de una tarjeta.
     * @param t Tarjeta consultada.
     * @return Devuelve TRUE si los archivos se suben correctamente.
     */
    public static boolean subirDatos(Tarjeta t, File [] archivo) {
        Long idtarjeta = t.getIdtarjeta();
        renombrar(archivo, nespPrefix + idtarjeta + audioExt,
                            nlenPrefix + idtarjeta + audioExt,
                            despPrefix + idtarjeta + audioExt,
                            dlenPrefix + idtarjeta + audioExt,
                            imgPrefix + idtarjeta + imgExt,
                            orgPrefix + idtarjeta + imgExt);
        return subirArchivos("subir", "archivo", archivo);
    }

    public static File[] archivos(Tarjeta t) {
        Long idtarjeta = t.getIdtarjeta();
        File[] archivo = {cacheFile(localDir, nespPrefix + idtarjeta + audioExt),
                cacheFile(localDir, nlenPrefix + idtarjeta + audioExt),
                cacheFile(localDir, despPrefix + idtarjeta + audioExt),
                cacheFile(localDir, dlenPrefix + idtarjeta + audioExt),
                cacheFile(localDir, imgPrefix + idtarjeta + imgExt),
                cacheFile(localDir, orgPrefix + idtarjeta + imgExt)};
        return archivo;
    }

    public static File[] respaldo(Tarjeta t) {
        File temp[] = new File[]{grabacion(0), grabacion(1), grabacion(2), grabacion(3), captura(false), captura(true)};
        renombrar(temp, temp[0].getName() + "-" + t.getIdtarjeta(),
                        temp[1].getName() + "-" + t.getIdtarjeta(),
                        temp[2].getName() + "-" + t.getIdtarjeta(),
                        temp[3].getName() + "-" + t.getIdtarjeta(),
                        temp[4].getName() + "-" + t.getIdtarjeta(),
                        temp[5].getName() + "-" + t.getIdtarjeta());
        return (copiar(archivos(t), temp))? temp: null;
    }

    public static File grabacion(int i) {
        return cacheFile(localDir, "audio-" + i + audioExt);
    }

    public static File captura(boolean original) {
        try{
            if (original) return cacheFile(localDir, "original" + imgExt); //return File.createTempFile("original", imgExt); //
            return cacheFile(localDir, "compacta" + imgExt); //File.createTempFile("compacta", imgExt); //
        } catch (Exception e){return null;}
    }

    public static Bitmap[] setBitmap(boolean inWidth, File archivo, ImageView v) {
        return setBitmap(inWidth, archivo, v, v.getWidth(), v.getHeight());
    }

    public static Bitmap[] setBitmap(boolean inWidth, File archivo, ImageView v, int w, int h) {
        return setBitmap(inWidth, getBitMap(archivo), v, w, h);
    }

    public static Bitmap[] setBitmap(boolean inWidth, Bitmap imagen, ImageView v, int w, int h) {
        if (imagen == null) {
            v.setImageResource(R.drawable.camara);
            return new Bitmap[2];
        } String[] wh = getResponse().split(",");
        double width = Double.parseDouble(wh[0]);
        double height = Double.parseDouble(wh[1]);
        double scale = (inWidth)? w / width: h / height;
        Bitmap mini = Bitmap.createScaledBitmap(imagen,
                (int)(width*scale), (int)(height*scale), true);
        v.setScaleType(ImageView.ScaleType.CENTER_CROP);
        v.setImageBitmap(mini);
        return new Bitmap[]{imagen, mini};
    }
}