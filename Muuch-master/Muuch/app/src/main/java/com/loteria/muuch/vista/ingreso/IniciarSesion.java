package com.loteria.muuch.vista.ingreso;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.recovery.RecoveryTarjeta;
import com.loteria.muuch.vista.recovery.RecoveryAmigos;
import com.loteria.muuch.vista.recovery.RecoveryPerfil;

import static com.loteria.muuch.modelo.Persistencia.dataState;
import static com.loteria.muuch.modelo.Persistencia.getAmigos;
import static com.loteria.muuch.modelo.Persistencia.getColeccion;
import static com.loteria.muuch.modelo.Persistencia.getError;
import static com.loteria.muuch.modelo.Persistencia.getResponse;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.localServer;
import static com.loteria.muuch.modelo.Persistencia.login;
import static com.loteria.muuch.modelo.Persistencia.guardaSesion;
import static com.loteria.muuch.modelo.Persistencia.online;
import static com.loteria.muuch.modelo.Persistencia.recovery;

public class IniciarSesion extends MenuActivity {
    TextView ingresar, registrar, olvidada, sin_cuente;
    public static String nombre, contraseña;
    EditText edit_nombre, edit_contraseña;
    final public static int RECOVERY = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        localServer.load();
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_iniciar_sesion);
        edit_nombre = (EditText) findViewById(R.id.nombre);
        edit_contraseña = (EditText) findViewById(R.id.contraseña);
        ingresar = (TextView) findViewById(R.id.ingresar);
        registrar = (TextView) findViewById(R.id.registrar);
        olvidada = (TextView) findViewById(R.id.tv_sin_contraseña);
        sin_cuente = (TextView) findViewById(R.id.tv_sin_cuenta);

        String hint = edit_nombre.getHint() + " ";
        edit_nombre.setHint(hint + getString(R.string.longitud));
        hint = edit_contraseña.getHint() + " ";
        edit_contraseña.setHint(hint + getString(R.string.longitud));
        setFont(ingresar, registrar, edit_contraseña, edit_nombre, sin_cuente, olvidada);

        ingresar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                nombre = edit_nombre.getText().toString().trim();
                contraseña = edit_contraseña.getText().toString().trim();
                if (nombre.isEmpty() || contraseña.isEmpty()) {
                    Toast.makeText(IniciarSesion.this, "Ingrese un nombre y una contraseña", Toast.LENGTH_LONG).show();
                } else {
                    if (login(nombre, contraseña)) {
                        if (guardaSesion()) {
                            startActivity(new Intent(IniciarSesion.this, Inicio.class));
                            finish();
                        } else Toast.makeText(IniciarSesion.this, getString(R.string.error_reinicio), Toast.LENGTH_LONG).show();
                    } else {
                        String res = getResponse(), error = getError();
                        String msj = (res.equals("[]") || res.startsWith("{\"field"))? "El usuario ingresado no existe." :
                                (error.isEmpty())? res : error + ", " + (online? res: localServer.getResult());
                        if (!online) msj += getString(R.string.reintentar_conectado);
                        Toast.makeText(IniciarSesion.this, msj, Toast.LENGTH_LONG).show();
                        //dataState(IniciarSesion.this);
                    }
                }
            }


        });

        registrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(IniciarSesion.this, Registrarse.class));
                finish();
            }


        });

        olvidada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { restableceClave(); }
        });

        ImageView logo = (ImageView)findViewById(R.id.ingresar_logo_muuch);
        logo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                localServer.reset();
                Toast.makeText(IniciarSesion.this, localServer.getResult(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = (online = !online)? "activado":"desactivado";
                Toast.makeText(IniciarSesion.this, "Online " + s, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void restableceClave() {
        nombre = edit_nombre.getText().toString().trim();
        if (nombre.isEmpty()) {
            Toast.makeText(IniciarSesion.this,
                    "Debes ingresar tu nombre de usuario para recuperar tu cuenta",
                    Toast.LENGTH_LONG).show();
            return;
        }
        if (recovery(nombre)) {
            Usuario found = getUsuario();
            if (found == null) confirmar(getString(R.string.correo_enviado));
            else {
                if (!found.getCorreo().isEmpty()) {
                    String msj = "No se te ha podido enviar tu contraseña al correo que registraste";
                    Toast.makeText(IniciarSesion.this, msj, Toast.LENGTH_LONG).show();
                }
                if (found.getContraseña() != null) {
                    //dataState(this);
                    startActivityForResult(!found.getTarjetas().isEmpty()?
                    new Intent(IniciarSesion.this, RecoveryTarjeta.class):
                    !found.getAmigos().isEmpty()?
                    new Intent(IniciarSesion.this, RecoveryAmigos.class):
                    new Intent(IniciarSesion.this, RecoveryPerfil.class), RECOVERY);
                } else confirmar(getString(R.string.datos_insuficientes));
            }
        } else {
            //dataState(this);
            String msj = getString(R.string.usuario_inexistente);
            confirmar((online)? msj: msj + getString(R.string.reintentar_conectado));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY && resultCode == RESULT_OK) {
            startActivity(new Intent(IniciarSesion.this, Inicio.class));
            finish();
        }
    }

    @Override
    protected void initGuia() {
        if (!datos.getBoolean("guia textos leida", false)) {
            datos.edit().putBoolean("guia textos leida", true).commit();
            confirmar(getString(R.string.guia_longitud));
        }
    }
}
