package com.loteria.muuch.vista.arma_loteria_admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.controlador.SocketIO;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin;

import static com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin.tarjetasUsuarios;
import static com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin.listIdUser;
import static com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin.idioma1;
import static com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin.nivel1;
import static com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin.jugadores1;
import static com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin.dificultad1;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.loteria.muuch.modelo.Persistencia.getUsuario;

/**
 * Clase que recibe los usuarios conectados y se selecciona a los usuaroios con los que se desea jugar una
 * partida.
 */
public class ArmaLoteriaAdmin extends MenuActivity {

    private GridView gridView;
    private ImageButton btnInvitar;
    private GridView lista;
    //lista en la que se van a guardar los usuarios que estan conectados.
    private ArrayList<Usuario> listaInvitados;
    //lista en la que se van a guardar el id en String de los usuarios que quiero invitar a jugar una partida.
    private ArrayList<String> porInvitar;
    public Socket mSocket;
    //nivel, idioma, y jugadores de la partida
    public String nivel, idioma, jugadores, dificultad, categoria;
    private TextView tvInvitaATusAmigos;
    private TextView tvSeleccionaALosAmigos;
    //variable que va incrementando el numero de usuarios que aceptan la invitacion
    private int numeroAceptados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_armaloteria_admin);
        super.init(R.layout.activity_armaloteria_admin);

        Bundle datos = getIntent().getExtras();

        nivel = datos.getString("nivel");
        idioma = datos.getString("idioma");
        jugadores = datos.getString("jugadores");
        dificultad = datos.getString("dificultad");
        categoria = datos.getString("categoría");
        Toast.makeText(this, jugadores, Toast.LENGTH_SHORT).show();
        this.numeroAceptados=0;
        this.btnInvitar = (ImageButton) findViewById(R.id.imageButton21);
        lista = (GridView) findViewById(R.id.gridViewMisAmigos_admin);
        listaInvitados = new ArrayList<>();
        porInvitar = new ArrayList<>();
        tvInvitaATusAmigos = (TextView) findViewById(R.id.textView5);
        tvSeleccionaALosAmigos = (TextView) findViewById(R.id.textView6);
        /**
         * Boton que envia al servidor el id de los usuarios que invitaste
         */
        btnInvitar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //se guarda los usurios que se seleccrionaron, porInvitar es una lista de string que guarda
                //los usuarios a los que se va a invitar a jugar
                String invitados = porInvitar.toString();
                invitados = invitados.replace("[","");
                invitados = invitados.replace("]","");
                invitados = invitados.trim();
                /*
                Se envian al servidor los jugadores en formato: 1,2,3,4
                el idioma, nivel, y numero de jugadores
                */
                mSocket.emit("invitoJugadores",invitados+"-"+idioma+","+nivel+","+jugadores);
                Toast.makeText(ArmaLoteriaAdmin.this, invitados, Toast.LENGTH_SHORT).show();
                //los textos cambian y el boton se hace invisible a la espera que todos acepten la invitacion.
                btnInvitar.setVisibility(View.INVISIBLE);
                tvInvitaATusAmigos.setText("Ya invitaste a tus amigos");
                tvSeleccionaALosAmigos.setText("Espera a que todos reciban la invitacion");
                btnInvitar.setVisibility(View.INVISIBLE);

            }
        });
        gridView = (GridView) findViewById(R.id.gridViewMisAmigos_admin);
        gridView.setAdapter(new AdapterAmigosArmaLoteria(this));
        SocketIO app =  (SocketIO) getApplication();
        mSocket = app.getSocket();
        mSocket.emit("ObtieneJugadores");
        mSocket.on("usuarioAcepto",invitacionAceptada);
        mSocket.on("LosUsuariosConectados", invitacion);
        AdapterListaInvita ali = new AdapterListaInvita(this, listaInvitados);
        lista.setAdapter(ali);
        /*
        Si se seleciona un usuario que se puede invitar a la partida se añade a la lista porInvitar que guarda
        String, si lo selecciona y ya esta añadido lo quieta de la lista
         */
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String seleccionado = String.valueOf(listaInvitados.get(position).getIdusuario());
                if(porInvitar.contains(seleccionado)){
                    porInvitar.remove(seleccionado);
                }else{
                    porInvitar.add(seleccionado);
                    Toast.makeText(ArmaLoteriaAdmin.this, "Invitar a "+
                            listaInvitados.get(position).getNombre(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Si un usuario acepta la invitacion se ejecuta el siguiente listener.
     * El cual agrega incrementa en uno a numeroAceptados, se procesa la cadena de entrada guardando las tarjetas
     * del usuario que acepto en una lista de la clase JugandoAdmin. Si todos los usuarios aceptaron se
     * avanza al siguiente activity.
     * La cadena de entrada tiene el siguiente formato: idAdminIO-idUser-listaTarjetasUser
     */
    private Emitter.Listener invitacionAceptada = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(numeroAceptados==0){
                        listIdUser.clear();
                        tarjetasUsuarios.clear();
                    }
                    numeroAceptados++;

                    Toast.makeText(ArmaLoteriaAdmin.this, "numero de jugadores: "
                            +jugadores+" numero aceptaron: "+numeroAceptados, Toast.LENGTH_SHORT).show();

                    //idUserSocket-idUser-listaTarjetasSeleccionadasPorUser
                    String entrada = args[0].toString().trim();
                    String[] separar = entrada.split("-");
                    String usuarioIdCorreo = separar[1];
                    String usuarioIdPersona = separar[0];
                    //la siguiente cadena tiene el formato [1,3,5] solo los numero son el id de las
                    //tarjetas, se tienen que procesar
                    String tarjetasEnviadas = separar[2];
                    tarjetasEnviadas = tarjetasEnviadas.replace("]", "");
                    tarjetasEnviadas = tarjetasEnviadas.replace("[", "").trim();
                    String[] arrTarjetasLlegadas = tarjetasEnviadas.split(",");
                    for(int i = 0; i<arrTarjetasLlegadas.length; i++){
                        tarjetasUsuarios.add(arrTarjetasLlegadas[i].trim());
                    }
                    if(numeroAceptados==Integer.parseInt(jugadores)){
                        listIdUser.clear();
                        idioma1 = idioma;
                        jugadores1 = jugadores;
                        nivel1 = nivel;
                        dificultad1=dificultad;
                        //listIdUser=listaInvitados;
                        for(String s : porInvitar){
                            listIdUser.add(Persistencia.getUsuario(Long.parseLong(s.trim())));
                        }
                        numeroAceptados=0;
                        startActivity(new Intent(ArmaLoteriaAdmin.this, JugandoAdmin.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    }
                }
            });
        }
    };

    /**
     * Listener que se ejecuta cuando llegan los usuarios que estan conectados.
     * Verifica si por lo menos hay un jugador conectado, si no lo hay solo manda mensaje
     * si hay mas de un jugador conectado guarda los usuario en la lista listaInvitados que guarda
     * a los usuarios que estan conectados y disponibles para jugar.
     * El mensaje de entrada tiene la lista de jugadores con el formato: 1,2,3,4,5
     */
    private Emitter.Listener invitacion = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String invitados = args[0].toString().trim();
                    if(invitados.equalsIgnoreCase("")){
                    }else{
                        String[] sep = invitados.split(",");
                        for(int i = 0; i<sep.length; i++){
                            if(!sep[i].equalsIgnoreCase("")){
                                listaInvitados.add(getUsuario(new Long(sep[i])));
                            }
                        }
                        lista.setAdapter(new AdapterListaInvita(ArmaLoteriaAdmin.this, listaInvitados));
                    }
                }
            });
        }
    };

}
