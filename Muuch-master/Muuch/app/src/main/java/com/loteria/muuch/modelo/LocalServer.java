package com.loteria.muuch.modelo;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.loteria.muuch.modelo.Contenido.localDir;
import static com.loteria.muuch.modelo.Contenido.subirDatos;
import static com.loteria.muuch.modelo.Persistencia.cacheFile;
import static com.loteria.muuch.modelo.Persistencia.copiar;
import static com.loteria.muuch.modelo.Persistencia.getAmigos;
import static com.loteria.muuch.modelo.Persistencia.getColeccion;
import static com.loteria.muuch.modelo.Persistencia.getId;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.internalFile;
import static com.loteria.muuch.modelo.Persistencia.online;

/**
 * @author Luis Angel Patlani Aguilar
 * Clase que se encarga de la persistencia en la Base de Datos.
 */
public class LocalServer {

    /** Tabla Usuario */
    private HashMap<Long, Usuario> usuarios;
    /** Tabla Tarjeta */
    private HashMap<Long, Tarjeta> tarjetas;
    /** Tablas de referencias. */
    private HashMap<Long, ArrayList<Long>> collected, added;
    /** Archivo donde se guarda la base de datos. */
    private File base;

    /** Interfaz y variables auxiliares para consulta. */
    private static interface Select { public boolean condition(Object param);}
    private static int index = 0;
    private static long ids[];
    private static String result = "";
    private static ArrayList<Long> keys;
    public static Usuario user;
    public static Tarjeta card;
    public static Long key;
    public static boolean cardsOk, friendsOk;

    public LocalServer() {
        base = internalFile("data/", "base.muuch");
        if (load()) return;
        usuarios = new HashMap<>();
        tarjetas = new HashMap<>();
        collected = new HashMap<>();
        added = new HashMap<>();
        ids = new long[2];
    }

    public boolean load() {
        try{
            if (!base.exists()) return false;
            FileInputStream entrada = new FileInputStream(base);
            ObjectInputStream lector = new ObjectInputStream(entrada);
            usuarios = (HashMap<Long, Usuario>)lector.readObject();
            tarjetas = (HashMap<Long, Tarjeta>)lector.readObject();
            collected = (HashMap<Long, ArrayList<Long>>)lector.readObject();
            added = (HashMap<Long, ArrayList<Long>>)lector.readObject();
            ids = (long[])lector.readObject();
            lector.close();
            entrada.close();
            result = "La base ha sido cargada.";
            return true;
        } catch (Exception ex) {
            result = "[Error: No se pudo cargar la base: " + ex.toString() + "]";
        } return false;
    }

    public boolean save() {
        try{
            FileOutputStream salida = new FileOutputStream(base);
            ObjectOutputStream escritor = new ObjectOutputStream(salida);
            escritor.writeObject(usuarios);
            escritor.writeObject(tarjetas);
            escritor.writeObject(collected);
            escritor.writeObject(added);
            escritor.writeObject(ids);
            escritor.close();
            salida.close();
            result = "La base ha sido actualizada.";
            return true;
        } catch (Exception ex) {
            result = "[Error: No se pudo actualizar la base: " + ex.toString() + "]";
        } return false;
    }

    public boolean reset() {
        ids = new long[2];
        usuarios = new HashMap<>();
        tarjetas = new HashMap<>();
        collected = new HashMap<>();
        added = new HashMap<>();
        base.delete();
        return (result = !base.exists()? "La base ha sido eliminada":
                "No se ha podido borrar la base").startsWith("La");
    }

    public boolean clean(Context c) {
        try{
            File carpeta[] = cacheFile(localDir, "").listFiles();
            String info = "Limpiando archivos ...";
            for (File archivo: carpeta) {
                String parts[] = archivo.getName().split("-");
                if (parts.length > 1) {
                    int last = parts.length - 1;
                    key = toLong(parts[last].substring(0, parts[last].indexOf(".")));
                    if (!tarjetas.containsKey(key)) {
                        info +=  "\nBorrando... " + archivo.getName();
                        archivo.delete();
                    }
                }
            } result = info + "\nMemoria caché limpiada";
            return true;
        } catch (Exception e) {
            result = "[Error cleaning: " + e.toString() + "]";
            return false;
        }
    }

    public String getResult() {return result;}

    public String getHTTP(final Object ... data) {
        try{
            result = toString(data[1]);
            //if (result.equals("u-update")) return result = toString(data);
            if (result.startsWith("u-")) { // Requiere datos de usuario.
                switch (result) {
                    case "u-login":
                        user = new Usuario(toString(data[3]), toString(data[5]));
                        return result = jsonArray(usuariosQue(new Select() {
                            @Override
                            public boolean condition(Object param) {
                                try {
                                    Usuario u = (Usuario) param;
                                    if (u.getNombre().equals(user.getNombre()) &&
                                        u.getContraseña().equals(user.getContraseña())) {
                                        user = u;
                                        return true;
                                    } return false;
                                } catch (Exception e) {return false;}
                            }
                        }));
                    case "u-id":
                        return result = "[" + jsonObject(user = usuario(data[3])) + "]";
                    case "u-collected":
                        keys = collected(data[3]);
                        ArrayList<Tarjeta> c = new ArrayList<>();
                        for (Long id : keys) c.add(tarjetas.get(id));
                        return result = jsonArray(c, false);
                    case "u-notcollected":
                        keys = added(data[3]);
                        result += keys.toString();
                        return result = jsonArray(tarjetasQue(new Select() {
                            @Override
                            public boolean condition(Object param) {
                                return keys.contains(((Tarjeta)param).getIdusuario());
                            }
                        }), true);
                    case "u-add":
                        if (online) return "[Inserción bloqueada.]";
                        key = toLong(++ids[0]);
                        user = new Usuario(toString(data[3]), toString(data[7]));
                        user.setIdusuario(key);
                        user.setCorreo(toString(data[5]));
                        user.setAvatar(toInt(data[9]));
                        result = user.toString();
                        if (usuariosQue(new Select() {
                            @Override
                            public boolean condition(Object param) {
                                return user.equals(param);
                            }
                        }).size() > 0) return result = "[Error: Ya existe otro usuario con ese nombre.]";
                        usuarios.put(key, user);
                        collected.put(key, new ArrayList<Long>());
                        added.put(key, new ArrayList<Long>());
                        if (!online) user.setInServer(false);
                        return result = save()? "{\"insertId\":" + user.getIdusuario() + "}":
                        "[Error: La base de datos no pudo ser actualizada.]";
                    case "u-update":
                        key = toLong(toLong(data[3]));
                        user = new Usuario(toString(data[5]), toString(data[9]));
                        user.setIdusuario(key);
                        user.setCorreo(toString(data[7]));
                        user.setAvatar(toInt(data[11]));
                        if (usuariosQue(new Select() {
                            @Override
                            public boolean condition(Object param) {
                                int id = toInt(((Usuario)param).getIdusuario());
                                return id != toInt(key) && user.equals(param);
                            }
                        }).size() > 0) return result = "[Error: Ya existe otro usuario con ese nombre.]";
                        usuarios.put(key, user);
                        if (!online) user.setOld(true);
                        return result = save()? "[Usuario actualizado]":
                        "[Error: La base de datos no pudo ser actualizada.]";
                    case "u-remove":
                        key = toLong(data[3]);
                        keys = collected(key);
                        for (Long id: keys) tarjetas.remove(id);
                        collected.remove(key);
                        added.remove(key);
                        user = usuarios.remove(key);
                        if (key == ids[0]) --ids[0];
                        return result = save()? "[Usuario eliminado]":
                        "[Error: La base de datos no pudo ser actualizada.]";
                    default: return result = jsonArray(usuariosQue(new Select() {
                                @Override
                                public boolean condition(Object param) { return true;}
                            }));
                }
            } else if (result.startsWith("c-")){ // Para datos de tarjetas.
                switch (result) {
                    case "c-id":
                        return result = "[" + jsonObject(card = tarjeta(data[3])) + "]";
                    case "c-add":
                        if (online) return "[Inserción bloqueada.]";
                        key = toLong(++ids[1]);
                        card = new Tarjeta(key, toString(data[5]),
                                toString(data[7]), toInt(data[9]), usuario(data[3]));
                        keys = collected(data[3]);
                        tarjetas.put(key, card);
                        collected(data[3]).add(key);
                        if (!online) card.setInServer(false);
                        return result = save()? "{\"insertId\":" + key + "}":
                        "[Error: La base de datos no pudo ser actualizada.]";
                    case "c-update":
                        key = toLong(data[11]);
                        card = new Tarjeta(key, toString(data[5]),
                                toString(data[7]), toInt(data[9]), usuario(data[3]));
                        if (!online) card.setOld(true);
                        tarjetas.put(key, card);
                        return result = save()? "[Tarjeta actualizada.]":
                                "[Error: La base de datos no pudo ser actualizada.]";
                    case "c-remove":
                        key = toLong(data[5]);
                        tarjetas.remove(key);
                        keys = collected(data[3]);
                        keys.remove(key);
                        if (key == ids[1]) --ids[1];
                        return result = save()? "[Tarjeta eliminada.]":
                                "[Error: La base de datos no pudo ser actualizada.]";
                    default: return result = jsonArray(tarjetasQue(new Select() {
                        @Override
                        public boolean condition(Object param) {
                            return true;
                        }
                    }), true);
                }
            } else { // Para datos de amigos.
                switch (result) {
                    case "a-added":
                        keys = added(data[3]);
                        ArrayList<Usuario> a = new ArrayList<>();
                        for (Long id : keys) {
                            Usuario u = usuarios.get(id);
                            if (u != null) a.add(u);
                        } return result = jsonArray(a);
                    case "a-notadded":
                        key = toLong(data[3]);
                        keys = added(key);
                        return result = jsonArray(usuariosQue(new Select() {
                            @Override
                            public boolean condition(Object param) {
                                Long id = ((Usuario)param).getIdusuario();
                                return !(key.equals(id) || keys.contains(id));
                            }
                        }));
                    case "a-add":
                        if (online) return "[Inserción bloqueada.]";
                        if (added(data[3]).contains(data[5])) return "[Amigo agregado]";
                        added(data[3]).add(toLong(data[5]));
                        return result = (save())? "[Amigo agregado]":
                        "[Error: La base de datos no pudo ser actualizada.]";
                    case "a-remove":
                        //if (!online) return result = "[Error: Prohibido eliminar, posible conflicto de id.]";
                        added(data[3]).remove(toLong(data[5]));
                        return result = (save())? "[Amigo eliminado]":
                        "[Error: La base de datos no pudo ser actualizada.]";
                    default: return result = "[Error: \"Unknown statement\"]";
                }
            }
        } catch (Exception ex) {
            try{
                String query = "'" + toString(data) + " '";
                return result = "[Error: On Query " + query + ", " + ex.toString() + "]";
            } catch (Exception e) { return result + ":" + e.toString(); }
        }
    }

    private int toInt(Object obj) { return new Integer(obj.toString()); }
    private long toLong(Object obj) { return new Long(obj.toString()); }
    private int toIndex(Object obj) { return toInt(obj) - 1; }
    private Usuario usuario(Object o) { return usuarios.get(o); }
    private Tarjeta tarjeta(Object o) { return tarjetas.get(o); }
    private ArrayList<Long> added(Object o) { return added.get(o); }
    private ArrayList<Long> collected(Object o) { return collected.get(o); }
    public String toString(Object ... objs) {
        String s = (objs[0].toString()).replaceAll("'","");
        for (int i = 1; i < objs.length; i++) s += " " + objs[i].toString();
        return s;
    }

    private String userJSON(Usuario u, boolean conId) {
        return ((conId)? "\"idusuario\":" + u.getIdusuario() : "")
                + ",\"nombre\":\"" + u.getNombre()
                + "\",\"correo\":\"" + u.getCorreo()
                + "\",\"avatar\":" + u.getAvatar()
                + ((u.getContraseña() != null)? ",\"contraseña\":\"" + u.getContraseña() + "\"":"");
    }

    private String cardJSON(Tarjeta t) {
        return "\"idtarjeta\":" + t.getIdtarjeta()
                + ",\"nombreLen\":\"" + t.getNombreLen()
                + "\",\"nombreEsp\":\"" + t.getNombreEsp()
                + "\",\"categoria\":\"" + t.getCategoria()
                + "\",\"idusuario\":" + t.getIdusuario();
    }

    private String jsonObject(Tarjeta t) {
        return "{" + cardJSON(t) + "}";
    }

    private String jsonObject(Tarjeta t, Usuario u) {
        return "{" + cardJSON(t) + userJSON(u, false) + "}";
    }

    private String jsonObject(Usuario u) {
        return "{" + userJSON(u, true) + "}";
    }

    private String jsonArray(ArrayList<Usuario> lista) {
        result = "[";
        try{
            for (int i = 0; i < lista.size(); i++) {
                Usuario u = lista.get(i);
                if (u.getIdusuario() != null) {
                    result += jsonObject(u);
                    if (i < lista.size() - 1) result += ",";
                }
            }
        }catch (Exception ex) {
            result += "Error: Building JSONUsers: " + ex.toString();
        }
        return result += "]";
    }

    private String jsonArray(ArrayList<Tarjeta> lista, boolean conUsuario) {
        result = "[";
        try {
            for (int i = 0; i < lista.size(); i++) {
                Tarjeta t = lista.get(i);
                if (t.getIdusuario() != null) {
                    Usuario u = usuario(t.getIdusuario());
                    if (conUsuario) result += jsonObject(t, u);
                    else result += jsonObject(t);
                    if (i < lista.size() - 1) result += ",";
                }
            }
        } catch (Exception ex) {result += "Error: BuildingJSONCards:" + ex.toString();}
        return result += "]";
    }

    private ArrayList<Usuario> usuariosQue(Select statement) {
        ArrayList<Usuario> founds = new ArrayList<>();
        Iterator i = usuarios.entrySet().iterator();
        while (i.hasNext()) {
            Usuario u = (Usuario)((Map.Entry)i.next()).getValue();
            if (u.getIdusuario() != null && statement.condition(u)) founds.add(u);
        } index = 0;
        return founds;
    }

    private ArrayList<Tarjeta> tarjetasQue(Select statement) {
        ArrayList<Tarjeta> founds = new ArrayList<>();
        Iterator i = tarjetas.entrySet().iterator();
        while (i.hasNext()) {
            Tarjeta t = (Tarjeta)((Map.Entry)i.next()).getValue();
            if (t.getIdtarjeta() != null && statement.condition(t)) founds.add(t);
        } index = 0;
        return founds;
    }

    /**
     * Método que recupera los datos, amigos y tarjetas de un usuario, por nombre.
     * @param nombre Nombre del usuario
     * @return Devuelve una cadena formato json con los datos, amigos y tarjetas del usuario.
     */
    public String recovery(final String nombre) {
        try{
            user = null; // Se asegura que el usuario era nulo, antes de buscarlo.
            ArrayList<Usuario> encontrados = usuariosQue(new Select() {
                @Override
                public boolean condition(Object param) {
                    Usuario u = (Usuario) param; // Se asgina a user, el usuario encontrado.
                    if (u.getNombre().equals(nombre) && u.getContraseña() != null) user = u;
                    return false; // No es necesario agregar usuarios al resultado de búsqueda.
                }
            }); // Se el usuario es nulo, es que no se ha encontrado su existencia.
            if (user == null) return "[]";
            key = user.getIdusuario();
            ArrayList<Usuario> amigos = new ArrayList<>();
            for (Long k: added(key)) amigos.add(usuario(k));
            ArrayList<Tarjeta> coleccion = new ArrayList<>();
            for (Long k: collected(key)) coleccion.add(tarjeta(k));
            return result = "[" + jsonObject(user) + "," + jsonArray(amigos) + "," + jsonArray(coleccion, false) + "]";
        } catch (Exception e) {return result = "[Error on recovery from " + nombre + ": " + e.toString() + "]"; }
    }

    /**
     * Método que sincroniza los datos del usuario, dando prioridad a lo que se encuentra en el servidor.
     * @return Devuelve tru si el usuario fue reconocido y sincronizado localmente.
     */
    public boolean syncLogin() {
        LocalServer backup = new LocalServer(); result = "";
        try { // Se sincroniza al usuario.
            ArrayList<Long> a = new ArrayList<>(), c = new ArrayList<>();
            if (getUsuario() == null) {
                // Indica que no se encontró en la base de datos local.
                if (user == null || user.getIdusuario() == null) return false;
                Long k = user.getIdusuario();
                Usuario local = usuarios.remove(k);
                // Se guardan los amigos y tarjetas que tenía;
                a = added.remove(k); c = collected.remove(k);
                // Si no estaba en el servidor, debe insertarse.
                if (!Persistencia.insertaUsuario(local)) { backup.save(); return false; }
                local.setInServer(true); // Ahora se encuentra en el servidor.
            } else if (user != null && user.getIdusuario() != null && user.equals(getUsuario())){
                Long k = user.getIdusuario();
                if (getId().longValue() != k.longValue()) usuarios.remove(k);
                a = added.remove(k); c = collected.remove(k);
            } user = getUsuario();
            key = user.getIdusuario(); // Es probablemente un id distinto al del servidor.
            usuarios.put(key, user); // Reemplazo del usuario.
            added.put(key, a); // Se le asignan los amigos que tenía, a su posible nuevo id.
            collected.put(key, c); // Se le asignan las tarjetas que tenía, a su posible nuevo id.
            for (Long k: c) tarjeta(k).setIdUsuario(user); // Se reasigna el usuario de sus tarjetas.
            cardsOk = false; friendsOk = false; // Para futura sincronización.
            if (!save()) return false;
            result = user.toString() + "\n" + usuarios.toString(); return true;
        } catch (Exception e) {
            // Se recarga la base y se devuelve falso, porque el usuario no se ha sincronizado.
            backup.save(); result = "[Error on sync user: " + e.toString() + "]";
        } return online = false;
    }

    /**
     * Se sincronizan los amigos del usuario que ha ingresado.
     * @return Devuelve TRUE si los amigos se han sincronizado.
     */
    public boolean syncFriends() {
        LocalServer backup = new LocalServer();
        try { // Se sincronizan amigos.
            ArrayList<Usuario> amigos = getAmigos();
            if (!online) return friendsOk = true; // Si no hay internet se detiene la sincronización.
            friendsOk = false; // Para indicar que los amigos no se han sincronizado.
            user = getUsuario(); key = user.getIdusuario();
            Usuario local = usuarios.get(key); // Se buscan datos locales.
            // Se agregarán a los que tenía el usuario en el servidor, si es el mismo.
            keys = (local != null && local.equals(user))? added(key): new ArrayList<Long>();
            result = keys.toString();
            for (Usuario u : amigos) {
                Long id = u.getIdusuario(); local = usuario(id);
                // Si asegura la existencia del amigo, con sus datos disponibles.
                if (local == null) {
                    added.put(id, new ArrayList<Long>());
                    collected.put(id, new ArrayList<Long>());
                } else { // Si el usuario se ha sincronizado, se conserva su contraseña.
                    if (local.isInServer()) u.setContraseña(local.getContraseña());
                } usuarios.put(id, u); // Se reemplaza al usuario local.
                if (!keys.contains(id)) keys.add(id); // Se agregan amistades nuevas, se ignoran las eliminaciones.
                if (id.longValue() > ids[0]) ids[0] = id; // Se actualiza el id, para insertar.
            } added.put(key, keys); // Se sincroniza la lista de amigos.
            result += keys.toString();
            friendsOk = true; // Para indicar que los amigos se han sincronizado.
            return save();
        } catch (Exception e) { result += "[Error on sync friends: " + e.toString() + "]"; }
        // Se recarga la base y se devuelve falso, porque el usuario no se ha sincronizado.
        backup.save();
        return online = false;
    }

    /**
     * Método que sincroniza las tarjetas no actualizadas en el servidor.
     * @return Devuelve TRUE si el proceso es completado.
     */
    public boolean syncCards() {
        LocalServer backup = new LocalServer();
        try { // Se sincronizan tarjetas.
            if (!online) return cardsOk = true; // Si no hay internet se detiene la sincronización.
            cardsOk = false; // Para indicar que las tarjetas no se han sincronizado.
            user = getUsuario(); key = user.getIdusuario();
            Usuario local = usuarios.get(key); // Se buscan datos locales.
            // Se agregarán a los que tenía el usuario en el servidor, si es el mismo.
            keys = (local != null && local.equals(user))? collected(key): new ArrayList<Long>();
            result = keys.toString();
            // Se analizan las tarjetas creadas y editadas, sin conexión.
            ArrayList<Tarjeta> pendientes = new ArrayList<>();
            for (Long k: keys) {
                Tarjeta t = tarjeta(k);
                if (!t.isInServer() || t.isOld()) {
                    Tarjeta p = new Tarjeta().copiar(t);
                    // Si no se respaldan los datos, se detiene el proceso en falso.
                    if (!p.respaldar()) return false;
                    // Se agrega una copia de la tarjeta en la base de datos.
                    pendientes.add(p);
                }
            } // Se realiza el proceso con una lista de respaldo.
            if (cardsUploaded(pendientes)) {
                ArrayList<Tarjeta> coleccion = getColeccion();
                key = getUsuario().getIdusuario();
                keys = new ArrayList<Long>(); // en este punto todas las tarjetas están en el servidor.
                for (Tarjeta t :coleccion) {
                    Long id = t.getIdtarjeta();
                    tarjetas.put(id, t); // Se reemplaza la tarjeta, con la del servidor.
                    keys.add(id); // Se agregan nuevas tarjetas.
                    if (id.longValue() > ids[1]) ids[1] = id; // Se actualiza el máximo id de tarjeta.
                } collected.put(key, keys); // Se actualiza la lista de tarjetas.
                cardsOk = true; // Para indicar que las tarjetas se han sincronizado.
                if (!save()) return false;
                result += "Collected: " + collected(key).toString();
                result += user.toString() + "\n" + tarjetas.toString(); return true;
            } else { for (Tarjeta p: pendientes) subirDatos(p, p.getDatos().getFiles()); }
        } catch (Exception e) { result += "[Error on sync cards: " + e.toString() + "]"; }
        // Se recarga la base y se devuelve falso, porque el usuario no se ha sincronizado.
        backup.save();
        return online = false;
    }

    /**
     * Mëtodo que se encarga de subir tarjetas pendientes.
     * @param pendientes Lista de tarjetas pendientes.
     * @return Devuelve TRUE si las tarjetas se subieron correctamente.
     * @throws Exception Lanza cualquier excepción del proceso.
     */
    private boolean cardsUploaded(ArrayList<Tarjeta> pendientes) {
        try { result = "";
            for (Tarjeta p: pendientes) {
                Long k = p.getIdtarjeta();
                Tarjeta t = tarjeta(k);
                if (!t.isInServer()) { // Si no está en el servidor, debe insertarse.
                    tarjetas.remove(k); // Se borra la tarjeta con el id, anterior.
                    if (!t.insertar()) {
                        result += "[Error inserting: " + t.toString() + "]";
                        return false;
                    } // El id, debería actualizarse por SyncCardAdd.
                } else { // Si ya estaba en el servidor, no se ha actualizado.
                    if (!t.actualizar()) {
                        result += "[Error updating: " + t.toString() + "]";
                        return false;
                    } // El id, debería conservarse tras una actualización.
                } // Ahora los datos coinciden en el servidor.
                Tarjeta n = tarjeta(t.getIdtarjeta());
                n.setInServer(true); n.setOld(false);
                if (!subirDatos(n, p.getDatos().getFiles())) {
                    result += "[Error uploading data from: " + t.toString() + "]";
                    return false;
                }
            } return true;
        } catch (Exception e) {
            result += "[Error on sync cards: " + e.toString() + "]";
            return false;
        }
    }

    /**
     * Método que sincroniza la inserción de un usuario en línea.
     * @param inServer Usuario que se asume insertado en el servidor.
     * @return Devuelve TRUE si el proceso se realizó correctamente.
     */
    public boolean syncUserAdded(Usuario inServer) {
        try {
            Long id = inServer.getIdusuario();
            usuarios.put(id, inServer);
            result += inServer.toString();
            added.put(id, new ArrayList<Long>());
            collected.put(id, new ArrayList<Long>());
            return save();
        } catch (Exception e) { result = "[Error on sync user added: " + e.toString() + "]";}
        return false;
    }

    /**
     * Método que sincroniza la inserción de una tarjeta en línea.
     * @param inServer Tarjeta que se asume insertada en el servidor.
     * @return Devuelve TRUE si el proceso se realizó correctamente.
     */
    public boolean syncCardAdded(Tarjeta inServer) {
        try{
            Long id = inServer.getIdtarjeta();
            tarjetas.put(id, inServer);
            result += inServer.toString();
            collected(inServer.getIdusuario()).add(id);
            return save();
        } catch (Exception e) { result = "[Error on sync card added: " + e.toString() + "]";}
        return false;
    }


    /**
     * Método que sincroniza la inserción de un usuario en línea.
     * @param inServer Usuario que se asume insertado en el servidor.
     * @return Devuelve TRUE si el proceso se realizó correctamente.
     */
    public boolean syncFriendAdded(Usuario inServer) {
        try {
            Long id = inServer.getIdusuario();
            Usuario local = usuario(id);
            // Si asegura la existencia del amigo, con sus datos disponibles.
            if (local == null) {
                added.put(id, new ArrayList<Long>());
                collected.put(id, new ArrayList<Long>());
            } else { // Si el usuario se ha sincronizado, se conserva su contraseña.
                if (local.isInServer()) inServer.setContraseña(local.getContraseña());
            } usuarios.put(id, inServer); // Se reemplaza al usuario local.
            keys = added(getId());
            if (!keys.contains(id)) keys.add(id); // Se agregan amistades nuevas, se ignoran las eliminaciones.
            if (id.longValue() > ids[0]) ids[0] = id; // Se actualiza el id, para insertar.
            return save();
        } catch (Exception e) { result = "[Error on sync friend added: " + e.toString() + "]";}
        return false;
    }
}