package com.loteria.muuch.vista.mis_tarjetas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.ingreso.AvatarAdapter;

import java.util.ArrayList;

import static com.loteria.muuch.modelo.Persistencia.getColeccion;
import static com.loteria.muuch.modelo.Persistencia.getCompartidas;

public class OtrasCategorias extends MisCategorias {

    public static Usuario amigo;

    public void filtra() {
        ArrayList<Tarjeta> coleccion = getColeccion(), compartidas = getCompartidas();
        int n = categorias.length;
        for (Tarjeta t: coleccion) coleccionPorCategoria[t.getCategoria()].add(t);
        long idamigo = amigo.getIdusuario();
        for (Tarjeta t: compartidas) {
            long idusuario = t.getIdusuario();
            if (idamigo == idusuario) compartidasPorCategoría[t.getCategoria()].add(t);
        }
        for (int i = 0; i < n; i++) {
            int size = compartidasPorCategoría[i].size();
            cantidades[i].setText(" " + size + " ");
        }
        fab.setImageResource(AvatarAdapter.getAvatar(amigo));
        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return false;
            }
        });
        nombre_usuario.setText(amigo.getNombre());
    }

    protected void avanza(int i) {
        coleccionadas = coleccionPorCategoria[i];
        compartidas = compartidasPorCategoría[i];
        seleccionada = i;
        startActivityForResult(new Intent(OtrasCategorias.this, OtrasTarjetas.class), 2);
    }
}
