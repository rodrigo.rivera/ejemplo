package com.loteria.muuch.vista.arma_loteria_user;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.vista.mis_tarjetas.TarjetaAdapter;

import java.util.ArrayList;

import static com.loteria.muuch.modelo.Contenido.setBitmap;

/**
 * Created by PATLANIUNAM on 01/10/2017.
 */

public class AdapterSeleccionTarjeta extends TarjetaAdapter {

    public ArrayList<Integer> seleccion;
    private static Drawable naranja, azul;
    private static int colorLetra;

    public AdapterSeleccionTarjeta(Context contexto, Typeface font, ArrayList<Tarjeta> lista) {
        super(contexto, font, lista);
        this.seleccion = new ArrayList<>();
        naranja = contexto.getResources().getDrawable(R.drawable.esquina_redondeado_administra_juego);
        azul = contexto.getResources().getDrawable(R.drawable.esquina_redondeado_admin);
        colorLetra = contexto.getResources().getColor(R.color.azulmarino_F8D);
    }

    public ArrayList<Tarjeta> getSeleccion() {
        ArrayList<Tarjeta> seleccionados = new ArrayList<>();
        for (int i: seleccion) seleccionados.add(lista.get(i));
        return seleccionados;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final View v = super.getView(i, view, viewGroup);
        ((TextView)v.findViewById(R.id.adapter_nesp)).setTextColor(colorLetra);
        ((TextView)v.findViewById(R.id.adapter_nlen)).setTextColor(colorLetra);
        ((ImageButton)v.findViewById(R.id.btn_mas)).setVisibility(View.VISIBLE);
        ajustarVista(i, v, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { ajustarVista(i, v, true);}
        });
        return v;
    }

    public void ajustarVista(int i, View v, boolean invertir) {
        if (seleccion.contains(new Integer(i))) {
            if (invertir) {
                seleccion.remove(new Integer(i));
                ajustarVista(i, v, false);
            } else {
                ((ImageButton)v.findViewById(R.id.btn_mas)).setVisibility(View.INVISIBLE);
                ((ImageButton)v.findViewById(R.id.btn_visto)).setVisibility(View.VISIBLE);
                v.setBackgroundDrawable(naranja);
            }
        } else {
            if (invertir) {
                seleccion.add(new Integer(i));
                ajustarVista(i, v, false);
            } else {
                ((ImageButton)v.findViewById(R.id.btn_mas)).setVisibility(View.VISIBLE);
                ((ImageButton)v.findViewById(R.id.btn_visto)).setVisibility(View.INVISIBLE);
                v.setBackgroundDrawable(azul);
            }
        }
    }
}