package com.loteria.muuch.vista.arma_loteria_user.jugando_loteria;


import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.loteria.muuch.R;
import com.loteria.muuch.controlador.SocketIO;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.arma_loteria_user.AdapterSeleccionTarjeta;
import com.loteria.muuch.vista.ingreso.Inicio;

import java.util.ArrayList;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class JugandoLoteria extends MenuActivity {

    private GridView gridView;
    private ArrayList<String> lisCarta;
    private Socket mSocket;
    private ImageView imagenTarjeta;
    private ArrayList<Tarjeta> listaTarjetasLLegadas;
    private ArrayList<Tarjeta> listaTarjetasSeleccionadas;
    private ImageButton btnLoteria;
    private String idAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_jugando_loteria);

        Bundle datos = getIntent().getExtras();
        idAdmin = datos.getString("idAdmin");

        lisCarta = (ArrayList<String>) getIntent().getStringArrayListExtra("listaCarta");
        listaTarjetasLLegadas = new ArrayList<>();
        listaTarjetasSeleccionadas = new ArrayList<>();
        obtieneCartas();
        SocketIO app =  (SocketIO) getApplication();
        mSocket = app.getSocket();

        imagenTarjeta = (ImageView) findViewById(R.id.imageView3);
        btnLoteria = (ImageButton) findViewById(R.id.btnloteria);

        gridView = (GridView) findViewById(R.id.gridViewJugando_loteria);
        final AdapterSeleccionTarjeta ac = new AdapterSeleccionTarjeta(this, font, listaTarjetasLLegadas);
        //gridView.setAdapter(new AdapterTarjetasArmaLoteria(this));
        gridView.setAdapter(ac);
        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.nav_principal);
        SpannableString s = new SpannableString(menuItem.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.RED),0,s.length(),0);
        s.setSpan(new AbsoluteSizeSpan(100, true),0, s.length(),0);

        btnLoteria.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                ArrayList<Tarjeta> tarjetasUser = ac.getSeleccion();

                boolean esta = false;
                int contadorEsta = 0;

                for(int i = 0; i<tarjetasUser.size(); i++){
                    esta = false;
                    for(int j = 0; j<listaTarjetasSeleccionadas.size(); j++){

                        if(String.valueOf(listaTarjetasSeleccionadas.get(j).getIdtarjeta()).equalsIgnoreCase(String.valueOf(tarjetasUser.get(i).getIdtarjeta()))){
                            esta = true;
                            contadorEsta++;
                            break;
                        }
                    }
                    if(!esta){
                        break;
                    }
                }

                if(contadorEsta==lisCarta.size()){
                    Toast.makeText(JugandoLoteria.this, "Ganaste", Toast.LENGTH_SHORT).show();
                    //miId-idAdmin
                    String mensaje = String.valueOf(Persistencia.getId()).trim()+"-"+idAdmin.trim();
                    mSocket.emit("ganador",mensaje);


                }

            }
        });

        mSocket.on("envioPrimeraTarjetaU", llegaPrimeraTarjeta);
        mSocket.on("mensajeParaPerdedor", perdiste);
        mSocket.on("terminarPartidaUsuario", terminarPartida);
        mSocket.on("ganoElAdmin", ganoAdmin);

    }

    private Emitter.Listener ganoAdmin = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                Toast.makeText(JugandoLoteria.this, "Perdiste :(", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(JugandoLoteria.this, Inicio.class));
                }
            });
        }
    };


    private Emitter.Listener perdiste = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String llegada = args[0].toString().trim();
                    Usuario u = Persistencia.getUsuario(Long.parseLong(llegada));

                }
            });
        }
    };

    private Emitter.Listener llegaPrimeraTarjeta = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String llegada = args[0].toString().trim();
                    Tarjeta t = Persistencia.getTarjeta(Long.parseLong(llegada));
                    imagenTarjeta.setImageBitmap(t.getImagen());
                    listaTarjetasSeleccionadas.add(t);


                }
            });
        }
    };

    private Emitter.Listener terminarPartida = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    startActivity(new Intent(JugandoLoteria.this, Inicio.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();



                }
            });
        }
    };



    private void obtieneCartas(){
        for(String s : lisCarta){
            listaTarjetasLLegadas.add(Persistencia.getTarjeta(Long.parseLong(s.trim())));
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        startActivity(new Intent(JugandoLoteria.this, Inicio.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();

    }

}
