package com.loteria.muuch.vista.arma_loteria_admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import com.loteria.muuch.R;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.mis_tarjetas.MisCategorias;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Clase a la que se le da la informacion de la partida. El nivel, numero de jugadores e idioma.
 */
public class ListaOpciones extends MenuActivity implements AdapterView.OnItemSelectedListener {

    Spinner mNivel ;
    Spinner mJugadores;
    Spinner mIdioma;
    Spinner categorias;
    ImageButton crear;
    private String idioma1;
    private String nivel1;
    private String jugadores1;
    private String dificultad1;
    private String categoria = "Cualquier categoría";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_lista_opciones);

        idioma1 = "";
        nivel1 = "";
        jugadores1 = "";
        dificultad1 = "";

        crear = (ImageButton) findViewById(R.id.imageButton3);
        crear.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                Intent i = new Intent(ListaOpciones.this, ArmaLoteriaAdmin.class);
                i.putExtra("idioma",idioma1);
                i.putExtra("nivel", nivel1);
                i.putExtra("jugadores", jugadores1);
                i.putExtra("dificultad", dificultad1);
                i.putExtra("categoría", categoria);
                startActivity(i);
                finish();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        //Inicializamos el menú
        mIdioma = (Spinner) findViewById(R.id.idioma);
        mJugadores = (Spinner) findViewById(R.id.jugadores);
        mNivel = (Spinner) findViewById(R.id.nivel);
        categorias = (Spinner) findViewById(R.id.categorias);

        String idioma[] = {
                "Primer idioma",
                "Segundo idioma"
        };

        String jugadores[] = {
                "1",
                "2",
                "3",
                "4",
                "5",
                "6 ",

        };

        String nivel[] = {
                //"Facil +3 tarjetas",
                //"Facil +6 tarjetas",
                //"Facil +9 tarjetas"
                "Facil","Intermedio","Dificil"
        };

        final List<String> idiomaLista = new ArrayList<>(Arrays.asList(idioma));
        final List<String> jugadoresLista = new ArrayList<>(Arrays.asList(jugadores));
        final List<String> nivelLista = new ArrayList<>(Arrays.asList(nivel));
        final ArrayList<String> categoriaLista = new ArrayList<>(Arrays.asList(MisCategorias.categorias));
        categoriaLista.add(0, categoria);

        //Inicializamos un ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, idiomaLista);
        final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, jugadoresLista);
        final ArrayAdapter<String> spinnerArrayAdapter3 = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, nivelLista);
        final ArrayAdapter<String> adapterCategorias = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, categoriaLista);

        // Apply the adapter to the spinner
        mIdioma.setAdapter(spinnerArrayAdapter1);
        mIdioma.setOnItemSelectedListener(this);
        mJugadores.setAdapter(spinnerArrayAdapter2);
        mJugadores.setOnItemSelectedListener(this);
        mNivel.setAdapter(spinnerArrayAdapter3);
        mNivel.setOnItemSelectedListener(this);
        categorias.setAdapter(adapterCategorias);
        categorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                categoria = categoriaLista.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }
    public void onItemSelected(AdapterView<?> parent,
                               View view, int pos, long id) {

        idioma1 = Integer.toString(mIdioma.getSelectedItemPosition());
        if(idioma1.equals("0")){
            idioma1 = "Primer idioma";
        }else if(idioma1.equals("1")){
            idioma1 = "Segundo idioma";
        }
        jugadores1 = Integer.toString(mJugadores.getSelectedItemPosition());
        if(jugadores1.equals("0")){
            jugadores1 = "1";
        }else if(jugadores1.equals("1")){
            jugadores1 = "2";
        }else if(jugadores1.equals("2")){
            jugadores1 = "3";
        }else if(jugadores1.equals("3")){
            jugadores1 = "4";
        }else if(jugadores1.equals("4")){
            jugadores1 = "5";
        }else if(jugadores1.equals("5")){
            jugadores1 = "6";
        }
        nivel1 = Integer.toString(mNivel.getSelectedItemPosition());
        if(nivel1.equals("0") && jugadores1.equalsIgnoreCase("1")){
            nivel1 ="5";
            dificultad1 = "facil";
        }else if(nivel1.equals("0") && jugadores1.equalsIgnoreCase("2")){
            nivel1="5";
            dificultad1 = "facil";
        }else if(nivel1.equals("0") && jugadores1.equalsIgnoreCase("3")){
            nivel1="4";
            dificultad1 = "facil";
        }else if(nivel1.equals("0") && jugadores1.equalsIgnoreCase("4")){
            nivel1="4";
            dificultad1 = "facil";
        }else if(nivel1.equals("0") && jugadores1.equalsIgnoreCase("5")){
            nivel1="3";
            dificultad1 = "facil";
        }else if(nivel1.equals("0") && jugadores1.equalsIgnoreCase("6")){
            nivel1="3";
            dificultad1 = "facil";
        }else if(nivel1.equals("1") && jugadores1.equalsIgnoreCase("1")){
            nivel1="7";
            dificultad1 = "intermedio";
        }else if(nivel1.equals("1") && jugadores1.equalsIgnoreCase("2")){
            nivel1="7";
            dificultad1 = "intermedio";
        }else if(nivel1.equals("1") && jugadores1.equalsIgnoreCase("3")){
            nivel1="6";
            dificultad1 = "intermedio";
        }else if(nivel1.equals("1") && jugadores1.equalsIgnoreCase("4")){
            nivel1="5";
            dificultad1 = "intermedio";
        }else if(nivel1.equals("1") && jugadores1.equalsIgnoreCase("5")){
            nivel1="4";
            dificultad1 = "intermedio";
        }else if(nivel1.equals("1") && jugadores1.equalsIgnoreCase("6")){
            nivel1="4";
            dificultad1 = "intermedio";
        }else if(nivel1.equals("2") && jugadores1.equalsIgnoreCase("1")){
            nivel1="10";
            dificultad1 = "dificil";
        }else if(nivel1.equals("2") && jugadores1.equalsIgnoreCase("2")){
            nivel1="10";
            dificultad1 = "dificil";
        }else if(nivel1.equals("2") && jugadores1.equalsIgnoreCase("3")){
            nivel1="8";
            dificultad1 = "dificil";
        }else if(nivel1.equals("2") && jugadores1.equalsIgnoreCase("4")){
            nivel1="7";
            dificultad1 = "dificil";
        }else if(nivel1.equals("2") && jugadores1.equalsIgnoreCase("5")){
            nivel1="7";
            dificultad1 = "dificil";
        }else if(nivel1.equals("2") && jugadores1.equalsIgnoreCase("6")){
            nivel1="7";
            dificultad1 = "dificil";
        }
    }

    public void onNothingSelected(AdapterView parent) {
        // Do nothing.
    }
}