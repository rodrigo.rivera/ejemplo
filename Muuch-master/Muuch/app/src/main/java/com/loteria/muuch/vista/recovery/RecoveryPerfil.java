package com.loteria.muuch.vista.recovery;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.ingreso.EditarPerfil;
import com.loteria.muuch.vista.ingreso.IniciarSesion;
import com.loteria.muuch.vista.ingreso.Inicio;
import com.loteria.muuch.vista.ingreso.Registrarse;

import static com.loteria.muuch.modelo.Persistencia.dataState;
import static com.loteria.muuch.modelo.Persistencia.getError;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.guardaSesion;
import static com.loteria.muuch.modelo.Persistencia.online;

public class RecoveryPerfil extends Registrarse {

    @Override
    protected void initDatos() {
        super.initDatos();
        registrado = new Usuario(getUsuario());
        edit_nombre.setText(registrado.getNombre());
        edit_contraseña.setText(registrado.getContraseña());
        edit_correo.setText(registrado.getCorreo());
        setViews(false, false, continuar, fab);
        if (!online) setViews(true, false, edit_nombre, edit_correo, edit_contraseña);
        else Toast.makeText(this, "Restablece tus datos y elige el avatar que tenías, para recuperar tu cuenta.", Toast.LENGTH_LONG);
        elige_avatar.setText(R.string.avatar_previo);
        avatar = registrado.getAvatar();
        avatares.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (avatar == i) {
                    registrado.setNombre(nombre = edit_nombre.getText().toString().trim());
                    registrado.setCorreo(correo = edit_correo.getText().toString().trim());
                    registrado.setContraseña(contraseña = edit_contraseña.getText().toString().trim());
                    String restantes = sonDatosValidos();
                    if (restantes.isEmpty()) {
                        if (registrado.actualizar() && guardaSesion()) {
                            setResult(RESULT_OK);
                            if (online) confirmar(getString(R.string.recovery_ok_update),
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) { finish();
                                        }
                                    });
                            else confirmar(getString(R.string.recovery_ok),
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) { finish();
                                        }
                                    });
                        } else {
                            confirmar(getString(R.string.error_reinicio),
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) { setResult(RESULT_CANCELED); finish();
                                        }
                                    });
                        }
                    } else Toast.makeText(RecoveryPerfil.this, "Debe " + restantes + " para continuar", Toast.LENGTH_SHORT).show();
                } else {
                    confirmar(getString(R.string.perfil_falso),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }
                            });
                }
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    protected void initGuia() { }
}