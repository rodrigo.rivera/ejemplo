package com.loteria.muuch.vista.arma_loteria_admin.administra_juego;


import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.loteria.muuch.R;
import com.loteria.muuch.controlador.SocketIO;
import com.loteria.muuch.modelo.Contenido;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.arma_loteria_admin.AdapterListaInvita;
import com.loteria.muuch.vista.ingreso.Inicio;
import android.view.View;
import android.media.MediaPlayer;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Ultima clase para jugar una partida.Se muestran los usuarios que van a jugar y todas las tarjetas.
 */
public class AdministraJuego extends MenuActivity {

    public static ArrayList<String> tarjetasUsuarios1 =new ArrayList<>();
    public static ArrayList<Usuario> listIdUser1 = new ArrayList<>();
    //gridView para los jugadores
    private GridView gridView;

    //imagen de la tarjeta que toca
    private ImageView imaTarjeta;

    //boton que reproduce el nombre de la imagen
    private ImageButton btnPrimerIdioma;

    //boton que reproduce el nombre de la imagen
    private ImageButton btnDescripcionImagen;

    //boton que obtendra la sig imagen y la pondra en pantalla
    private ImageButton btnSigImagen;

    //tarjeta actual
    private Tarjeta tarjeta1;

    //sirve para reproducir audio
    private MediaPlayer mp;

    private ImageButton btnTerminar;

    private Socket mSocket;

    //cuenta el indice de la tarjeta en la que va
    private int contadorImagenes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_administra_juego);

        SocketIO app =  (SocketIO) getApplication();
        mSocket = app.getSocket();

        contadorImagenes = 0;

        btnTerminar = (ImageButton) findViewById(R.id.imageButton4);
        btnTerminar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                mSocket.emit("terminarPartida", obtenerIdJugadores(listIdUser1));

                startActivity(new Intent(AdministraJuego.this, Inicio.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                finish();


            }
        });


        //Obtenemos la primer imagen
        tarjeta1 = new Tarjeta(Long.parseLong(tarjetasUsuarios1.get(0)));
        btnSigImagen = (ImageButton) findViewById(R.id.imageButton6);
        btnSigImagen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(contadorImagenes<tarjetasUsuarios1.size()){
                    boolean ganoAdmin = ganoAdmin();
                    if(ganoAdmin){
                        Toast.makeText(AdministraJuego.this, "Ganaste", Toast.LENGTH_SHORT).show();
                        mSocket.emit("ganoAdmin", obtenerIdJugadores(listIdUser1));
                        startActivity(new Intent(AdministraJuego.this, Inicio.class));
                    }
                    tarjeta1 = new Tarjeta(Long.parseLong(tarjetasUsuarios1.get(contadorImagenes).trim()));
                    imaTarjeta.setImageBitmap(tarjeta1.getImagen());

                    mSocket.emit("envioPrimeraTarjeta", obtenerIdJugadores(listIdUser1)+","+tarjetasUsuarios1.get(contadorImagenes));

                    contadorImagenes++;
                }else{

                }
            }
        });

        //metodo que reproduce la descripcion de la imagen
        btnDescripcionImagen = (ImageButton) findViewById(R.id.descripcion_imagen);
        btnDescripcionImagen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mp = new MediaPlayer();
                Contenido c = tarjeta1.descargaCompleta();
                try {
                    if(c.getDescEsp().getAbsolutePath()==null){
                    }else {
                        mp.setDataSource(c.getDescEsp().getAbsolutePath());
                        mp.prepare();
                        mp.start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //Boton para reproducir el nombre de la imagen
        btnPrimerIdioma = (ImageButton) findViewById(R.id.nombre_idioma);
        btnPrimerIdioma.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mp = new MediaPlayer();
                Contenido c = tarjeta1.descargaCompleta();
                try {
                    if(c.getDescEsp().getAbsolutePath()==null){
                    }else {
                        //mp.setDataSource(c.getDescEsp().getAbsolutePath());
                        mp.setDataSource(c.getNombreEsp().getAbsolutePath());
                        mp.prepare();
                        mp.start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //Se pone en la parte de la imagen la primera imagen de la lista de todas las imagenes
        imaTarjeta = (ImageView) findViewById(R.id.imageView3);
        //imaTarjeta.setImageBitmap(tarjeta1.getImagen());

        //GridView donde añadimos a los usuarios que estan jugando
        gridView = (GridView) findViewById(R.id.gridView_administra_juego);
        gridView.setAdapter(new AdapterListaInvita(this, listIdUser1));

        //Se envia la primera tarjeta
        //mSocket.emit("envioPrimeraTarjeta", obtenerIdJugadores(listIdUser1)+","+tarjetasUsuarios1.get(0));
        mSocket.on("mensajeAdminGanador", llegoGanador);
    }

    private Emitter.Listener llegoGanador = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String idGanador = args[0].toString().trim();
                    ArrayList<Usuario> listaUsuariosPerdedores = new ArrayList<>();
                    Usuario use = Persistencia.getUsuario(Long.parseLong(idGanador));
                    Toast.makeText(AdministraJuego.this, "Gano: "+use.getNombre(), Toast.LENGTH_SHORT).show();
                    for(Usuario u : listIdUser1){
                        if( !String.valueOf(u.getIdusuario()).trim().equals(idGanador) ){
                            listaUsuariosPerdedores.add(u);
                            String mensaje = String.valueOf(u.getIdusuario()).trim()+"-"+idGanador;
                            mSocket.emit("jugadoresPerdedores",mensaje);
                        }
                    }

                }
            });
        }
    };

    /**
     * Metodo para obtener el id de los usuarios que estan jugando
     * @param listaUsuario los usuarios que estan jugando
     * @return cadena con el id de los jugadores que estan jugando
     */
    private String obtenerIdJugadores(ArrayList<Usuario> listaUsuario){

        String salida = "";

        for(int i = 0; i<listaUsuario.size(); i++){

            if(i==0){
                salida = String.valueOf(listaUsuario.get(i).getIdusuario());
            }else if(i==listaUsuario.size()-1){
                salida = salida+"-"+String.valueOf(listaUsuario.get(i).getIdusuario());
            }else{
                salida = salida +"-"+String.valueOf(listaUsuario.get(i).getIdusuario());
            }
        }

        return salida;
    }


    /**
     * Metodo que solo funciona cuando hay un solo jugador. Da el triunfo a la maquina si no lo ha
     * ganado el usurio
     * @return
     */
    private boolean ganoAdmin(){

        int gano =(int) Math.floor(Math.random()*10+1);

        if(contadorImagenes==7){
            if(gano<2){
                return true;
            }
        }else if(contadorImagenes == 8){
            if(gano<4){
                return true;
            }
        }else if(contadorImagenes == 9){
            if(gano<6){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        startActivity(new Intent(AdministraJuego.this, Inicio.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }
}