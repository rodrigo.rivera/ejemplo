package com.loteria.muuch.vista.crea_tarjeta;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.mis_tarjetas.MisCategorias;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.categorias;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.initResources;

/**
 * Created by PATLANIUNAM on 23/11/2017.
 */

public class AdapterCategoria extends BaseAdapter{

    Context c;
    int[] resources;
    String[] nombres;
    Typeface font;
    ImageView imagen;
    TextView nombre;

    /**
     * Método por defecto, que incluye todas las categorias.
     * @param font Tipografía con que se muestran las categorías.
     */
    public AdapterCategoria(Context c, Typeface font) {
        this.c = c;
        this.font = font;
        this.resources = initResources();
        this.nombres = categorias;
    }

    /**
     * Método por defecto, que incluye las categorias de los indices especificados.
     * @param font Tipografía con que se muestran las categorías.
     * @param indices Indices que se incluyen en la lista.
     */
    public AdapterCategoria(Context c, Typeface font, int ... indices) {
        this.c = c;
        this.font = font;
        int todos[] = initResources(), n = indices.length;
        this.resources = new int[indices.length];
        this.nombres = new String [resources.length];
        for(int i = 0; i < n; i++) {
            this.resources[i] = todos[indices[i]];
            this.nombres[i] = categorias[indices[i]];
        }
    }

    /**
     * Método por defecto, que incluye todas las categorias y las que se incluyan como parámetro.
     * @param font Tipografía con que se muestran las categorías.
     * @param extra Etiquetas extra que se incluyen al final en la lista.
     */
    public AdapterCategoria(Context c, Typeface font, String ... extra) {
        this.c = c;
        this.font = font;
        int todos[] = initResources(), n = extra.length;
        this.resources = new int[todos.length + n];
        this.nombres = new String [resources.length];
        for(int i = 0; i < extra.length; i++) {
            this.resources[i] = -1;
            this.nombres[i] = extra[i];
        }
        for(int i = 0; i < todos.length; i++) {
            this.resources[n + i] = todos[i];
            this.nombres[n + i] = categorias[i];
        }
    }

    @Override
    public int getCount() {
        return nombres.length;
    }

    @Override
    public Object getItem(int i) {
        return nombres[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null){
            LayoutInflater inflater = LayoutInflater.from(c);
            view  =inflater.inflate(R.layout.adapter_categoria, null);
        } final TextView nombre = (TextView) view.findViewById(R.id.tv_adapter_categoria);
        nombre.setText(nombres[i]);
        final ImageView avatar = (ImageView) view.findViewById(R.id.iv_adapter_categoria);
        avatar.post(new Runnable() {
            @Override
            public void run() {
                if(resources[i] >= 0) avatar.setImageResource(resources[i]);
                nombre.setTypeface(font);
            }
        });
        return view;
    }
}