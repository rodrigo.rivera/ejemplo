package com.loteria.muuch.vista.arma_loteria_admin.jugando_admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.controlador.SocketIO;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.arma_loteria_admin.TarjetasPorUsuario;
import com.loteria.muuch.vista.arma_loteria_admin.administra_juego.AdministraJuego;
import com.loteria.muuch.vista.arma_loteria_admin.revisarTarjetas.RevisarTarjetasUsuarios;
import com.loteria.muuch.vista.mis_amigos.AmigoAdapter;
import static com.loteria.muuch.vista.arma_loteria_admin.TarjetasPorUsuario.enviadas;
import static com.loteria.muuch.vista.arma_loteria_admin.revisarTarjetas.RevisarTarjetasUsuarios.listaIdTarjetas;
import static com.loteria.muuch.vista.arma_loteria_admin.administra_juego.AdministraJuego.listIdUser1;
import static com.loteria.muuch.vista.arma_loteria_admin.administra_juego.AdministraJuego.tarjetasUsuarios1;
import java.util.ArrayList;
import java.util.Collections;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/*
Clase que muestra los usuarios que aceptaron jugar la partida. Esta clase contiene dos botones uno
nos lleva a iniciar la partida y el otro nos muestra las tarjetas de los usuarios
 */
public class JugandoAdmin extends MenuActivity {

    private GridView gridView;
    public static ArrayList<String> tarjetasUsuarios = new ArrayList<>();
    public static ArrayList<Usuario> listIdUser = new ArrayList<>();
    private ImageButton btnRevizarTarjetas;
    private ImageButton btnIniciarPartida;
    public static String nivel1, idioma1, jugadores1, dificultad1;
    public Socket mSocket;
    private int contadorCartasRecibidas;
    private ArrayList<String> listaCartasRecibidas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_jugando_admin);

        listaCartasRecibidas = new ArrayList();
        contadorCartasRecibidas = 0;
        SocketIO app =  (SocketIO) getApplication();
        mSocket = app.getSocket();

        btnIniciarPartida = (ImageButton) findViewById(R.id.imageButton);
        btnIniciarPartida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crearCarta();

            }
        });

        btnRevizarTarjetas = (ImageButton) findViewById(R.id.imageButton2);
        btnRevizarTarjetas.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                listaIdTarjetas = tarjetasUsuarios;
                startActivity( new Intent(JugandoAdmin.this, RevisarTarjetasUsuarios.class));

            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(JugandoAdmin.this, AdministraJuego.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();

            }
        });
        gridView = (GridView) findViewById(R.id.gridView_jugando_admin);
        if(gridView != null) {
            AmigoAdapter ali = new AmigoAdapter(this, font, listIdUser);
            gridView.setAdapter(ali);
            // Definimos la acción al tocar un amigo que aceptó y envió sus tarjetas.
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    // COn la posición obtenemos el usuario que seleccionó y su id.
                    long id = listIdUser.get(i).getIdusuario().longValue();
                    // El id lo ocupamos para decidir si agregamos una tarjeta a la sig. lista.
                    // Inicializamos la lista de tarjetas filtradas por usuario.
                    enviadas = new ArrayList<Tarjeta>();
                    for (String s: tarjetasUsuarios) {
                        // Obtenemos la tarjeta con el string de su id.
                        Tarjeta t = Persistencia.getTarjeta(new Long(s.trim()));
                        long k = t.getIdusuario();
                        // Si el usuario de la tarjeta es igual al seleccionado se agrega.

                        if (id == k) enviadas.add(t);
                    } // Iniciamos la activity que muestra tarjetas filtradas.
                    startActivity(new Intent(JugandoAdmin.this, TarjetasPorUsuario.class));


                }
            });
        } else {}

        mSocket.on("cartaRecibidaCorrectamente", llegaCartaCorrectamente);
    }


    /**
     * Metodo que crear la carta para todos los usuarios y se las envia a los usuarios
     */
    private void crearCarta(){
        ArrayList<ArrayList<String>> listaTotal = new ArrayList<>();
        ArrayList<String> listaTemporal = new ArrayList<>();
        int numerosUser = Integer.parseInt(jugadores1);
        //int numeroTarjetas = tarjetasUsuarios.size()-1;
        boolean iguales = false;
        boolean esta = false;

        int tamCarta =0;
        if(dificultad1.equalsIgnoreCase("facil")){
            tamCarta=6;
        }else if(dificultad1.equalsIgnoreCase("intermedio")){
            tamCarta=9;
        }else if(dificultad1.equalsIgnoreCase("dificil")){
            tamCarta=16;
        }

        //por si solo entra a jugar un solo jugador
        if(numerosUser == 1){

            //id de las tarjetas de un usuario Universal
            ArrayList<String> idTarjetasUniversal = new ArrayList<>();
            idTarjetasUniversal.add("32");
            idTarjetasUniversal.add("33");
            idTarjetasUniversal.add("34");
            idTarjetasUniversal.add("35");
            idTarjetasUniversal.add("36");
            for(int i = 0; i<idTarjetasUniversal.size(); i++){
                tarjetasUsuarios.add(idTarjetasUniversal.get(i));
            }
        }
        Toast.makeText(JugandoAdmin.this, "las tarjetas son: "
                +tarjetasUsuarios.size(), Toast.LENGTH_SHORT).show();
        /*
        Ciclo que se ejecuta siempre que la lista de Cartas sea menor o igual al numero de usuarios
         */
        while (listaTotal.size() < numerosUser) {
            //lista temporal que guarda la carta en turno
            listaTemporal.clear();
            /*
            Ciclo que se ejecuta siempre que la lista donde se va guardar la carta sea menor que el tamaño de la carta
             */
            while (listaTemporal.size() < tamCarta) {
                //se escoje un numero aleatorio que sera la tarjeta que se añadira a la carta
                int random = (int) (Math.random() * tarjetasUsuarios.size());


                //falso si la tarjeta que se selecciona no esta en la carta; true en otro caso
                esta = true;
                /*
                Ciclo que sirve para recorrer la lista de tarjetas, revisando por si se repite una tarjeta en la carta
                 */
                for (int i = 0; i < listaTemporal.size(); i++) {
                    if (tarjetasUsuarios.get(random).equalsIgnoreCase(listaTemporal.get(i))) {
                        esta = false;
                    }
                }
                //guarda la tarjeta en la lista de la carta si no se encuentra en ella
                if (esta) {
                    listaTemporal.add(tarjetasUsuarios.get(random));
                }
            }

            ArrayList<String> listaTemporalDesordenada = new ArrayList<>();
            for(String s : listaTemporal){
                listaTemporalDesordenada.add(s);
            }
            //ordena la lista de la carta
            //Collections.sort(listaTemporal);
            Collections.sort(listaTemporal);


            String listaTeT = listaTemporal.toString();
            iguales = true;
            for (int i = 0; i < listaTotal.size(); i++) {

                String listaTT = listaTotal.get(i).toString();
                if(listaTeT.equalsIgnoreCase(listaTT)){
                    iguales=false;
                }
            }
            if(iguales){
                listaTotal.add(listaTemporalDesordenada);
            }
        }

        //se envia las cartas a los usuarios.
        for(int j = 0; j<listaTotal.size(); j++){
            Usuario u = listIdUser.get(j);
            //idAdmin-idUser-CartaUser
            String cartaId = String.valueOf(String.valueOf(Persistencia.getId())+"-"+u.getIdusuario())+"-"+listaTotal.get(j).toString();
            mSocket.emit("envioCarta", cartaId);
        }
    }

    private Emitter.Listener llegaCartaCorrectamente = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String mensajeLlegada = args[0].toString().trim();

                    boolean esta = true;

                    for(String s : listaCartasRecibidas) {
                        if(s.equalsIgnoreCase(mensajeLlegada)){
                            esta = false;
                        }
                    }

                    if(esta){
                        listaCartasRecibidas.add(mensajeLlegada);
                        contadorCartasRecibidas++;
                    }

                    if(contadorCartasRecibidas == Integer.parseInt(jugadores1)){
                        String usuariosCartasRecibidas = listaCartasRecibidas.toString();
                        usuariosCartasRecibidas = usuariosCartasRecibidas.replaceAll("\\[","");
                        usuariosCartasRecibidas = usuariosCartasRecibidas.replaceAll("]","").trim();
                        listIdUser1 = listIdUser;
                        tarjetasUsuarios1 = tarjetasUsuarios;
                        mSocket.emit("todasCartasRecibidas", usuariosCartasRecibidas);

                        Intent i = new Intent(JugandoAdmin.this, AdministraJuego.class);
                        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();


                    }

                }
            });
        }
    };


}