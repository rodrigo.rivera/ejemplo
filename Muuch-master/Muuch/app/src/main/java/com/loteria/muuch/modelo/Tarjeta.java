package com.loteria.muuch.modelo;

import android.graphics.Bitmap;

import java.io.File;
import java.io.Serializable;

import static com.loteria.muuch.modelo.Contenido.descargarImagen;
import static com.loteria.muuch.modelo.Contenido.descargarOriginal;
import static com.loteria.muuch.modelo.Persistencia.getBitMap;
import static com.loteria.muuch.modelo.Persistencia.getTarjeta;

/**
 * @author Luis Angel Patlani Aguilar
 * Clase que representa a una tarjeta en la Base de Datos.
 */
public class Tarjeta implements Serializable {

    /** Identificador de la tarjeta usado en el nombre de sus recursos. */
    private Long idtarjeta;
    /** Nombre escrito en su lenguaje original. */
    private String nombreLen;
    /** Nombre escrito en el idioma Español. */
    private String nombreEsp;
    /** Catgoría de la tarjeta. */
    private Integer categoria;
    /** Datos de los archivos de la tarjeta. */
    private Contenido datos;
    /** Propietario de la tarjeta. */
    private Long idusuario;
    /** Indica si se ha sincronizado. */
    private boolean inServer = true, old = false;
    /** Variable que asegura la consistencia en la serialización. */
    private static final long serialVersionUID = 1L;

    public Tarjeta(){}

    /**
     * Constructor de una Tarjeta con su identificador.
     * @param idtarjeta Identificador de la tarjeta.
     */
    public Tarjeta(Long idtarjeta) {
        copiar(getTarjeta(idtarjeta));
    }

    /**
     * Mëtodo que copia los datos de una tarjeta.
     * @param otra Tarjeta de la que se copian los datos.
     * @return Devuelve la tarjeta con los datos copiados.
     */
    public Tarjeta copiar(Tarjeta otra) {
        this.idtarjeta = otra.getIdtarjeta();
        this.nombreLen = otra.getNombreLen();
        this.nombreEsp = otra.getNombreEsp();
        this.categoria = otra.getCategoria();
        this.idusuario = otra.getIdusuario();
        this.datos = otra.getDatos();
        this.inServer = otra.isInServer();
        this.old = otra.isOld();
        return this;
    }

    /**
     * Constructor de una Tarjeta sin identificador con el id del usuario ingresado.
     * @param nombreLen Nombre registrado en su lenguaje original.
     * @param nombreEsp Nombre registrado en el idioma Español.
     * @param categoria Nombre registrado de la categoria.
     */
    public Tarjeta(String nombreLen,String nombreEsp, Integer categoria) {
        this.nombreLen = nombreLen;
        this.nombreEsp = nombreEsp;
        this.categoria = categoria;
        this.idusuario = Persistencia.getUsuario().getIdusuario();
        this.datos = new Contenido();
    }

    /**
     * Constructor de una Tarjeta con todos sus parámetros.
     * @param idTarjeta Identificado de la tarjeta.
     * @param nombreLen Nombre registrado en su lenguaje original.
     * @param nombreEsp Nombre registrado en el idioma Español.
     * @param categoria Nombre registrado de la categoria.
     * @param u Usuario que registra la tarjeta.
     */
    public Tarjeta(Long idTarjeta, String nombreLen,String nombreEsp, Integer categoria, Usuario u) {
        this.idtarjeta = idTarjeta;
        this.nombreLen = nombreLen;
        this.nombreEsp = nombreEsp;
        this.categoria = categoria;
        this.idusuario = u.getIdusuario();
        this.datos = new Contenido();
    }

    /**
     * Constructor de una Tarjeta con todos sus parámetros.
     * @param idTarjeta Identificado de la tarjeta.
     * @param nombreLen Nombre registrado en su lenguaje original.
     * @param nombreEsp Nombre registrado en el idioma Español.
     * @param categoria Nombre registrado de la categoria.
     * @param idusuario Id del Usuario que registra la tarjeta.
     */
    public Tarjeta(Long idTarjeta, String nombreLen,String nombreEsp, Integer categoria, long idusuario) {
        this.idtarjeta = idTarjeta;
        this.nombreLen = nombreLen;
        this.nombreEsp = nombreEsp;
        this.categoria = categoria;
        this.idusuario = idusuario;
        this.datos = new Contenido();
    }

    public Long getIdtarjeta() {
        return idtarjeta;
    }

    public void setIdtarjeta(Long idtarjeta) {
        this.idtarjeta = idtarjeta;
    }

    public String getNombreLen() {
        return nombreLen;
    }

    public void setNombreLen(String nombreLen) {
        this.nombreLen = nombreLen;
    }

    public String getNombreEsp() {
        return nombreEsp;
    }

    public void setNombreEsp(String nombreEsp) {
        this.nombreEsp = nombreEsp;
    }

    public Integer getCategoria() { return categoria; }

    public void setCategoria(Integer categoria) { this.categoria = categoria; }

    public Long getIdusuario() { return idusuario; }

    public void setIdUsuario(Usuario usuario) { this.idusuario = usuario.getIdusuario(); }

    public boolean isInServer() { return inServer; }

    public void setInServer(boolean inServer) { this.inServer = inServer; }

    public boolean isOld() { return old; }

    public void setOld(boolean old) { this.old = old; }

    public Contenido getDatos() { return datos; }

    public Contenido descargaDatos() {
        return datos = Contenido.descargarDatos(this);
    }

    public Contenido descargaCompleta() { return datos = Contenido.descargarDatos(this, true); }

    public boolean respaldar() {
        File[] respaldo = Contenido.respaldo(this);
        if (respaldo == null) return false;
        datos = new Contenido(respaldo);
        return true;
    }

    public Bitmap getImagen() { return getBitMap(descargarImagen(this)); }

    public Bitmap getOriginal() { return getBitMap(descargarOriginal(this)); }

    public Usuario getUsuario() { return Persistencia.getUsuario(idusuario); }

    public boolean insertar() { return Persistencia.insertaTarjeta(this); }

    public boolean actualizar() {
        return Persistencia.actualizaTarjeta(this);
    }

    public boolean eliminar() {
        return Persistencia.eliminaTarjeta(idtarjeta);
    }

    @Override
    public String toString() {
        return "\nIdtarjeta: " + idtarjeta
                + "\t Nombre original:\t" + nombreLen
                + "\t Nombre en Español:\t" + nombreEsp
                + "\t Categoría:\t" + categoria
                + "\t Idusuario:\t" + idusuario;
    }

    @Override
    public boolean equals(Object otra) {
        Tarjeta t = (Tarjeta) otra;
        if (t == null) return false;
        return idtarjeta.longValue() == t.getIdtarjeta().longValue();
    }
}
