package com.loteria.muuch.vista;

import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import com.loteria.muuch.R;

import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.vista.ingreso.IniciarSesion;
import com.loteria.muuch.vista.ingreso.Inicio;

import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.cargaSesion;

public class Launcher extends AppCompatActivity {
    private final int DURACION_SPLASH = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        StrictMode.setThreadPolicy(new StrictMode.
                ThreadPolicy.Builder().permitAll().build());
        setContentView(R.layout.activity_launcher);
        Persistencia.init(getApplicationContext());

        new Handler().postDelayed(new Runnable(){
            public void run(){
                Persistencia.init(getApplicationContext());
                if (cargaSesion()) startActivity(new Intent(Launcher.this,Inicio.class));
                else startActivity(new Intent(getApplicationContext(),IniciarSesion.class));
                finish();
            };
        }, DURACION_SPLASH);
    }
}
