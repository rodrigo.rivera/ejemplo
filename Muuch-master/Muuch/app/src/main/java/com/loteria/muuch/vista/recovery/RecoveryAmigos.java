package com.loteria.muuch.vista.recovery;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.mis_amigos.AmigoAdapter;
import com.loteria.muuch.vista.mis_amigos.MisAmigos;

import java.util.ArrayList;
import java.util.Random;

import static com.loteria.muuch.modelo.Persistencia.eliminaAmigo;
import static com.loteria.muuch.modelo.Persistencia.getAmigos;
import static com.loteria.muuch.modelo.Persistencia.getAvatar;
import static com.loteria.muuch.modelo.Persistencia.getNombre;
import static com.loteria.muuch.modelo.Persistencia.getSugerencias;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.guardaSesion;
import static com.loteria.muuch.modelo.Persistencia.insertaAmigo;
import static com.loteria.muuch.modelo.Persistencia.online;

public class RecoveryAmigos extends MisAmigos {

    public int reconocidos;
    private boolean primero = true;
    private Usuario amigo;
    private int r, p;
    private ArrayList<Usuario> elegidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        amigos = getUsuario().getAmigos();
        sugerencias = getSugerencias();
        reconocidos = amigos.size();
        if (reconocidos > 3) reconocidos = 3;
        elegidos = new ArrayList<>();
        r = new Random().nextInt(amigos.size());
        if (sugerencias.isEmpty()) p = 0;
        else p = new Random().nextInt(sugerencias.size());
        super.onCreate(savedInstanceState);
        nombre_usuario.setText(getNombre());
        fab.setImageResource(getAvatar());
    }

    /**
     * Método que incializa las listas de amigos y sugerencias.
     */
    @Override
    public void initDatos() {
        if (primero) {
            primero = false;
            confirmar(getString(R.string.recovery_amigos).replace("?", "" + reconocidos));
        }
        gv_superior.setAdapter(new AmigoAdapter(this, font, elegidos));
        r = (r + 1) % amigos.size();
        if (!sugerencias.isEmpty()) p = (p + 1) % sugerencias.size();
        amigo = amigos.get(r);
        sugerencias.add(p, amigo);
        tv_otros.setText(R.string.amigo_previo);
        gv_inferior.setAdapter(new AmigoAdapter(this, font, sugerencias));
        gv_inferior.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Usuario elegido = sugerencias.remove(i);
                if (elegido.equals(amigo)) {
                    reconocidos--;
                    elegidos.add(elegido);
                    initDatos();
                    if (reconocidos == 0) {
                        if (guardaSesion()) {
                            confirmar(getString(R.string.recovery_ok),
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            setResult(RESULT_OK);
                                            finish();
                                        }
                                    });
                        } else confirmar(getString(R.string.error_reinicio),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) { setResult(RESULT_CANCELED); finish();
                                    }
                                });
                    }
                } else {
                    confirmar(getString(R.string.amigo_falso).replace("(?)", elegido.getNombre()),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    setResult(RESULT_CANCELED);
                                    finish();}
                            });
                }
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    protected void filtrar(String s) {}
}