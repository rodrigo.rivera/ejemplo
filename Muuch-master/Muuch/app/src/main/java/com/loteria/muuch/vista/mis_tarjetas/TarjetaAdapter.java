package com.loteria.muuch.vista.mis_tarjetas;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;

import java.util.ArrayList;

import static com.loteria.muuch.modelo.Contenido.descargarImagen;
import static com.loteria.muuch.modelo.Contenido.setBitmap;

/**
 * Created by PATLANIUNAM on 01/10/2017.
 */

public class TarjetaAdapter extends BaseAdapter {

    protected Context contexto;
    protected ArrayList<Tarjeta> lista;
    protected Typeface font;
    protected int height;

    public TarjetaAdapter(Context contexto, Typeface font, ArrayList<Tarjeta> lista) {
        this.contexto = contexto;
        this.lista = lista;
        this.font = font;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null){
            LayoutInflater inflater = LayoutInflater.from(contexto);
            view  =inflater.inflate(R.layout.adapter_tarjeta, null);
        }
        final Tarjeta actual = lista.get(i);
        String esp = actual.getNombreEsp(), len = actual.getNombreLen();
        float sizeEsp = esp.length() / 15f, sizeLen = len.length() / 15f;
        final TextView nesp = (TextView) view.findViewById(R.id.adapter_nesp);
        if (sizeEsp > 1) nesp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f / sizeEsp);
        nesp.setText(esp); nesp.setTypeface(font);
        final TextView nlen = (TextView) view.findViewById(R.id.adapter_nlen);
        if (sizeLen > 1) nlen.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f / sizeLen);
        nlen.setText(len); nlen.setTypeface(font);
        final ImageView imagen = (ImageView) view.findViewById(R.id.adapter_imagen);
        imagen.postDelayed(new Runnable() {
            @Override
            public void run() {
                height = (int)(imagen.getWidth() * (3 / 2.5));
                imagen.getLayoutParams().height = height; imagen.requestLayout();
                if (height == 0) return;
                setBitmap(false, actual.getImagen(), imagen, imagen.getWidth(), height);
            }
        }, 0);
        return view;
    }
}