package com.loteria.muuch.vista;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;
import android.view.Display;
import android.view.DragEvent;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.controlador.SocketIO;
import com.loteria.muuch.modelo.LocalServer;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.ingreso.AvatarAdapter;
import com.loteria.muuch.vista.ingreso.EditarPerfil;
import com.loteria.muuch.vista.ingreso.IniciarSesion;
import com.loteria.muuch.vista.ingreso.Inicio;
import com.loteria.muuch.vista.ingreso.Registrarse;

import org.w3c.dom.Text;

import io.socket.client.Socket;

import static com.loteria.muuch.modelo.Persistencia.actualiza;
import static com.loteria.muuch.modelo.Persistencia.cargaSesion;
import static com.loteria.muuch.modelo.Persistencia.dataState;
import static com.loteria.muuch.modelo.Persistencia.getAvatar;
import static com.loteria.muuch.modelo.Persistencia.getNombre;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.localServer;
import static com.loteria.muuch.modelo.Persistencia.login;
import static com.loteria.muuch.modelo.Persistencia.online;
import static com.loteria.muuch.modelo.Persistencia.recovery;
import static com.loteria.muuch.modelo.Persistencia.terminaSesion;
import static com.loteria.muuch.modelo.Persistencia.getId;

/**
 * Clase genérica para activities con menú hamburguesa.
 */
public class MenuActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {

    protected DrawerLayout drawer;
    protected NavigationView navigationView;
    protected Toolbar toolbar;
    protected FloatingActionButton fab;
    protected TextView nombre_usuario;
    protected Typeface font;
    protected boolean esLaPrincipal;
    protected static boolean monitorMode = false;
    protected static final int EDITAR_PERFIL = 2;
    protected static SharedPreferences datos;

    protected void init(int layout) {
        setContentView(layout);
        if (monitorMode) dataState(this.getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu m = navigationView .getMenu();
        font = Typeface.createFromAsset(getAssets(), "fonts/VAG Rounded Bold.ttf");
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);
            SpannableString s = new SpannableString(mi.getTitle());
            s.setSpan(new CustomTypefaceSpan("", font), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mi.setTitle(s);
        } setFontTitle(getTitle());
        esLaPrincipal = false;
        fab = (FloatingActionButton) findViewById(R.id.fab);
        nombre_usuario = (TextView)findViewById(R.id.nombre_usuario);
        if (cargaSesion()) {
            fab.setImageResource(getAvatar());
            fab.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    startActivityForResult(new Intent(getApplicationContext(), EditarPerfil.class), EDITAR_PERFIL);
                    return true;
                }
            });
            if (nombre_usuario == null) return;
            nombre_usuario.setText(getNombre());
            setFont(nombre_usuario);
        }
        datos = this.getSharedPreferences("PREFERENCIAS", MODE_PRIVATE);
        initGuia();
        actualiza = true;
    }

    protected void initDatos() {}

    protected void initGuia() {}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDITAR_PERFIL && resultCode == RESULT_OK) {
            fab.setImageResource(getAvatar());
            nombre_usuario.setText(getNombre());
        } else initDatos();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Context c = getApplicationContext();
        boolean sale = false;
        switch (item.getItemId()) {
            case R.id.nav_ayuda: //case R.id.login_ayuda:
                startActivity(new Intent(c, Ayuda.class)); break;
            case R.id.nav_perfil:
                startActivityForResult(new Intent(c, EditarPerfil.class), EDITAR_PERFIL); break;
            case R.id.nav_principal:
                if (!esLaPrincipal) startActivity(new Intent(c, Inicio.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)); break;
            case R.id.nav_salir:
                if (!localServer.clean(getApplicationContext())) break;
                if (!terminaSesion()) break;
                SocketIO app =  (SocketIO) getApplication();
                Socket mSocket = app.getSocket();
                mSocket.disconnect();
                Intent i = (esLaPrincipal)? new Intent(c, IniciarSesion.class): new Intent(c, Inicio.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i); sale = true; break;
            case R.id.nav_muuch: //case R.id.login_sobre_muuch:
                startActivity(new Intent(c, SobreMuuch.class)); break;
            default: return false;
        } drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        if (sale) finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    protected Dialog avisar(String aviso) {return avisar(new Dialog(this), aviso); }
    protected Dialog avisar(Dialog ventana, String aviso) {
        // Quita el título por defecto, visible en dispositivos con Android anterior a KitKat.
        ventana.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Usa la plantilla de diálogo personalizada en los layouts.
        ventana.setContentView(R.layout.view_alerta);
        // Hace transparente el fondo, para no influir en el diseño personalzado del dialogo.
        ventana.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Se extrae el componente que contiene el texto de aviso, para modificarlo.
        TextView tv_aviso = (TextView)ventana.findViewById(R.id.tv_aviso);
        tv_aviso.setText(aviso); // Se cambia el texto del aviso.
        setFont(tv_aviso); // Se ajusta la tipografía.
        ventana.show(); // Se muestra el aviso.
        ventana.setCancelable(false); // Se impide ocultar el diálogo con la flecha hacia atrás.
        ventana.setCanceledOnTouchOutside(false); // Se impide ocultar el diálogo, tocando fuera del cuadro.
        return ventana; // Se devuelve la ventana del diálogo.
    }

    protected TextView confirmar(String aviso) {
        return confirmar(aviso, new View.OnClickListener() {
            @Override
            public void onClick(View view) {}
        });
    }

    protected TextView confirmar(String aviso, final View.OnClickListener action) {
        final Dialog ventana = new Dialog(this);
        avisar(ventana, aviso);
        TextView confirmar = (TextView)ventana.findViewById(R.id.tv_continuar);
        setFont(confirmar); // Se ajusta la tipografía.
        confirmar.setVisibility(View.VISIBLE);
        confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { action.onClick(view); ventana.dismiss(); }
        }); return confirmar;
    }

    protected TextView preguntar(String aviso, final View.OnClickListener action) {
        final Dialog ventana = new Dialog(this);
        avisar(ventana, aviso);
        ventana.findViewById(R.id.ly_alerta_respuestas).setVisibility(View.VISIBLE);
        TextView aceptar = (TextView)ventana.findViewById(R.id.tv_aceptar);
        TextView cancelar = (TextView)ventana.findViewById(R.id.tv_cancelar);
        setFont(aceptar, cancelar); // Se ajusta la tipografía.
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { ventana.dismiss();}
        });
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { action.onClick(view); ventana.dismiss(); }
        }); return aceptar;
    }

    public void setViews(boolean visible, boolean enabled, View ... views) {
        for (View v : views) {
            v.setVisibility((visible)? View.VISIBLE: View.INVISIBLE);
            v.setEnabled(enabled);

        }
    }

    public void setFont(TextView ... v) { for (TextView tv: v) tv.setTypeface(font);}

    public void setFontTitle(int resId){setFontTitle(getString(resId));}

    public void setFontTitle(CharSequence title) {
        Spannable stitle = new SpannableString(title);
        stitle.setSpan(new CustomTypefaceSpan("", font), 0, stitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTitle(stitle);
    }

    public static class CustomTypefaceSpan extends TypefaceSpan {

        public static Parcelable CREATOR;
        private final Typeface newType;

        public CustomTypefaceSpan(String family, Typeface type) {
            super(family);
            newType = type;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            applyCustomTypeFace(ds, newType);
        }

        @Override
        public void updateMeasureState(TextPaint paint) {
            applyCustomTypeFace(paint, newType);
        }

        private static void applyCustomTypeFace(Paint paint, Typeface tf) {
            int oldStyle;
            Typeface old = paint.getTypeface();
            if (old == null) {
                oldStyle = 0;
            } else {
                oldStyle = old.getStyle();
            }
            int fake = oldStyle & ~tf.getStyle();
            if ((fake & Typeface.BOLD) != 0) {
                paint.setFakeBoldText(true);
            }
            if ((fake & Typeface.ITALIC) != 0) {
                paint.setTextSkewX(-0.25f);
            }
            paint.setTypeface(tf);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Persistencia.init(getApplicationContext());
        cargaSesion();
    }
}