package com.loteria.muuch.vista.mis_tarjetas;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.crea_tarjeta.ConsultaTarjeta;
import com.loteria.muuch.vista.crea_tarjeta.CreaTarjeta;
import com.loteria.muuch.vista.crea_tarjeta.EditaTarjeta;

import java.util.ArrayList;

import static android.widget.Toast.makeText;
import static com.loteria.muuch.modelo.Persistencia.actualiza;
import static com.loteria.muuch.modelo.Persistencia.getColeccion;
import static com.loteria.muuch.modelo.Persistencia.getNombre;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.categorias;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.coleccionadas;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.compartidas;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.resources;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.seleccionada;

public class MisTarjetas extends MenuActivity {

    protected GridView gv_superior, gv_inferior;
    protected ArrayList<Tarjeta> tarjetas, otras;
    protected ArrayList<Tarjeta> edicion, consulta;
    protected SearchView buscador;
    protected boolean isUpdate;
    public static String query = "";
    public static final int EDICION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getUsuario() == null) { finish(); return; }
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_mis_tarjetas);
        gv_superior = (GridView) findViewById(R.id.gridViewMisTarjetas);
        gv_inferior = (GridView) findViewById(R.id.gridViewOtrasTarjetas);
        TextView tv_categoria = (TextView)findViewById(R.id.tv_categoria);
        ImageView iv_categoria = (ImageView)findViewById(R.id.iv_categoria);
        setFont((TextView)findViewById(R.id.tv_mis_tarjetas),
                (TextView)findViewById(R.id.tv_otras_tarjetas),
                tv_categoria);
        if (seleccionada >= 0) {
            tv_categoria.setText(categorias[seleccionada]);
            iv_categoria.setImageResource(resources[seleccionada]);
        } else setViews(false, false, iv_categoria, tv_categoria);
        buscador = (SearchView)findViewById(R.id.searchView);
        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {return false;}

            @Override
            public boolean onQueryTextChange(String s) {
                filtrar(s);
                return false;
            }
        });
        initDatos();
        isUpdate = true;
    }

    @Override
    protected void initDatos() {
        setListas(tarjetas = coleccionadasFiltradas(), otras = compartidas);
        initGridViews(gv_superior, gv_inferior);
    }

    protected ArrayList<Tarjeta> coleccionadasFiltradas() {
        if (!isUpdate) return MisCategorias.coleccionadas;
        ArrayList<Tarjeta> coleccion = getColeccion();
        int n = categorias.length;
        MisCategorias.coleccionadas = new ArrayList<>();
        for (Tarjeta t: coleccion) {
            if (t.getCategoria() == seleccionada) coleccionadas.add(t);
        } return coleccionadas;
    }

    protected void initGridViews(GridView gv_edicion, GridView gv_consulta) {
        gv_edicion.setAdapter(new TarjetaAdapter(this, font, edicion));
        gv_edicion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                EditaTarjeta.creada = edicion.get(i);
                abrirTarjeta(true);
            }
        });
        gv_edicion.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                return eliminaConAviso(edicion.get(i));
            }
        });
        gv_consulta.setAdapter(new TarjetaAdapter(this, font, consulta));
        gv_consulta.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ConsultaTarjeta.creada = consulta.get(i);
                abrirTarjeta(false);
            }
        });
    }

    protected void abrirTarjeta(final boolean forResult) {
        makeText(this, "Abriendo tarjeta ...", Toast.LENGTH_LONG).show();
        final Dialog abriendo = avisar("Espera por favor ...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                CreaTarjeta.creada.descargaCompleta();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!forResult) startActivity(new Intent(MisTarjetas.this, ConsultaTarjeta.class));
                        else startActivityForResult(new Intent(MisTarjetas.this, EditaTarjeta.class), EDICION);
                        abriendo.dismiss();
                    }
                });
            }
        }).start();
    }

    protected boolean eliminaConAviso(final Tarjeta t) {
        preguntar(getString(R.string.elimina_tarjeta),
        new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                t.eliminar();
                initDatos();
                filtrar(query);
            }
        }); return true;
    }

    @Override
    protected void initGuia() {
        if (datos.getBoolean("guía tarjetas no leida por " + getNombre(), false)) {
            datos.edit().remove("guía tarjetas no leida por " + getNombre()).commit();
            confirmar(getString(R.string.guia_tarjetas));
        }
    }

    protected void filtrar(String s) {
        if (!(query = s).isEmpty()) {
            query = query.toLowerCase();
            ArrayList<Tarjeta> filtroSuperior = new ArrayList<Tarjeta>();
            for (Tarjeta t: tarjetas) {
                if (t.getNombreLen().toLowerCase().startsWith(query) ||
                        t.getNombreEsp().toLowerCase().startsWith(query))
                    filtroSuperior.add(t);
            } ArrayList<Tarjeta> filtroInferior = new ArrayList<Tarjeta>();
            for (Tarjeta t: otras) {
                if (t.getNombreLen().toLowerCase().startsWith(query) ||
                        t.getNombreEsp().toLowerCase().startsWith(query))
                    filtroInferior.add(t);
            } setListas(filtroSuperior, filtroInferior);
        } else setListas(tarjetas, otras);
        gv_superior.setAdapter(new TarjetaAdapter(MisTarjetas.this, font, edicion));
        gv_inferior.setAdapter(new TarjetaAdapter(MisTarjetas.this, font, consulta));
    }

    protected void setListas(ArrayList<Tarjeta> edicion, ArrayList<Tarjeta> consulta) {
        this.edicion = edicion;
        this.consulta = consulta;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) super.onActivityResult(requestCode, resultCode, data);
        filtrar(query);
    }
}