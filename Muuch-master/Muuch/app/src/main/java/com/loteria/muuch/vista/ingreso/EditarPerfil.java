package com.loteria.muuch.vista.ingreso;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;

import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.terminaSesion;

public class EditarPerfil extends Registrarse {

    protected TextView eliminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getUsuario() != null) {
            registrado = new Usuario(getUsuario());
            super.onCreate(savedInstanceState);
        } else finish();
    }

    @Override
    protected void initDatos() {
        avatar = AvatarAdapter.getAvatar(registrado);
        super.initDatos();
        continuar.setText("actualizar");
        continuar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) { return eliminaConAviso();}
        });
        edit_nombre.setText(registrado.getNombre());
        edit_contraseña.setText(registrado.getContraseña());
        edit_correo.setText(registrado.getCorreo());
        fab.setImageResource(avatar);
        fab.setVisibility(View.VISIBLE);
    }


    protected boolean eliminaConAviso() {
        preguntar(getString(R.string.elimina_cuenta),
        new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrado.eliminar();
                terminaSesion();
                startActivity(new Intent(EditarPerfil.this, Inicio.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        }); return true;
    }

    @Override
    protected boolean commit() {
        return registrado.actualizar();
    }

    @Override
    protected void avanza() { setResult(RESULT_OK); }
}