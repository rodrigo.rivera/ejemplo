package com.loteria.muuch.modelo;

import java.io.Serializable;
import java.util.ArrayList;

import static com.loteria.muuch.modelo.Persistencia.getUsuario;

/**
 * @author Luis Angel Patlani Aguilar
 * Clase que representa a un Usuario en la Base de Datos de la aplicación.
 */
public class Usuario implements Serializable {

    /** Identificador del usuario usado en el nombre de sus recursos. */
    private Long idusuario;
    /** Nombre del usuario. */
    private String nombre;
    /** Correo del usuario. */
    private String correo;
    /** Clave del usuario. */
    private String contraseña;
    /** Datos de la imagen. */
    private Integer avatar;
    /** Lista de tarjetas coleccionadas por el usuario. */
    private ArrayList<Tarjeta> tarjetas = new ArrayList<>();
    /** Lista de amigos registrados por el usuario. */
    private ArrayList<Usuario> amigos = new ArrayList<>();
    /** Lista de tarjetas compartidas al usuario. */
    private ArrayList<Tarjeta> compartidas = new ArrayList<>();
    /** Lista de sugerencias de amistad hechas al usuario. */
    private ArrayList<Usuario> sugerencias = new ArrayList<>();
    /** Propiedad que indica si se ha sincronizado. */
    private boolean inServer = true, old = false, connected;
    /** Variable que asegura la consistencia en la serialización. */
    private static final long serialVersionUID = 1L;

    public Usuario(){}

    /**
     * Constructor de un Usuario sin su identificador.
     * @param nombre Nombre del usuario.
     * @param contraseña Clave del usuario.
     */
    public Usuario(String nombre, String contraseña) {
        this.nombre = nombre;
        this.contraseña = contraseña;
        this.correo = "loteria.soporte.app@gmail.com";
        this.avatar = 1;
    }

    /**
     * Constructor de un Usuario sin su identificador.
     * @param nombre Nombre del usuario.
     * @param correo Correo del usuario.
     * @param contraseña Clave del usuario.
     * @param avatar Nombre de la imagen del usuario.
     */
    public Usuario(String nombre, String correo, String contraseña, Integer avatar) {
        this.nombre = nombre;
        this.correo = correo;
        this.contraseña = contraseña;
        this.avatar = avatar;
    }

    /**
     * Constructor de un Usuario con todos sus datos definidos.
     * @param idusuario Identificador del usuario.
     * @param nombre Nombre del usuario.
     * @param correo Correo del usuario.
     * @param avatar Nombre de la imagen del usuario.
     */
    public Usuario(Long idusuario, String nombre, String correo, Integer avatar) {
        this.idusuario = idusuario;
        this.nombre = nombre;
        this.correo = correo;
        this.avatar = avatar;
    }

    /**
     * Constructor que hace una copia de un usuario en otra instancia.
     * @param original Es el usuario del que se copian los datos.
     */
    public Usuario(Usuario original) {
        this.idusuario = original.getIdusuario();
        this.nombre = original.getNombre();
        this.correo = original.getCorreo();
        this.contraseña = original.getContraseña();
        this.avatar = original.getAvatar();
        this.tarjetas = original.getTarjetas();
        this.compartidas = original.getCompartidas();
        this.amigos = original.getAmigos();
        this.sugerencias = original.getSugerencias();
        this.inServer = original.isInServer();
        this.connected = original.isConnected();
    }

    public Long getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Long idusuario) {
        this.idusuario = idusuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public int getAvatar() { return avatar; }

    public void setAvatar(Integer avatar) { this.avatar = avatar; }

    public boolean isInServer() { return inServer; }

    public void setInServer(boolean inServer) { this.inServer = inServer; }

    public boolean isOld() { return old; }

    public void setOld(boolean old) { this.old = old; }

    public boolean isConnected() { return connected; }

    public void setConnected(boolean connected) { this.connected = connected; }

    public ArrayList<Tarjeta> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(ArrayList<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }

    public ArrayList<Usuario> getAmigos() {
        return amigos;
    }

    public void setAmigos(ArrayList<Usuario> amigos) {
        this.amigos = amigos;
    }

    public ArrayList<Tarjeta> getCompartidas() { return compartidas; }

    public void setCompartidas(ArrayList<Tarjeta> compartidas) { this.compartidas = compartidas; }

    public ArrayList<Usuario> getSugerencias() { return sugerencias; }

    public void setSugerencias(ArrayList<Usuario> sugerencias) { this.sugerencias = sugerencias; }

    public boolean insertar() {
        return Persistencia.insertaUsuario(this);
    }

    public boolean actualizar() {
        return Persistencia.actualizaUsuario(this);
    }

    public boolean eliminar() {
        return Persistencia.eliminaUsuario();
    }

    public boolean insertarAmigo(Usuario amigo) {
        return Persistencia.insertaAmigo(amigo.getIdusuario());
    }

    public boolean eliminaAmigo(Usuario amigo) {
        return Persistencia.eliminaAmigo(amigo.getIdusuario());
    }

    @Override
    public String toString() {
        return "\nId: " + idusuario
                + "\nNombre:\t" + nombre
                + ((contraseña != null)? "\nContraseña:\t" + contraseña: "")
                + "\nCorreo:\t" + correo
                + "\nAvatar:\t" + avatar;
    }

    @Override
    public boolean equals(Object o) {
        Usuario otro = (Usuario) o;
        if (otro == null) return false;
        return nombre.equals(otro.getNombre());
    }
}
