package com.loteria.muuch.modelo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.loteria.muuch.controlador.HttpRequest;
import com.loteria.muuch.vista.ingreso.AvatarAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Random;

import static android.widget.Toast.makeText;
import static com.loteria.muuch.vista.crea_tarjeta.CreaTarjeta.creada;

/**
 * @author Luis Angel Patlani Aguilar
 * Clase que se encarga de la persistencia en la Base de Datos.
 */
public class Persistencia {

    /**
     * Dirección del servidor para realizar consultas a la base de datos.
     * Se debe actualizar según la IP del equipo utilizado como servidor.
     * ESIE: 3513_CC4D3t.
     * */
    public static final String serverURL = "http://esie.ccadet.unam.mx:3000/";
    /** Servidor local usado cuando no hay conexión a internet. */
    public static LocalServer localServer;
    /** Contexto auxiliar para mostrar mensajes. */
    private static Context c;
    /** Variables auxiliares de almacenamiento y consulta. */
    private static String cacheDir, internalDir, response = "", error = "", data = "user.muuch";
    /** Usuario auxiliar para realizar consultas y almacenar datos. */
    private static Usuario u;
    /** Variables auxiliares para consultas y descargas */
    private static boolean special;
    public static boolean actualiza = true;
    public static boolean online = true;
    public static int w, h;

    public static String getNombre() {
        return (u != null)? u.getNombre(): null;
    }
    public static Long getId() {
        return (u != null)? u.getIdusuario(): null;
    }
    public static int getAvatar() {
        return (u != null)? AvatarAdapter.getAvatar(u) : null;
    }
    public static Usuario getUsuario() { return u; }

    /**
     * Método de acceso a la última consulta hecha al servidor.
     * @return Devuelve la cadena de respuesta.
     */
    public static String getResponse() { return response + (response = "");}

    /**
     * Método de acceso a la última consulta hecha al servidor.
     * @return Devuelve la cadena de respuesta.
     */
    public static String getError() { return error + (error = ""); }

    /**
     * Método para obtener un string de response.
     * @param params parámetros de la URL.
     * @return Devuelve el String de respuesta.
     */
    public static String getQuery(Object ... params) {
        try {
            response = localServer.getHTTP(params);
            if (online) response = HttpRequest.get(serverURL + "query?", true, params).connectTimeout(3000).body();
            getError(); // Se limpian errores al momento.
            return response;
        }catch (Exception e) {
            online = false;
            String local = localServer.getResult();
            if (!local.endsWith("bloqueada.]")) local = localServer.getHTTP(params);
            if (!local.contains("Error")) return response = local;
            return error += "HttpResponse: " + localServer.toString(params) + e.toString();
        }
    }

    /**
     * Método para usar parámetros de consultas con llave requerida.
     * @param statement tipo de consulta.
     * @return Devuelve los parámetros a usar.
     */
    private static Object[] userParams(String statement, Object ... params) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("statement"); list.add(statement);
        list.add("iduser"); list.add(getId());
        if (params != null) for (Object p : params) list.add(p);
        if (online) {
            list.add("name"); list.add("'" + getNombre()+ "'");
            list.add("password"); list.add("'" + u.getContraseña() + "'");
        } return list.toArray();
    }

    /**
     * Consulta para iniciar sesión.
     * @param nombre Nombre del usuario que quiere acceder.
     * @param contraseña Clave ingresada por el usuario.
     * @return Devuelve el Usuario encontrado en la BD, o NULL en otro caso.
     */
    public static boolean login(String nombre, String contraseña) {
        u = getUsuarioJSON(getJSON("statement", "u-login",
                "name", "'" + nombre + "'",
                "password", "'" + contraseña + "'"));
        if (u != null) {
            u.setContraseña(contraseña);
            return !online || localServer.syncLogin();
        } return online && localServer.syncLogin();
    }

    /**
     * Actualización de datos de un usuario.
     * @return Devuelve el usuario actualizado.
     */
    public static Usuario getUsuario(Long idusuario) {
        return getUsuarioJSON(getJSON("statement", "u-id", "iduser", idusuario));
    }

    /**
     * Consulta de la colección de tarjetas de un usuario por id.
     * @return Devuelve la lista de tarjetas coleccionadas por el usuario.
     */
    public static ArrayList<Tarjeta> getColeccion() {
        u.setTarjetas(getTarjetasHTTP("statement", "u-collected", "iduser", getId()));
        return u.getTarjetas();
    }

    /**
     * Consulta de las tarjetas de los amigos de un usuario.
     * @return Devuelve la lista de tarjetas no coleccionadas por los amigos del usuario.
     */
    public static ArrayList<Tarjeta> getCompartidas() {
        special = true;
        u.setCompartidas(getTarjetasHTTP("statement", "u-notcollected", "iduser", getId()));
        return u.getCompartidas();
    }

    /**
     * Inserción de un usuario en la Base de Datos.
     * @param n Usuario registrado.
     * @return Devuelve el identificador con el que se ha registrado al usuario.
     */
    public static boolean insertaUsuario(Usuario n) {
        String response = getQuery("statement", "u-add",
                "name", "'" + n.getNombre() + "'",
                "mail", "'" + n.getCorreo() + "'",
                "password", "'" + n.getContraseña() + "'",
                "avatar", n.getAvatar());
        n.setIdusuario(getIdHTTP(response, "insertId"));
        if (n.getIdusuario() > 0) {
            u = n;
            return !online || localServer.syncUserAdded(n);
        } else return false;
    }

    /**
     * Actualización de un usuario en la Base de Datos.
     * @param n Usuario actualizado.
     * @return Devuelve el identificador con el que se ha registrado al usuario.
     */
    public static boolean actualizaUsuario(Usuario n) {
        String response = getQuery(userParams("u-update",
                "newname", "'" + n.getNombre() + "'",
                "mail", "'" + n.getCorreo() + "'",
                "newpassword", "'" + n.getContraseña() + "'",
                "avatar", n.getAvatar()));
        if (error.isEmpty() && !response.contains("Error")){
            n.setIdusuario(u.getIdusuario()); u = n;
            return true;
        } return false;
    }

    /**
     * Eliminación de un usuario por su id.
     * @return Devuelve el mensaje de respuesta.
     */
    public static boolean eliminaUsuario() {
        response = getQuery(userParams("u-remove"));
        if (error.isEmpty() && !response.contains("Error")) return terminaSesion();
        return false;
    }

    /**
     * Consulta de una tarjeta por su id.
     * @param idtarjeta Identificador de la tarjeta consultada.
     * @return Devuelve la tarjeta encontrada con todos sus datos.
     */
    public static Tarjeta getTarjeta(Long idtarjeta) {
        return getTarjetaJSON(getJSON("statement", "c-id", "idcard", idtarjeta));
    }

    /**
     * Inserción de una tarjeta en la Base de Datos.
     * @param t Tarjeta que se inserta.
     * @return Devuelve el identificador con el que se ha registrado la tarjeta.
     */
    public static boolean insertaTarjeta(Tarjeta t) {
        response = getQuery(userParams("c-add",
                "original", "'" + t.getNombreLen() + "'",
                "spanish", "'" + t.getNombreEsp() + "'",
                "category", t.getCategoria()));
        t.setIdtarjeta(getIdHTTP(response, "insertId"));
        if (t.getIdtarjeta() > 0){
            u.getTarjetas().add(t);
            return !online || localServer.syncCardAdded(t);
        } else return false;
    }

    /**
     * Actualización de una tarjeta en la Base de Datos.
     * @param t Tarjeta que se actualiza.
     * @return Devuelve el identificador con el que se ha registrado la tarjeta.
     */
    public static boolean actualizaTarjeta(Tarjeta t) {
        String response = getQuery(userParams("c-update",
                "original", "'" + t.getNombreLen() + "'",
                "spanish", "'" + t.getNombreEsp() + "'",
                "category", t.getCategoria(),
                "idcard", t.getIdtarjeta()));
        return error.isEmpty() && !response.contains("Error");
    }

    /**
     * Eliminación de una tarjeta por su id.
     * @param idtarjeta Identificador de la tarjeta que se elimina.
     * @return Devuelve el mensaje de respuesta.
     */
    public static boolean eliminaTarjeta(Long idtarjeta) {
        String response = getQuery(userParams("c-remove", "idcard", idtarjeta));
        return error.isEmpty() && !response.contains("Error");
    }

    /**
     * Consulta de la lista de amigos de un usuario por id.
     * @return Devuelve la lista de amigos del usuario.
     */
    public static ArrayList<Usuario> getAmigos() {
        u.setAmigos(getUsuariosHTTP("statement", "a-added", "iduser", getId()));
        return u.getAmigos();
    }

    /**
     * Consulta de la lista de no amigos de un usuario por id.
     * @return Devuelve la lista de no amigos del usuario.
     */
    public static ArrayList<Usuario> getSugerencias() {
        ArrayList<Usuario> result = new ArrayList<Usuario>();
        try {
            response = getQuery("statement", "a-notadded", "iduser", getId());
            JSONArray lista = new JSONArray(response);
            int r = (lista.length() > 0)? new Random().nextInt(lista.length()): 0;
            for (int i = 0; i < lista.length() && i < 60; i++) {
                result.add(getUsuarioJSON(lista.getString((i + r) % lista.length())));
            }
        } catch (JSONException e) {
            error += "Getting user list: " + e.toString();
        } u.setSugerencias(result);
        return u.getSugerencias();
    }

    /**
     * Inserción de una relación de amistad en la Base de Datos.
     * @param idamigo Identificador del usuario registrado como amigo.
     * @return Devuelve el mensaje de respuesta.
     */
    public static boolean insertaAmigo(Long idamigo) {
        getQuery(userParams("a-add", "idfriend", idamigo));
        return error.isEmpty() && !response.contains("Error");
    }

    public static boolean insertaAmigo(Usuario u) {
        if (insertaAmigo(u.getIdusuario())) {
            u.getAmigos().add(u);
            return !online || localServer.syncFriendAdded(u);
        } else return false;
    }

    /**
     * Eliminación de una relación de amistad entre 2 usuarios.
     * @param idamigo Identificador del amigo registrado.
     * @return Devuelve el mensaje de respuesta.
     */
    public static boolean eliminaAmigo(Long idamigo) {
        getQuery(userParams("a-remove", "idfriend", idamigo, "iduser", getId()));
        return error.isEmpty() && !response.contains("Error");
    }

    public static boolean eliminaAmigo(Usuario u) {
        return eliminaAmigo(u.getIdusuario());
    }

    /**
     * Método para obtener un string en formato JSON.
     * @param params Parámetros de consulta.
     * @return Devuelve el String de respuesta.
     */
    public static String getJSON(Object ... params) {
        try { return new JSONArray(getQuery(params)).getString(0);
        } catch (JSONException e) {
            return error += "GettingJSONArray: " + e.toString();
        }
    }

    /**
     * Auxiliar decodificador de un objeto JSON a un objeto Usuario.
     * @param stringJSON Texto en formato JSON.
     * @return Devuelve un objeto Usuario.
     */
    public static Usuario getUsuarioJSON(String stringJSON) {
        try {
            JSONObject obj = new JSONObject(stringJSON);
            Long idusuario = new Long(obj.get("idusuario").toString());
            String nombre = obj.get("nombre").toString();
            String correo = obj.get("correo").toString();
            Integer avatar = new Integer(obj.get("avatar").toString());
            return new Usuario(idusuario, nombre, correo, avatar);
        } catch (Exception e) {
            error += "Getting user from JSON: " + e.toString();
            return null;
        }
    }

    /**
     * Auxiliar decodificador de un objeto JSON a un objeto Tarjeta.
     * @param stringJSON Texto en formato JSON.
     * @return Devuelve un objeto Tarjeta.
     */
    public static Tarjeta getTarjetaJSON(String stringJSON) {
        try {
            JSONObject obj = new JSONObject(stringJSON);
            Long idtarjeta = new Long(obj.get("idtarjeta").toString());
            String nombreLen = obj.get("nombreLen").toString();
            String nombreEsp = obj.get("nombreEsp").toString();
            Integer categoria = new Integer(obj.get("categoria").toString());
            Long idusuario = new Long(obj.get("idusuario").toString());
            if (special) {
                String nombre = obj.get("nombre").toString();
                String correo = obj.get("correo").toString();
                Integer avatar = new Integer(obj.get("avatar").toString());
                Usuario o = new Usuario(idusuario, nombre, correo, avatar);
                return new Tarjeta(idtarjeta, nombreLen, nombreEsp, categoria, o);
            } else return new Tarjeta(idtarjeta, nombreLen, nombreEsp, categoria, idusuario);
        } catch (Exception e) {
            error += "Getting card from JSON: " + e.toString();
            return null;
        }
    }

    /**
     * Auxiliar de solicitud HTTP del identificador generado en una URL definida.
     * @param stringJSON Dirección completa de la consulta.
     * @return Devuelve el identificador generado.
     */
    private static Long getIdHTTP(String stringJSON, final String nombreJSON) {
        Long result = new Long(-1);
        try {
            JSONObject obj = new JSONObject(stringJSON);
            result = new Long(obj.get(nombreJSON).toString());
        } catch (JSONException e) {
            error += "GettingID: " + e.toString();
        }
        return result;
    }

    /**
     * Axiliar de solicitud HTTP de una lista de usuarios a una URL definida.
     * @param params Dirección completa de la consulta.
     * @return Devuelve una lista de Usuarios obtenidos.
     */
    private static ArrayList<Usuario> getUsuariosHTTP(Object ... params) {
        return getUsuariosJSON(response = getQuery(params));
    }

    /**
     * Axiliar de solicitud HTTP de una lista de usuarios a una URL definida.
     * @param responseJSON Respuesta del servidor con formato JSON.
     * @return Devuelve una lista de Usuarios obtenidos.
     */
    private static ArrayList<Usuario> getUsuariosJSON(String responseJSON) {
        ArrayList<Usuario> result = new ArrayList<Usuario>();
        try {
            JSONArray lista = new JSONArray(responseJSON);
            for (int i = 0; i < lista.length(); i++) {
                result.add(getUsuarioJSON(lista.getString(i)));
            }
        } catch (JSONException e) {
            error += "Getting user list: " + e.toString();
        }
        return result;
    }

    /**
     * Auxiliar de solicitud HTTP de una lista de tarjetas a una URL definida.
     * @param params Dirección completa de la consulta.
     * @return Devuelve una lista de Tarjetas obtenidas.
     */
    private static ArrayList<Tarjeta> getTarjetasHTTP(Object ... params) {
        return getTarjetasJSON(getQuery(params));
    }

    /**
     * Auxiliar de solicitud HTTP de una lista de tarjetas a una URL definida.
     * @param responseJSON Respuesta del servidor en formato JSON.
     * @return Devuelve una lista de Tarjetas obtenidas.
     */
    private static ArrayList<Tarjeta> getTarjetasJSON(String responseJSON) {
        ArrayList<Tarjeta> result = new ArrayList<Tarjeta>();
        try {
            JSONArray lista = new JSONArray(responseJSON);
            for (int i = 0; i < lista.length(); i++) {
                result.add(getTarjetaJSON(lista.getString(i)));
            }
        } catch (Exception e) {
            error += "Getting cards list: " + e.toString();
        } special = false;
        return result;
    }

    /**
     * Método que sube archivos
     * @param url
     * @param param
     * @param archivo
     * @return
     */
    public static boolean subirArchivos(String url, String param, File ... archivo) {
        try {
            if (!online) return true;
            HttpRequest request = HttpRequest.post(serverURL + url);
            for (int i = 0; i < archivo.length; i++) {
                response += "\nfrom " + archivo[i].getAbsolutePath() + " to " + url;
                request.part(param, archivo[i].getName(), archivo[i]);
            } return request.ok();
        } catch (Exception e) {
            error += "Uploading files: " + response + "\n" + e.toString();
            return false;
        }
    }

    /**
     * Método que renombra un archivo y devuelve el archivo renombrado.
     * @param nuevos El nuevo directorio y nombre del archivo.
     * @return Devuelve el nombre del archivo renombrado.
     */
    public static void renombrar(File[] archivo, String ... nuevos) {
        response += "Renaming files:";
        for (int i = 0; i < archivo.length; i++) {
            File renombrado = new File(archivo[i].getParent(), nuevos[i]);
            response += "\nfrom " + archivo[i].getAbsolutePath() + " to " + renombrado.getAbsolutePath();
            archivo[i].renameTo(renombrado);
            archivo[i] = renombrado;
        }
    }

    /**
     * Método que copia archivos con nuevo nombre y devuelve los archivos copiados.
     * @param origen Directorios y nombres originales de los archivos.
     * @param destino Directorios y nombres nuevos de los archivos.
     * @return Devuelve los archivos copiados.
     */
    public static boolean copiar(File[] origen, File[] destino) {
        try{
            response += "Copying files:";
            for (int i = 0; i < origen.length; i++) {
                if (origen[i].exists()) {
                    response += "\nfrom " + origen[i].getAbsolutePath() + " to " + destino[i].getAbsolutePath();
                    FileInputStream lector = new FileInputStream(origen[i]);
                    FileOutputStream escritor = new FileOutputStream(destino[i]);
                    FileChannel entrada = lector.getChannel(), salida = escritor.getChannel();
                    entrada.transferTo(0, entrada.size(), salida);
                    entrada.close(); lector.close();
                    salida.close(); escritor.close();
                } else response += "\n" + origen[i].getName() + " not exists.";
            } return true;
        } catch (Exception e) { error = "Error on copy files: " + e.toString();}
        return false;
    }

    /**
     * Método que asigna los directorios para escribir y leer, a partir de un contexto.
     * @param c Contexto en el que se asignan los directorios.
     */
    public static void init(Context c) {
        Persistencia.c = c;
        cacheDir = c.getCacheDir() + "/muuch/";
        internalDir = c.getFilesDir() + "/muuch/";
        localServer = new LocalServer();
    }

    /**
     * Método que guarda un archivo dentro de la carpeta de la app.
     * @param subdir Subdirectorio donde se guarda el archivo.
     * @param nombre Nombre con el que se guarda el archivo.
     * @return Devuelve el archivo que se ha guardado.
     */
    public static File cacheFile(String subdir, String nombre) {
        String path = cacheDir + subdir;
        new File(path).mkdirs();
        return new File(path + nombre);
    }

    /**
     * Método que guarda un archivo dentro de la carpeta de la app.
     * @param subdir Subdirectorio donde se guarda el archivo.
     * @param nombre Nombre con el que se guarda el archivo.
     * @return Devuelve el archivo que se ha guardado.
     */
    public static File internalFile(String subdir, String nombre) {
        String path = internalDir + subdir;
        new File(path).mkdirs();
        return new File(path + nombre);
    }

    /**
     * Consulta de un archivo del servidor.
     * @param nombre Es el nombre del archivo.
     * @param origen Es la ruta del archivo a descargar.
     * @param destino Es la ruta donde se guarda el archivo.
     * @return Devuelve los datos del archivo descargado.
     */
    public static File descargarArchivo(String origen, String destino, String nombre) {
        try {
            File archivo = cacheFile(destino, nombre);
            response += "\nfrom " + origen + nombre + " to " + destino + nombre;
            if (!online || !actualiza) return archivo;
            String fileURL = serverURL + origen + nombre;
            HttpRequest.get(fileURL).receive(archivo);
            return archivo;
        } catch (Exception e) {
            error += "Downloading file: " + response + "\n" + e.toString();
            return null;
        }
    }

    /**
     * Método que convierte un archivo a imagen.
     * @param archivo Archivo que contiene los datos de la imagen.
     * @return Devuelve el BitMap que se genera del archivo.
     */
    public static Bitmap getBitMap(File archivo) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inDither = true;
        Bitmap bm = BitmapFactory.decodeFile(archivo.getPath(), options);
        w = options.outWidth; h = options.outHeight;
        response = options.outWidth + "," + options.outHeight;
        return bm;
    }

    /**
     * Consulta para recuperar cuenta por nombre.
     * @param nombre Nombre del usuario que quiere acceder.
     * @return Devuelve el Usuario encontrado en la BD, o NULL en otro caso.
     */
    public static boolean recovery(String nombre) {
        try {
            response = localServer.recovery(nombre);
            if (online) response = HttpRequest.get(serverURL + "recovery?", true,
                        "name", "'" + nombre + "'", "origen", new Integer(1))
                        .connectTimeout(3000).body();
            getError(); // Se limpian errores al momento.
        } catch (Exception e) {
            error = "Http Response:" + e.toString();
            if (!online) return false;
            online = false; response = localServer.getResult();
        } try {
            if (response.equals("[]")) return false;
            if (response.contains("Correo enviado")) return true;
            JSONArray lista = new JSONArray(response);
            u = getUsuarioJSON(lista.getString(0));
            u.setContraseña(localServer.user.getContraseña());
            u.setAmigos(getUsuariosJSON(lista.getString(1)));
            u.setTarjetas(getTarjetasJSON(lista.getString(2)));
            return true;
        } catch (Exception e) { error = "Recovering user: " + e.toString(); return false; }
    }

    public static boolean cargaSesion() {
        File session = internalFile("data/", data);
        try{
            if (!session.exists()) return false;
            FileInputStream entrada = new FileInputStream(session);
            ObjectInputStream lector = new ObjectInputStream(entrada);
            u = (Usuario)lector.readObject();
            lector.close();
            entrada.close();
            response = "Cargando sesión ...";
            return true;
        } catch (Exception ex) { error += ex.toString(); }
        response = "No hay sesión cargada. " + getError();
        return false;
    }

    public static boolean guardaSesion() {
        File session = internalFile("data/", data);
        try{
            if (session.exists()) session.delete();
            FileOutputStream salida = new FileOutputStream(session);
            ObjectOutputStream escritor = new ObjectOutputStream(salida);
            escritor.writeObject(u);
            escritor.close();
            salida.close();
            response = "Guardando sesión...";
            return session.exists();
        } catch (Exception ex) { error += ex.toString(); }
        response = "No hay sesión guardada. " + getError();
        return false;
    }

    public static boolean terminaSesion() {
        u = null;
        File session = internalFile("data/", data);
        response = "Cerrando sesión...";
        return session.delete();
    }

    public static void dataState(Context c) {
        try{
            String server = (error.isEmpty()? "": "Error:\n" + getError())
                    + (response.isEmpty()? "": "\nNode Server:\n" + getResponse())
                    + (localServer.getResult().isEmpty()? "": "\nLocal Server:\n" + localServer.getResult())
                    + ((u == null)? "": "\n\nUsuario:\n" + u.toString()
                    + "\nAmigos:\n" + getAmigos().toString()
                    + "\nTarjetas:\n" + getColeccion().toString())
                    + ((creada == null)? "": "\n\nTarjeta:\n" + creada.toString());
            makeText(c, server, Toast.LENGTH_LONG).show();
            if (localServer.getResult().contains("Error")) {
                makeText(c, localServer.getResult(), Toast.LENGTH_LONG).show();
                makeText(c, getQuery("statement", "c-all"), Toast.LENGTH_LONG).show();
                makeText(c, getQuery("statement", "u-all"), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e){
            makeText(c, "Error: " + getError(), Toast.LENGTH_LONG).show();
        }
    }
}