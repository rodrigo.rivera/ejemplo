package com.loteria.muuch.vista.arma_loteria_user;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.loteria.muuch.R;
import com.loteria.muuch.controlador.SocketIO;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.arma_loteria_user.jugando_loteria.JugandoLoteria;
import java.util.ArrayList;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import static com.loteria.muuch.modelo.Persistencia.getColeccion;
import static com.loteria.muuch.modelo.Persistencia.getId;


public  class ArmaLoteriaUser extends MenuActivity {
    private GridView gridView;
    private ImageButton continuar;
    public static String envio;
    private TextView numTarjetas;
    private int numeroTarjetas;
    private AdapterSeleccionTarjeta ast;
    private ArrayList<String> listaTarjetasSeleccionada;
    String[] sep;
    private TextView nombreUsuario;
    SocketIO app;
    Socket mSocket ;
    private ArrayList<String> lisCarta;
    private String idAdmin1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_armaloteria_user);

        this.idAdmin1 = "";
        lisCarta = new ArrayList<>();
        app = (SocketIO) getApplication();
        mSocket = app.getSocket();

        this.nombreUsuario= (TextView) findViewById(R.id.textView7);
        nombreUsuario.setText(Persistencia.getNombre());
        listaTarjetasSeleccionada = new ArrayList<>();
        ast = new AdapterSeleccionTarjeta(this, font, getColeccion());
        gridView = (GridView) findViewById(R.id.gridViewMisTarjetas_user);
        gridView.setAdapter(ast);
        numTarjetas = (TextView) findViewById(R.id.textView6);

        /*
        *   Boton que si se preciona y se cumple las condiciones se manda al administrador de la partida
         *   que se acepto la invitacion para jugar.
         */
        continuar = (ImageButton)findViewById(R.id.imageButton21);
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Tarjeta> tarjetasSeleccionadas = ast.getSeleccion();
                if(numeroTarjetas == tarjetasSeleccionadas.size()){
                    for(int i = 0; i<tarjetasSeleccionadas.size(); i++){
                        listaTarjetasSeleccionada.add(String.valueOf(tarjetasSeleccionadas.get(i).getIdtarjeta()));
                    }
                    try{
                        idAdmin1 =  sep[0];
                        //idAdmin-idUser-tarjetasSeleccionadasParaJugar
                        mSocket.emit("aceptoInvitacion", sep[0]+"-"+String.valueOf(getId())+"-"+listaTarjetasSeleccionada.toString());
                    } catch (Exception e) {}
                    continuar.setVisibility(View.INVISIBLE);
                }else if(numeroTarjetas<tarjetasSeleccionadas.size()){
                    Toast.makeText(ArmaLoteriaUser.this, "El número de tarjetas seleccionada es mayor al " +
                            "permitido. ", Toast.LENGTH_LONG).show();
                }else if(numeroTarjetas>tarjetasSeleccionadas.size()){
                    Toast.makeText(ArmaLoteriaUser.this, "No has seleccionado suficientes" +
                            " tarjetas. ", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Revisar comportamiento cuando no hay conexión al servidor.
        if (envio != null) {
            sep = envio.split(",");
            //idAdmin,
            String parte2 = sep[2];
            numeroTarjetas = Integer.parseInt(sep[2]);
            numTarjetas.setText("Selecciona tus "+parte2+" tarjetas");
        }

        //Se ejecuta cuando llega la carta del usuario
        mSocket.on("envioCartaUser", llegadaCarta);
        mSocket.on("iniciarPartida", llegadaListoParaJugar);
    }

    private Emitter.Listener llegadaCarta = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //idAdmin-[carta]
                    String carta = (String)args[0];
                    String[] separarAtributos = carta.split("-");

                    String soloCarta = separarAtributos[1];
                    soloCarta = soloCarta.replaceAll("\\[" , "");
                    soloCarta = soloCarta.replaceAll("]", "").trim();

                    String[] volSep = soloCarta.split(",");


                    for(int i = 0; i<volSep.length; i++){
                        lisCarta.add(volSep[i].trim());
                    }
                    //idadmin-miIdUser
                    mSocket.emit("cartaRecibida",separarAtributos[0]+"-"+String.valueOf(Persistencia.getId()));
                }
            });
        }
    };

    private Emitter.Listener llegadaListoParaJugar = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String listo = (String)args[0];
                    if(listo.equalsIgnoreCase("listo")){
                        Intent i = new Intent(ArmaLoteriaUser.this, JugandoLoteria.class);
                        i.putExtra("listaCarta", lisCarta);
                        i.putExtra("idAdmin", idAdmin1);
                        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    }
                }
            });
        }
    };
}