package com.loteria.muuch.vista.mis_amigos;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.mis_tarjetas.OtrasCategorias;

import java.util.ArrayList;

import static com.loteria.muuch.modelo.Persistencia.eliminaAmigo;
import static com.loteria.muuch.modelo.Persistencia.getAmigos;
import static com.loteria.muuch.modelo.Persistencia.getNombre;
import static com.loteria.muuch.modelo.Persistencia.getSugerencias;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.insertaAmigo;

public class MisAmigos extends MenuActivity {

    protected GridView gv_superior, gv_inferior;
    public static ArrayList<Usuario> amigos, sugerencias;
    private String query = "";
    private ArrayList<Usuario> superior, inferior;
    protected TextView tv_amigos, tv_otros;
    protected SearchView buscador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getUsuario() == null) { finish(); return; }
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_mis_amigos);
        gv_superior = (GridView) findViewById(R.id.gridViewMisAmigos);
        gv_inferior = (GridView) findViewById(R.id.gridViewOtrosAmigos);
        tv_amigos = (TextView)findViewById(R.id.tv_mis_amigos);
        tv_otros = (TextView)findViewById(R.id.tv_otros_amigos);
        setFont(tv_amigos, tv_otros);
        initDatos();
        buscador = (SearchView)findViewById(R.id.searchView);
        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {return false;}

            @Override
            public boolean onQueryTextChange(String s) {
                filtrar(s);
                return false;
            }
        });
    }

    /**
     * Método que incializa las listas de amigos y sugerencias.
     */
    @Override
    public void initDatos() {
        setListas(amigos = getAmigos(), sugerencias = getSugerencias());
        gv_superior.setAdapter(new AmigoAdapter(this, font, superior));
        gv_superior.setClickable(true);
        gv_superior.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                OtrasCategorias.amigo = superior.get(i);
                startActivity(new Intent(MisAmigos.this, OtrasCategorias.class));
            }
        });
        gv_superior.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                return eliminaConAviso(superior.get(i));
            }
        });
        gv_inferior.setAdapter(new AmigoAdapter(this, font, inferior));
        gv_inferior.setClickable(true);
        gv_inferior.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                agregarAmigo(i);
            }
        });
    }

    protected void agregarAmigo(final int i) {
        final Dialog agregando = avisar("Agregando amigo...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                final boolean agregado = insertaAmigo(inferior.get(i));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!agregado) confirmar(getString(R.string.error_reinicio));
                        initDatos();
                        filtrar(query);
                        agregando.dismiss();
                    }
                });
            }
        }).start();
    }

    protected boolean eliminaConAviso(final Usuario amigo) {
        preguntar(getString(R.string.elimina_amigo).replace("?", amigo.getNombre()),
        new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!eliminaAmigo(amigo)) confirmar(getString(R.string.error_reinicio));
                initDatos();
                filtrar(query);
            }
        }); return true;
    }

    @Override
    protected void initGuia() {
        if (datos.getBoolean("guía amigos no leida por " + getNombre(), false)) {
            datos.edit().remove("guía amigos no leida por " + getNombre()).commit();
            confirmar(getString(R.string.guia_amigos));
        }
    }

    protected void filtrar(String s) {
        if (!(query = s).isEmpty()) {
            query = query.toLowerCase();
            ArrayList<Usuario> filtroSuperior = new ArrayList<Usuario>();
            for (Usuario u: amigos) {
                if (u.getNombre().toLowerCase().startsWith(query)) filtroSuperior.add(u);
            }
            ArrayList<Usuario> filtroInferior = new ArrayList<Usuario>();
            for (Usuario u: sugerencias) {
                if (u.getNombre().toLowerCase().startsWith(query)) filtroInferior.add(u);
            } setListas(filtroSuperior, filtroInferior);
        } else setListas(amigos, sugerencias);
        gv_superior.setAdapter(new AmigoAdapter(MisAmigos.this, font, superior));
        gv_inferior.setAdapter(new AmigoAdapter(MisAmigos.this, font, inferior));
    }

    protected void setListas(ArrayList<Usuario> superior, ArrayList<Usuario> inferior) {
        this.superior = superior;
        this.inferior = inferior;
    }
}
