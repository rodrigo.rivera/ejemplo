package com.loteria.muuch.vista.ingreso;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.controlador.SocketIO;
import com.loteria.muuch.modelo.LocalServer;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.arma_loteria_admin.ListaOpciones;
import com.loteria.muuch.vista.arma_loteria_user.ArmaLoteriaUser;
import com.loteria.muuch.vista.crea_tarjeta.CreaTarjeta;
import com.loteria.muuch.vista.mis_amigos.MisAmigos;
import com.loteria.muuch.vista.mis_tarjetas.MisCategorias;

import static com.loteria.muuch.modelo.LocalServer.cardsOk;
import static com.loteria.muuch.modelo.LocalServer.friendsOk;
import static com.loteria.muuch.modelo.Persistencia.cargaSesion;
import static com.loteria.muuch.modelo.Persistencia.dataState;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.localServer;
import static com.loteria.muuch.modelo.Persistencia.online;
import static com.loteria.muuch.vista.arma_loteria_user.ArmaLoteriaUser.envio;
import com.loteria.muuch.R;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.loteria.muuch.modelo.Persistencia.getId;

public class Inicio extends MenuActivity {
    ImageButton creaTarjetas;
    ImageButton misTarjetas;
    ImageButton armaLoteria;
    ImageButton misAmigos;
    ImageButton invitacion;
    TextView txtCreaTarjetas;
    TextView txtMisTarjetas;
    TextView txtArmaLoteria;
    TextView txtMisAmigos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!cargaSesion()) {
            startActivity(new Intent(Inicio.this, IniciarSesion.class));
            finish(); return;
        } startSync();
        super.init(R.layout.activity_inicio); esLaPrincipal = true;
        creaTarjetas =(ImageButton) findViewById(R.id.Btn_crea_tarjetas);
        misTarjetas = (ImageButton) findViewById(R.id.Btn_mis_tarjetas);
        armaLoteria = (ImageButton) findViewById(R.id.Btn_arma_loteria);
        misAmigos = (ImageButton) findViewById(R.id.Btn_mis_Amigos);
        invitacion = (ImageButton) findViewById(R.id.imageButton5);

        txtCreaTarjetas = (TextView) findViewById(R.id.txt_crea_tarjeta);
        txtMisTarjetas = (TextView) findViewById(R.id.txt_mis_tarjetas);
        txtArmaLoteria = (TextView) findViewById(R.id.txt_arma_lotertia);
        txtMisAmigos = (TextView) findViewById(R.id.txt_mis_amigos);

        setFont(txtArmaLoteria, txtMisAmigos, txtMisTarjetas, txtCreaTarjetas);

        creaTarjetas.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), CreaTarjeta.class));
            }
        });
        misTarjetas.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), MisCategorias.class));
            }
        });
        misAmigos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), MisAmigos.class));
            }
        });
        armaLoteria.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), ListaOpciones.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });
        //invitacion.setVisibility(View.INVISIBLE);
        invitacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                invitacion.setVisibility(View.INVISIBLE);
                startActivity(new Intent(Inicio.this, ArmaLoteriaUser.class));
            }
        });
        String nombre = Persistencia.getNombre();

        SocketIO app =  (SocketIO) getApplication();
        Socket mSocket = app.getSocket();
        //Manda mensaje al servidor para conectarse con el id del usuario
        mSocket.emit("Conectado", getId());
        //mensaje de entrada por si llega una invitacion
        mSocket.on("LosJugadoresInvitados", llegadaInv);
        mSocket.connect();
    }

    /**
     * Se ejecuta este listener por si llega la invitacion de juego
     */
    private Emitter.Listener llegadaInv = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Se hace visible el boton de invitacion
                    invitacion.setVisibility(View.VISIBLE);
                    //se guarda los datos de la partida en la variable envio. envio es una variable de otra de la clase ArmaLoteriaUser
                    //args[0] = idAdmin,idioma,nivel,jugadores
                    //ejemplo = 2,español,facil,2
                    envio = args[0].toString();
                    Toast.makeText(Inicio.this, "Hola te invitaron a jugar" , Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != EDITAR_PERFIL) super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Método que sincroniza los datos de los amigos y tarjetas del usuario.
     */
    private void startSync() {
        try{
            if (online && (!cardsOk || !friendsOk)) {
                final Dialog progreso = avisar("Cargando tus datos...");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final boolean error = !localServer.syncFriends() || !localServer.syncCards();
                        progreso.dismiss();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() { if (error) confirmar(getString(R.string.error_reinicio));}
                        });
                    }
                }).start();
            }
        } catch (Exception e) {
            dataState(this);
            confirmar(getString(R.string.error_reinicio));
        }
    }
}