package com.loteria.muuch.vista.arma_loteria_user.jugando_loteria;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.vista.arma_loteria_user.AdapterSeleccionTarjeta;
import com.loteria.muuch.vista.mis_tarjetas.TarjetaAdapter;

import java.util.ArrayList;

/**
 * Created by Hugo on 09/01/2018.
 */

public class AdapterCarta extends AdapterSeleccionTarjeta {

    public AdapterCarta(Context contexto, Typeface font, ArrayList<Tarjeta> lista) {
        super(contexto, font, lista);
        // Este adapter inicia con todos los elementos seleccionados.
        for (int i = 0; i < lista.size(); i++) seleccion.add(new Integer(i));
    }
}
