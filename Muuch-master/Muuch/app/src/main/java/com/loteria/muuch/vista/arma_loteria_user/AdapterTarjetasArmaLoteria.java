package com.loteria.muuch.vista.arma_loteria_user;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;

import java.util.ArrayList;

/**
 * Created by Usuario on 16/06/2017.
 */

public class AdapterTarjetasArmaLoteria extends BaseAdapter {
    private Context contexto;
    private TextView idioma1;
    private TextView idioma2;
    private ImageButton btn_mas;
    private ImageView imageView;
    private ArrayList<Tarjeta> listaCarta;
    boolean checkAll_flag = false;
    boolean checkItem_flag = false;

    public  AdapterTarjetasArmaLoteria(Context c){
        contexto = c;
    }

    @Override
    public int getCount() {
        return listaCarta.size();
    }

    @Override
    public Object getItem(int i) {
        return listaCarta.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v;
        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(contexto);
            v  =inflater.inflate(R.layout.adapter_tarjetas_armaloteria, null);

        }else{
            v = convertView;
        }

        idioma1 = (TextView) v.findViewById(R.id.idioma1);
        idioma2 = (TextView) v.findViewById(R.id.idioma2);
        btn_mas = (ImageButton)v.findViewById(R.id.btn_mas);
        imageView = (ImageView) v.findViewById(R.id.imagenTarjeta);

        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(1, 1, 1, 1);
        imageView.setImageResource(mThumbIds[position]);

       // idioma1.setText(nombres1[position]);
        idioma2.setText(nombres2[position]);
        return v;

    }    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.camara, R.drawable.camara,
            R.drawable.camara, R.drawable.camara,
            R.drawable.camara, R.drawable.camara

    };

    private String[] nombres1 = {
            "Img 1",
            "Img 2",
            "Img 3",
            "Img 4",
            "Img 5",
            "Img 6"
    };

    private String[] nombres2 = {
            "Img 1",
            "Img 2",
            "Img 3",
            "Img 4",
            "Img 5",
            "Img 6"
    };
}