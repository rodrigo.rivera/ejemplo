package com.loteria.muuch.vista.arma_loteria_admin.jugando_admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.vista.ingreso.AvatarAdapter;

/**
 * Created by Usuario on 13/10/2017.
 */

public class AdapterAmigosJugandoAdmin extends BaseAdapter{
    private Context contexto;
    private TextView nombreAmigo;
    private ImageView imageView;

    boolean checkAll_flag = false;
    boolean checkItem_flag = false;

    AdapterAmigosJugandoAdmin(Context c){
        contexto = c;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(contexto);
            v  =inflater.inflate(R.layout.adapter_amigos_armaloteria, null);

        }else{
            v = convertView;
        }

        nombreAmigo = (TextView) v.findViewById(R.id.nombre_amigo);
        imageView = (ImageView) v.findViewById(R.id.avatar_amigo);


        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(1, 1, 1, 1);
        //imageView.setImageResource(mThumbIds[position]);

        nombreAmigo.setText(nombres1[position]);
        return v;

    }    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.camara, R.drawable.camara,
            R.drawable.camara
    };

    private String[] nombres1 = {
            "Img 1",
            "Img 2",
            "Img 3"
    };


}

