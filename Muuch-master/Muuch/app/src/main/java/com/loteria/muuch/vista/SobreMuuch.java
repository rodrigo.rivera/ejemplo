package com.loteria.muuch.vista;

import android.os.Bundle;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.vista.SimpleActivity;

public class SobreMuuch extends SimpleActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_sobre_muuch);
        setFont((TextView)findViewById(R.id.tv_sobre_muuch));
    }
}
