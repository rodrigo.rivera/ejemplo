package com.loteria.muuch.vista.crea_tarjeta;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.loteria.muuch.R;

import static com.loteria.muuch.modelo.Contenido.setBitmap;

public class EditaTarjeta extends CreaTarjeta {

    protected TextView actualizar;

    @Override
    protected void initDatos() {
        super.initDatos();
        proceso = "edición";
        idtarjeta = creada.getIdtarjeta();
        editText_id.setText(creada.getNombreLen());
        editText_esp.setText(creada.getNombreEsp());
        categoria = creada.getCategoria();
        listaCategorias.setSelection(categoria + 1);
        sv_imagen.post(new Runnable() {
            @Override
            public void run() {
                archivo = creada.descargaCompleta().getFiles();
                bm_imagen = setBitmap(true, archivo[5], btn_imagen)[0];
                if (bm_imagen != null) {
                    setViews(false, false, (TextView)findViewById(R.id.txt_captura));
                } else setViews(true, true, (TextView)findViewById(R.id.txt_captura));
            }
        });
        sv_imagen.postDelayed(new Runnable() {
            @Override
            public void run() { sv_imagen.scrollTo(0, (btn_imagen.getHeight() - sv_imagen.getHeight()) / 2);}
        }, 200);
        setViews(true, true, btn_reproducir);
        setViews(true, true, barra_audio);
        setViews(false, false, btnguardar, btnseguir);
        setViews(false, false, btn_grabar[2]);
        setViews(false, false, btn_grabar[3]);
        setViews(true, true, btn_grabar[4]);
        setViews(true, true, btn_grabar[5]);
        reproducir3 = true; reproducir4 = true;
        actualizar = (TextView)findViewById(R.id.tv_actualizar);
        setViews(true, true, actualizar);
        setFont(actualizar);
        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardar(true);
            }
        });
    }

    @Override
    protected boolean commit() {
        creada.setIdtarjeta(idtarjeta);
        return creada.actualizar();
    }

    @Override
    protected void initGuia() {
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (creada == null) finish();
        else super.onRestoreInstanceState(savedInstanceState);
    }
}