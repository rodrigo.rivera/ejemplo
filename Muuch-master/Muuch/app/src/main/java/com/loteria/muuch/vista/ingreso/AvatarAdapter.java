package com.loteria.muuch.vista.ingreso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;

/**
 * Created by PATLANIUNAM on 30/09/2017.
 */

public class AvatarAdapter extends BaseAdapter{

    Context contexto;
    View seleccionado;
    int previo;
    public static final Integer lista[] =
            {R.drawable.ic_a_1, R.drawable.ic_a_2, R.drawable.ic_a_3, R.drawable.ic_a_4,
            R.drawable.ic_a_5, R.drawable.ic_a_6, R.drawable.ic_a_7, R.drawable.ic_a_8,
            R.drawable.ic_a_9, R.drawable.ic_a_10, R.drawable.ic_a_11, R.drawable.ic_a_12,
            R.drawable.ic_a_13, R.drawable.ic_a_14, R.drawable.ic_a_15, R.drawable.ic_a_16};

    /**
     * Método para obtener el ID recurso en android del avatar del usuario.
     * @param u Usuario del que se obtiene el avatar.
     * @return Devuelve el ID del recurso del avatar.
     */
    public static int getAvatar(Usuario u) {
        return lista[u.getAvatar() % lista.length];
    }

    public AvatarAdapter(Context contexto, int previo) {
        this.contexto = contexto;
        this.previo = previo;
    }

    @Override
    public int getCount() {
        return lista.length;
    }

    @Override
    public Object getItem(int i) { return lista[i]; }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(contexto);
            view = inflater.inflate(R.layout.adapter_avatar, null);
        }
        ImageView avatar = (ImageView) view.findViewById(R.id.item_avatar);
        int id = lista[i];
        avatar.setImageResource(id);
        if (id == previo) {
            view.setBackgroundColor(view.getResources().getColor(R.color.amarillo_629));
            seleccionado = view;
            previo = -1;
        } return view;
    }

    public View getSeleccionado() { return seleccionado; }
    public View setSeleccionado(View v) { return seleccionado = v; }
}
