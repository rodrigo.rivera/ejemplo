package com.loteria.muuch.vista.crea_tarjeta;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.ingreso.AvatarAdapter;
import com.loteria.muuch.vista.mis_tarjetas.OtrasCategorias;

public class ConsultaTarjeta extends EditaTarjeta {

    @Override
    protected void initDatos() {
        super.initDatos();
        proceso = "";
        final Usuario u = creada.getUsuario();
        RelativeLayout detalles = (RelativeLayout) findViewById(R.id.rl_detalles);
        detalles.getLayoutParams().height = RelativeLayout.LayoutParams.MATCH_PARENT;
        detalles.requestLayout();
        TextView describe = (TextView)findViewById(R.id.descripcion_imagen);
        describe.setText(R.string.consulta_descripcion);
        fab.setImageResource(AvatarAdapter.getAvatar(u));
        nombre_usuario.setText(u.getNombre());
        setViews(true, false, editText_id, editText_esp, listaCategorias, btn_imagen);
        setViews(false, false, btnguardar);
        setViews(false, false, btn_grabar);
        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                OtrasCategorias.amigo = u;
                startActivity(new Intent(ConsultaTarjeta.this, OtrasCategorias.class));
                return false;
            }
        });
    }
}