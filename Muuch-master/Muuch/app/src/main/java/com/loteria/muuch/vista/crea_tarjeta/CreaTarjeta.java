package com.loteria.muuch.vista.crea_tarjeta;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.R;
import com.loteria.muuch.vista.MenuActivity;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static com.loteria.muuch.modelo.Contenido.captura;
import static com.loteria.muuch.modelo.Contenido.grabacion;
import static com.loteria.muuch.modelo.Contenido.setBitmap;
import static com.loteria.muuch.modelo.Contenido.subirDatos;
import static com.loteria.muuch.modelo.Persistencia.getAvatar;
import static com.loteria.muuch.modelo.Persistencia.getNombre;
import static com.loteria.muuch.modelo.Persistencia.getResponse;
import static com.loteria.muuch.modelo.Persistencia.h;
import static com.loteria.muuch.modelo.Persistencia.w;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.categorias;

public class CreaTarjeta extends MenuActivity {

    public static Tarjeta creada;
    // Recursos para de los 4 audios y las 2 barras de progreso.
    protected static final int audios = 4, barras = 2;
    // Archivos necesarios de audio e imagen.
    protected File archivo[];
    protected MediaRecorder recorder[] = new MediaRecorder[audios];
    protected MediaPlayer player[] = new MediaPlayer[audios];
    protected ImageButton btn_grabar[] = new ImageButton[audios + 2];
    protected ImageButton btn_reproducir[] = new ImageButton[audios];
    protected ImageButton btn_detener[] = new ImageButton[audios];
    protected ImageButton btn_imagen;
    protected ScrollView sv_imagen;
    protected Bitmap bm_imagen;
    // Vsriables del progreso del audio.
    protected SeekBar barra_audio[] = new SeekBar[barras];
    protected Thread hilo_barra[] = new Thread[barras];
    // Variables de los datos ingresados.
    protected ImageButton btnguardar, btnseguir;
    protected EditText editText_id, editText_esp;
    protected String idioma, español, proceso;
    protected int categoria, reproduciendo = -1;
    protected Long idtarjeta;
    protected Spinner listaCategorias;
    protected boolean reproducir3, reproducir4;
    protected boolean hayGrabacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_crea_tarjeta);
        initDatos();
        setFont(editText_esp, editText_id,
                (TextView)findViewById(R.id.idioma),
                (TextView)findViewById(R.id.español),
                (TextView)findViewById(R.id.txt_captura),
                (TextView)findViewById(R.id.descripcion_imagen));
        permisoDeAudio();
    }

    /**
     * Método auxiliar que solicita permisos para grabar audio.
     * @return Devuelve TRUE si los permisos no fueron negados.
     */
    protected boolean permisoDeAudio(){
        int record = ContextCompat.checkSelfPermission(
        this, Manifest.permission.RECORD_AUDIO);
        boolean negado = record != PackageManager.PERMISSION_GRANTED;
        if (negado) {
            String[] permisos = {android.Manifest.permission.RECORD_AUDIO};
            ActivityCompat.requestPermissions(this, permisos, 1);
        } return !negado;
    }

    /**
     * Método que carga los datos del usuario e inicializa los componentes
     * para ingresar los datos de la tarjeta.
     */
    @Override
    protected void initDatos() {
        archivo = new File[audios + 2]; // 2 archivos para imagen.
        editText_id = (EditText) findViewById(R.id.txt_idioma);
        editText_esp = (EditText) findViewById(R.id.txt_esp);
        String hint = editText_id.getHint() + " ";
        editText_id.setHint(hint + getString(R.string.longitud));
        hint = editText_esp.getHint() + " ";
        editText_esp.setHint(hint + getString(R.string.longitud));

        btnguardar = (ImageButton) findViewById(R.id.guardar);
        btnseguir = (ImageButton) findViewById(R.id.seguir);
        sv_imagen = (ScrollView)findViewById(R.id.sv_captura);
        btn_imagen = (ImageButton) findViewById(R.id.capturar);
        btn_imagen.setImageResource(R.drawable.camara);
        btn_imagen.setScaleType(ImageView.ScaleType.CENTER);
        bm_imagen = null;
        initBarra(barra_audio, false, R.id.barraDE, R.id.barraDL);
        initBotones(btn_grabar, true, R.id.recNE, R.id.recNL, R.id.recDE, R.id.recDL, R.id.recDEE, R.id.recDLL);
        initBotones(btn_reproducir, false, R.id.playNE, R.id.playNL, R.id.playDE, R.id.playDL);
        initBotones(btn_detener, false, R.id.stopNE, R.id.stopNL, R.id.stopDE, R.id.stopDL);
        setViews(true, false, (TextView)findViewById(R.id.txt_captura));
        setViews(false, false, btn_grabar[audios]);
        setViews(false, false, btn_grabar[audios+1]);
        editText_id.setText("");
        editText_esp.setText("");
        proceso = "creación";
        reproducir3 = false;
        reproducir4 = false;
        activaBotones();
        initMenu();
    }

    /**
     * Método que asigna la acción de los botones para guardar, salir y el
     * que carga la imagen capturada.
     */
    protected void activaBotones() {
        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardar(true);
            }
        });
        btnseguir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardar(false);
            }
        });
        btn_imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capturaImagen();
            }
        });
        for (int i = 0; i < audios; i++) {
            final boolean hayBarra = i > 1; final int n = i;
            btn_grabar[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) { grabar(n, hayBarra);
                }
            });
            btn_reproducir[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) { reproducir(n, hayBarra);
                }
            });
            btn_detener[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) { detener(n, hayBarra);
                }
            });
        }
        btn_grabar[audios].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { grabar(audios, true);
            }
        });
        btn_grabar[audios+1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { grabar(audios+1, true);
            }
        });
        btn_imagen.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (bm_imagen == null) return false;
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) return false;
                boolean actual = !datos.getBoolean("está girada", false);
                if (actual) {
                    confirmar(getString(R.string.correccion_activada));
                    onActivityResult(1, RESULT_OK, new Intent());
                } else confirmar(getString(R.string.correcion_desactivada));
                datos.edit().putBoolean("está girada", actual).commit();
                return true;
            }
        });
    }

    /**
     * Método que realiza la inserción de la tarjeta.
     *
     * @param sePideSalir Se decide si se regresa al inicio.
     */
    protected void guardar(final boolean sePideSalir) {
        idioma = editText_id.getText().toString().trim();
        español = editText_esp.getText().toString().trim();
        categoria = listaCategorias.getSelectedItemPosition();
        String noIngresados = sonDatosValidos();
        if (noIngresados.isEmpty()) {
            creada = new Tarjeta(idioma, español, categoria - 1);
            final Dialog p = avisar("Guardando tarjeta ...");
            // Crear un hilo hace que la aplicación no se detenga y aparezca el aviso.
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final boolean procesos[] = new boolean[2];
                    procesos[0] = commit();
                    if (procesos[0]) procesos[1] = subirDatos(creada, archivo);
                    // Se require un hilo de este tipo cambios visuales en el activity.
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (procesos[0]) {
                                if (procesos[1]) {
                                    Toast.makeText(CreaTarjeta.this, "Tarjeta guardada", Toast.LENGTH_LONG).show();
                                    p.dismiss(); setResult(RESULT_OK);
                                    if (sePideSalir) finish();
                                    else initDatos();
                                } else {
                                    String msg = "No se pudieron subir los archivos, Intente más tarde.";
                                    Toast.makeText(CreaTarjeta.this, msg, Toast.LENGTH_LONG).show();
                                }
                            } else {
                                String msg = (getResponse().contains("mismo nombre"))?
                                        "Ya has agregado otra tarjeta con ese nombre":
                                        "Verifique su conexión y vuelva a intentarlo.";
                                Toast.makeText(CreaTarjeta.this, msg, Toast.LENGTH_LONG).show();
                                //dataState(getApplicationContext());
                            } p.dismiss();
                        }
                    });
                }
            }).start();
        } else {
            String msg = "Falta " + noIngresados;
            Toast.makeText(CreaTarjeta.this, msg, Toast.LENGTH_LONG).show();
        }
    }

    protected boolean commit() { return creada.insertar(); }

    /**
     * Método que verifica los datos ingresados antes de registrarlos.
     *
     * @return Devuelve TRUE si los datos pueden ser registrados.
     */
    protected String sonDatosValidos() {
        String nulos = "";
        boolean hayDatos = !idioma.isEmpty() && !español.isEmpty(),
                hayNombres = archivo[0] != null && archivo[1] != null,
                hayDescripciones = archivo[2] != null && archivo[3] != null,
                hayImagen = archivo[4] != null,
                hayCategoria = categoria > 0;
        if (!hayDatos) nulos += "escribir nombres, ";
        if (!hayNombres) nulos += "grabar nombres, ";
        if (!hayDescripciones) nulos += "grabar descripciones, ";
        if (!hayImagen) nulos += "capturar foto, ";
        if (!hayCategoria) nulos += "elegir una categoría, ";
        if (!nulos.isEmpty()) nulos = nulos.substring(0, nulos.length() - 2);
        int last = nulos.lastIndexOf(", ");
        if (last < 0) return nulos;
        return nulos.substring(0, last) + " y " + nulos.substring(last + 2);
    }

    /**
     * Método que se encarga de obtener la imagen de la tarjeta.
     */
    protected void capturaImagen() {
        //Creamos el Intent que abre la cámara.
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Creamos el archivo en la memoria interna.
        archivo[4] = captura(false);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            archivo[5] = new File(getExternalCacheDir().getPath(), "original.jpg");
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(archivo[5]));
        } else {
            archivo[5] = captura(true);
            // Usamos el proveedor de archivos para usar la ruta interna.
            Uri uriSavedImage = FileProvider.getUriForFile(
                    getApplicationContext(), "com.loteria.fileprovider", archivo[5]);
            // Le decimos al intent la ruta para guardar la imagen.
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        } //Lanzamos la aplicacion de la camara con retorno (forResult)
        startActivityForResult(cameraIntent, 1);
    }

    /**
     * Método que decodifica la imagen obtenida con la cámara.
     *
     * @param resultCode Código de respuesta.
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        fab.setImageResource(getAvatar());
        nombre_usuario.setText(getNombre());
        // Comprobamos que la foto se tome correctamente.
        if (requestCode == 1 && resultCode == RESULT_OK) {
            try {
                btnguardar.setEnabled(false); btnseguir.setEnabled(false);
                // Se asegura la espera para guardar imagen.
                setViews(false, false, findViewById(R.id.txt_captura));
                int btn_w = btn_imagen.getWidth(), btn_h = btn_imagen.getHeight();
                Bitmap bm[] = setBitmap(true, archivo[5], btn_imagen), mini = bm[1];
                bm_imagen = bm[0];
                if (corrigeOrientacion(w, h)) {
                    bm = setBitmap(true, bm_imagen, btn_imagen, btn_w, btn_h);
                    mini = bm[1];
                } sv_imagen.scrollTo(0, (btn_imagen.getHeight() - sv_imagen.getHeight()) / 2);
                FileOutputStream fos = new FileOutputStream(archivo[4]);
                OutputStream os = new BufferedOutputStream(fos);
                mini.compress(Bitmap.CompressFormat.JPEG, 20, os);
                os.close();
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    fos = new FileOutputStream(archivo[5] = captura(true));
                    os = new BufferedOutputStream(fos);
                    mini.compress(Bitmap.CompressFormat.JPEG, 20, os);
                    os.close();
                }
            } catch (Exception e) {
                Toast.makeText(CreaTarjeta.this, "Error al guardar la imagen" + e.toString(), Toast.LENGTH_SHORT);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    Toast.makeText(CreaTarjeta.this, "Asegúrese de tener monatada una tarjeta externa." + e.toString(), Toast.LENGTH_LONG).show();
            } // Se libera accion de botones.
            btnguardar.setEnabled(true); btnseguir.setEnabled(true);
        } else {
            if (bm_imagen != null) {
                btnguardar.setEnabled(false); btnseguir.setEnabled(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            OutputStream os = new BufferedOutputStream(new FileOutputStream(archivo[5]));
                            bm_imagen.compress(Bitmap.CompressFormat.JPEG, 100, os);
                            os.close();
                        } catch (Exception e) {
                            Toast.makeText(CreaTarjeta.this, "Error al guardar la imagen", Toast.LENGTH_SHORT);
                        } runOnUiThread(new Runnable() {
                            @Override
                            public void run() { btnguardar.setEnabled(true); btnseguir.setEnabled(true); }
                        });
                    }
                }).start();
            } else archivo[4] = null;
        }
    }

    private boolean corrigeOrientacion (int w, int h) {
        try {
            if (!datos.getBoolean("está girada", false)) return false;
            ExifInterface ei = new ExifInterface(captura(true).getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            int grados = 0;
            switch(orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90: grados = 90; break;
                case ExifInterface.ORIENTATION_ROTATE_180: grados = 180; break;
                case ExifInterface.ORIENTATION_ROTATE_270: grados = 270; break;
            } Matrix matrix = new Matrix();
            matrix.postRotate(grados);
            bm_imagen = Bitmap.createBitmap(bm_imagen, 0, 0, w, h, matrix, true);
            return true;
        } catch (Exception e) { return false;}
    }

    /**
     * Método que inicializa las barras de progreso de audio con invisibilidad.
     *
     * @param barra Conjunto de barras a inicializar.
     * @param id    Identificadores ordenados para inicializar las barras.
     */
    protected void initBarra(SeekBar[] barra, boolean visible, int... id) {
        for (int i = 0; i < id.length; i++) {
            barra[i] = (SeekBar) this.findViewById(id[i]);
            setViews(visible, visible, barra[i]);
        }
    }

    /**
     * Método que inicializa botones con visibilidad definida.
     *
     * @param botones   Botones Conjunto de botones a inicializar.
     * @param visible Indica si el boton se inicializa invisible.
     * @param id        Identificador del botón.
     */
    protected void initBotones(ImageButton[] botones, boolean visible, int... id) {
        for (int i = 0; i < id.length; i++) {
            botones[i] = (ImageButton) this.findViewById(id[i]);
            setViews(visible, visible, botones[i]);
        }
    }

    /**
     * Método que realiza la acción de un botón para grabar un audio.
     *
     * @param i        Es el indice del recurso de audio utilizado.
     * @param hayBarra Indica si es un audio con barra para desactivar el botón de grabar.
     */
    protected void grabar(int i, boolean hayBarra) {
        try {
            if(!permisoDeAudio()) return;
            if(i >= audios){
                recorder[i-2] = new MediaRecorder();
                recorder[i-2].setAudioSource(MediaRecorder.AudioSource.MIC);
                recorder[i-2].setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
                recorder[i-2].setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                archivo[i-2] = grabacion(i);
                recorder[i-2].setOutputFile(archivo[i-2].getAbsolutePath());
                recorder[i-2].prepare();
                recorder[i-2].start();
                for (ImageButton b : btn_grabar) b.setEnabled(false);
                setViews(false, false, btn_reproducir[i-2]);
                setViews(true, true, btn_detener[i-2]);
                setViews(false, false, btn_grabar[i-2]);
                setViews(false, false, barra_audio[i % barras]);
                return;
            }
            if(i == 2) reproducir3 = true;
            if(i == 3) reproducir4 = true;
            if(i == 4) reproducir3 = true;
            if(i == 5) reproducir4 = true;

            recorder[i] = new MediaRecorder();
            recorder[i].setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder[i].setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
            recorder[i].setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            archivo[i] = grabacion(i);
            recorder[i].setOutputFile(archivo[i].getAbsolutePath());
            recorder[i].prepare();
            recorder[i].start();
            for (ImageButton b : btn_grabar) b.setEnabled(false);
            setViews(false, false, btn_reproducir[i]);
            setViews(true, true, btn_detener[i]);
            if (!hayBarra) return;
            setViews(false, false, btn_grabar[i]);
            //Toast.makeText(CreaTarjeta.this, "Grabando...", Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(CreaTarjeta.this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Método que realiza la acción de un botón para reproducir un audio.
     *
     * @param i        Es el indice del recurso de audio utilizado.
     * @param hayBarra Indica si es un audio con barra para mostrar el progreso.
     */
    protected void reproducir(final int i, boolean hayBarra) {
        try {
            if (reproduciendo >= 0) {
                if (player[reproduciendo].isPlaying()) return;
                //detener(reproduciendo, reproduciendo > 1);
            } reproduciendo = i;
            player[i] = new MediaPlayer();
            player[i].setDataSource(archivo[i].getAbsolutePath());
            player[i].prepare();
            player[i].start();
            //Toast.makeText(CreaTarjeta.this, "Reproduciendo...", Toast.LENGTH_SHORT).show();
            if (!hayBarra) return;
            // Se bloquea reproduccion concurrente de otros audios.
            hilo_barra[i % barras] = new Thread() {
                @Override
                public void run() {
                    int duracion = player[i].getDuration();
                    barra_audio[i % barras].setMax(duracion);
                    int posicionActual = 0;
                    int ejecucion = 0;
                    boolean ban = false;
                    while (posicionActual <= duracion) {
                        try {
                            sleep(500);
                            posicionActual = player[i].getCurrentPosition();
                            barra_audio[i % barras].setProgress(posicionActual);
                        } catch (Exception e) {
                            Toast.makeText(CreaTarjeta.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    } barra_audio[i % barras].setProgress(0);
                }
            };
            hilo_barra[i % barras].start();
        }catch (Exception e) {
            Toast.makeText(CreaTarjeta.this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Método que realiza la acción de un botón para detener un audio.
     *
     * @param i        Es el indice del recurso de audio utilizado.
     * @param hayBarra Indica si es un audio con barra para activarla.
     */
    protected void detener(int i, boolean hayBarra) {
        try { hayGrabacion = true;
            recorder[i].stop();
            recorder[i].release();
                for(int j = 0; j < btn_grabar.length - 2; j++) {
                    setViews(true, true, btn_grabar[j]);
                    if(j == 2) {
                        if (reproducir3 == true){
                            setViews(false, false, btn_grabar[j]);
                            setViews(true, true, btn_grabar[j+2]);
                        }
                    }
                    if(j == 3){
                        if(reproducir4 == true){
                            setViews(false, false, btn_grabar[j]);
                            setViews(true, true, btn_grabar[j+2]);
                        }
                    }
                }
            setViews(false, false, btn_detener[i]);
            setViews(true, true, btn_reproducir[i]);
            if (!hayBarra) return;
            setViews(false, false, btn_grabar[i]);
            setViews(true, true, barra_audio[i % barras]);
            setViews(true, true, btn_grabar[i+2]);
            //Toast.makeText(CreaTarjeta.this, "Deteniendo...", Toast.LENGTH_SHORT).show();
        }catch (Exception e) {
            Toast.makeText(CreaTarjeta.this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Método que inicializa la barra del menú.
     */
    protected void initMenu() {
        listaCategorias = (Spinner)this.findViewById(R.id.categoria_spinner);
        this.listaCategorias.setAdapter(new AdapterCategoria(this, font, "Elegir categoria:"));
        final boolean fueDesplegada[] = new boolean[1]; // Se inicaliza un booleano en falso.
        this.listaCategorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (fueDesplegada[0] && pos > 0) { // Si se elige una categoría (segunda posición hacia abajp en la lista.)
                    Toast.makeText(CreaTarjeta.this, "Categoría seleccionada " + categorias[pos - 1], Toast.LENGTH_LONG).show();
                } fueDesplegada[0] = true; // Se cambia a true porque se ha desplegado la lista.
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }

    @Override
    protected void initGuia() {
        if (datos.getBoolean("guía crear no leida por " + getNombre(), false)) {
            datos.edit().remove("guía crear no leida por " + getNombre()).commit();
            final TextView guia[] = new TextView[5];
            guia[0] = (TextView)findViewById(R.id.tv_guia_nombres);
            guia[1] = (TextView)findViewById(R.id.tv_guia_categoria);
            guia[2] = (TextView)findViewById(R.id.tv_guia_foto);
            guia[3] = (TextView)findViewById(R.id.tv_guia_descripcion);
            guia[4] = (TextView)findViewById(R.id.tv_guia_terminada);
            setViews(true, true, guia);
            for (final TextView t: guia) {
                t.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setViews(false, false, t);
                    }
                });
            } setFont(guia);
        }
    }

    @Override
    public void onBackPressed() {
        if (!proceso.isEmpty()) {
            Editable e = editText_esp.getText(), i = editText_id.getText();
            if (bm_imagen != null || hayGrabacion || !(e.toString() + i.toString()).isEmpty())
            preguntar(getString(R.string.cancelar_proceso).replace("?", proceso),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setResult(RESULT_CANCELED);
                            CreaTarjeta.super.onBackPressed();
                        }
                    });
            else super.onBackPressed();
        } else super.onBackPressed();
    }
}