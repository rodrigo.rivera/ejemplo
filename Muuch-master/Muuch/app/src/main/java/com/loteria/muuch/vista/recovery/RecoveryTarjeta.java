package com.loteria.muuch.vista.recovery;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.vista.crea_tarjeta.EditaTarjeta;
import com.loteria.muuch.vista.ingreso.IniciarSesion;
import com.loteria.muuch.vista.ingreso.Inicio;

import java.util.ArrayList;
import java.util.Random;

import static com.loteria.muuch.modelo.Contenido.setBitmap;
import static com.loteria.muuch.modelo.Persistencia.getAvatar;
import static com.loteria.muuch.modelo.Persistencia.getNombre;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.guardaSesion;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.categorias;

public class RecoveryTarjeta extends EditaTarjeta {

    public int reconocidas;
    private ArrayList<Tarjeta> disponibles;
    private boolean primera = true;
    private int indice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        disponibles = getUsuario().getTarjetas();
        reconocidas = disponibles.size();
        if (reconocidas > 2) reconocidas = 2;
        indice = new Random().nextInt(disponibles.size());
        super.onCreate(savedInstanceState);
        nombre_usuario.setText(getNombre());
        fab.setImageResource(getAvatar());
    }

    @Override
    protected void initDatos() {
        creada = disponibles.get((indice++) % disponibles.size());
        super.initDatos();
        proceso = "";
        String aviso = "";
        if (primera) {
            primera = false;
            aviso = getString(R.string.recovery_tarjeta)
                    .replace("?", "" + reconocidas) + " ";
        } setViews(false, false, btn_reproducir);
        setViews(false, false, findViewById(R.id.rl_descripciones));
        confirmar(aviso + getString(R.string.recovery_tarjeta_datos));
        editText_esp.setText(" ");
        editText_id.setText(" ");
        listaCategorias.setSelection(new Random().nextInt(categorias.length));
        actualizar.setText("continuar");
        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sonDatosIguales()) {
                    if (reconocidas == 1) {
                        if (guardaSesion()) {
                            confirmar(getString(R.string.recovery_ok),
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            setResult(RESULT_OK);
                                            finish();
                                        }
                                    });
                        } else confirmar(getString(R.string.error_reinicio),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) { setResult(RESULT_CANCELED); finish();
                                    }
                                });
                    } else {
                        reconocidas--;
                        initDatos();
                    }
                } else {
                    confirmar(getString(R.string.tarjeta_falsa),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }
                            });
                }
            }
        });
    }

    private boolean sonDatosIguales() {
        español = editText_esp.getText().toString().trim();
        idioma = editText_id.getText().toString().trim();
        categoria = listaCategorias.getSelectedItemPosition();
        return español.equals(creada.getNombreEsp()) &&
                idioma.equals(creada.getNombreLen()) &&
                (categoria == creada.getCategoria());
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }
}