package com.loteria.muuch.vista.mis_tarjetas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.vista.MenuActivity;

import java.util.ArrayList;

import static com.loteria.muuch.modelo.Persistencia.getColeccion;
import static com.loteria.muuch.modelo.Persistencia.getCompartidas;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.vista.mis_tarjetas.MisTarjetas.query;

public class MisCategorias extends MenuActivity {

    protected ArrayList<Tarjeta>[] coleccionPorCategoria, compartidasPorCategoría;
    public static ArrayList<Tarjeta> coleccionadas, compartidas;
    public static int seleccionada, resources[];
    public static ImageView iv_categorias[];
    protected TextView[] cantidades;
    /** Lista final de categorías. */
    public static final String categorias[] = {"Plantas","Comida","Fiestas","Artesanias",
            "Personajes", "Objetos", "Milpa", "Animales", "Agua", "Clima", "Otros"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getUsuario() == null) { finish(); return; }
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_mis_categorias);
        seleccionada = -1;
        cantidades = new TextView[]{
                (TextView)findViewById(R.id.tv_n_plantas),
                (TextView)findViewById(R.id.tv_n_comida),
                (TextView)findViewById(R.id.tv_n_fiestas),
                (TextView)findViewById(R.id.tv_n_artesania),
                (TextView)findViewById(R.id.tv_n_personajes),
                (TextView)findViewById(R.id.tv_n_objetos),
                (TextView)findViewById(R.id.tv_n_milpa),
                (TextView)findViewById(R.id.tv_n_animales),
                (TextView)findViewById(R.id.tv_n_agua),
                (TextView)findViewById(R.id.tv_n_clima),
                (TextView)findViewById(R.id.tv_n_otros)};
        iv_categorias = new ImageView[]{
                (ImageView)findViewById(R.id.iv_plantas),
                (ImageView)findViewById(R.id.iv_comida),
                (ImageView)findViewById(R.id.iv_fiestas),
                (ImageView)findViewById(R.id.iv_artesania),
                (ImageView)findViewById(R.id.iv_personajes),
                (ImageView)findViewById(R.id.iv_objetos),
                (ImageView)findViewById(R.id.iv_milpa),
                (ImageView)findViewById(R.id.iv_animales),
                (ImageView)findViewById(R.id.iv_agua),
                (ImageView)findViewById(R.id.iv_clima),
                (ImageView)findViewById(R.id.iv_otros)};
        initDatos();
        setFont(cantidades);
        setFont((TextView)findViewById(R.id.nombre_usuario),
                (TextView)findViewById(R.id.tv_plantas),
                (TextView)findViewById(R.id.tv_comida),
                (TextView)findViewById(R.id.tv_fiestas),
                (TextView)findViewById(R.id.tv_fiestas),
                (TextView)findViewById(R.id.tv_artesania),
                (TextView)findViewById(R.id.tv_personajes),
                (TextView)findViewById(R.id.tv_objetos),
                (TextView)findViewById(R.id.tv_milpa),
                (TextView)findViewById(R.id.tv_animales),
                (TextView)findViewById(R.id.tv_agua),
                (TextView)findViewById(R.id.tv_clima),
                (TextView)findViewById(R.id.tv_otros));
    }

    @Override
    protected void initDatos() {
        initResources();
        int n = categorias.length;
        coleccionPorCategoria = new ArrayList[n];
        compartidasPorCategoría = new ArrayList[n];
        for (int i = 0; i < n; i++) {
            coleccionPorCategoria[i] = new ArrayList<Tarjeta>();
            compartidasPorCategoría[i] = new ArrayList<>();
        } filtra();
        for (int i = 0; i < n; i++) {
            final int c = i;
            iv_categorias[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    avanza(c);
                }
            });
        }
    }

    protected void filtra() {
        ArrayList<Tarjeta> coleccion = getColeccion(), compartidas = getCompartidas();
        int n = categorias.length;
        for (Tarjeta t: coleccion) coleccionPorCategoria[t.getCategoria()].add(t);
        for (Tarjeta t: compartidas) compartidasPorCategoría[t.getCategoria()].add(t);
        for (int i = 0; i < n; i++) {
            int size = coleccionPorCategoria[i].size();
            //size += compartidasPorCategoría[i].size();
            cantidades[i].setText(" " + size + " ");
        }
    }

    protected void avanza(int i) {
        coleccionadas = coleccionPorCategoria[i];
        compartidas = compartidasPorCategoría[i];
        seleccionada = i;
        query = "";
        startActivityForResult(new Intent(MisCategorias.this, MisTarjetas.class), 2);
    }

    public static int[] initResources() {
        return resources = new int[]{
                R.drawable.ic_plantas, R.drawable.ic_comida,
                R.drawable.ic_fiestas, R.drawable.ic_artesania,
                R.drawable.ic_personajes, R.drawable.ic_objetos,
                R.drawable.ic_milpa, R.drawable.ic_animales,
                R.drawable.ic_agua, R.drawable.ic_clima,
                R.drawable.ic_otros};
    }
}
