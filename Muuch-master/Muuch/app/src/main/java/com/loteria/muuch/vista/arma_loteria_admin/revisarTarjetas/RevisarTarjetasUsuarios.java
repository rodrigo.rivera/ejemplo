package com.loteria.muuch.vista.arma_loteria_admin.revisarTarjetas;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin;
import com.loteria.muuch.vista.arma_loteria_user.AdapterSeleccionTarjeta;
import com.loteria.muuch.vista.arma_loteria_user.jugando_loteria.AdapterCarta;

import java.util.ArrayList;

import static com.loteria.muuch.modelo.Persistencia.getNombre;
import static com.loteria.muuch.modelo.Persistencia.getTarjeta;

/*
Clase para mostrar las tarjetas que los usuarios enviaron para poder jugar la partida
 */
public class RevisarTarjetasUsuarios extends MenuActivity {

    private GridView gridView;
    //todas las tarjetas de los usuarios
    private ArrayList<Tarjeta> listaTarjetas;
    //el id de todas las tarjetas de los usurios.
    public static ArrayList<String> listaIdTarjetas = new ArrayList<>();
    private TextView nombreAdmin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_armaloteria_user);



        this.nombreAdmin = (TextView) findViewById(R.id.textView7);
        nombreAdmin.setText(getNombre());
        listaTarjetas = new ArrayList<>();

        //crea las tarjetas a partir de los id de la lisra "listaIdTarjetas"
        for(int i = 0; i<listaIdTarjetas.size(); i++){
            listaTarjetas.add(getTarjeta(Long.parseLong(listaIdTarjetas.get(i))));
        }

        gridView = gridView = (GridView) findViewById(R.id.gridViewMisTarjetas_user);
        gridView.setAdapter(new AdapterCarta(this, font, listaTarjetas));
        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.nav_principal);
        SpannableString s = new SpannableString(menuItem.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.RED),0,s.length(),0);
        s.setSpan(new AbsoluteSizeSpan(100, true),0, s.length(),0);

    }
}
