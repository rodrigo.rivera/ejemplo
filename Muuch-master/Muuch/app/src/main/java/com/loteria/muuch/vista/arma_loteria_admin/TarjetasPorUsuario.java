package com.loteria.muuch.vista.arma_loteria_admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Tarjeta;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.arma_loteria_admin.jugando_admin.JugandoAdmin;
import com.loteria.muuch.vista.arma_loteria_admin.revisarTarjetas.RevisarTarjetasUsuarios;
import com.loteria.muuch.vista.arma_loteria_user.AdapterSeleccionTarjeta;
import com.loteria.muuch.modelo.Persistencia;
import com.loteria.muuch.vista.arma_loteria_user.jugando_loteria.AdapterCarta;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static com.loteria.muuch.vista.arma_loteria_admin.revisarTarjetas.RevisarTarjetasUsuarios.listaIdTarjetas;

/**
 * Nueva activity para mostrar tarjetas enviadas por un usuario.
 * Hubo que declararla en el manifest.
 * Created by PATLANIUNAM on 04/12/2017.
 */

public class TarjetasPorUsuario extends MenuActivity {

    GridView gv_tarjetas;
    public static ArrayList<Tarjeta> enviadas;
    private TextView nombreUsuario;

    private ImageButton regresar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Usamos la misma pantalla de seleccionar tarjetas.
        super.init(R.layout.activity_armaloteria_user);

        regresar = (ImageButton) findViewById(R.id.imageButton21);
        regresar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                startActivity( new Intent(TarjetasPorUsuario.this, JugandoAdmin.class));

            }
        });

        this.nombreUsuario = (TextView) findViewById(R.id.textView7);
        nombreUsuario.setText(Persistencia.getNombre());
        // Usamos el mismo gridview del usuario al seleccionar tarjetas.
        gv_tarjetas = (GridView) findViewById(R.id.gridViewMisTarjetas_user);
        // Agregamos las tarjetas enviadas desde otro activity (filtradas por usuario).
        gv_tarjetas.setAdapter(new AdapterCarta(this, font, enviadas));
        // Modificamos la etiqueta que ya tenia para seleccionar tarjetas.
        TextView etiqueta = (TextView)findViewById(R.id.textView5);
        etiqueta.setText("Puedes quitar las tarjetas repetidas.");
        setFont(etiqueta);
        // Inhabilitamos la segunda etiqueta de cantidad de tarjetas.
        setViews(false, false, findViewById(R.id.textView6));
        confirmar("Lista enviada y filtrada: " + enviadas.toString());
    }
}
