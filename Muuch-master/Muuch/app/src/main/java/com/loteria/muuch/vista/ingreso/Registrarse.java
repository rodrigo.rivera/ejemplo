package com.loteria.muuch.vista.ingreso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;

import static com.loteria.muuch.modelo.Persistencia.cargaSesion;
import static com.loteria.muuch.modelo.Persistencia.dataState;
import static com.loteria.muuch.modelo.Persistencia.getError;
import static com.loteria.muuch.modelo.Persistencia.getId;
import static com.loteria.muuch.modelo.Persistencia.getNombre;
import static com.loteria.muuch.modelo.Persistencia.getResponse;
import static com.loteria.muuch.modelo.Persistencia.getUsuario;
import static com.loteria.muuch.modelo.Persistencia.guardaSesion;
import static com.loteria.muuch.modelo.Persistencia.login;
import static com.loteria.muuch.modelo.Persistencia.online;
import static com.loteria.muuch.modelo.Persistencia.terminaSesion;

public class Registrarse extends MenuActivity {

    protected String nombre, contraseña, correo;
    protected Integer avatar = -1;
    protected GridView avatares;
    protected TextView continuar, elige_avatar;
    protected EditText edit_nombre, edit_contraseña, edit_correo;
    protected AvatarAdapter adapter;
    public static Usuario registrado;
    public static String guias[] = {"crear", "tarjetas", "amigos", "armar"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init(R.layout.activity_registrarse);
        edit_nombre = (EditText) findViewById(R.id.reg_nombre);
        edit_contraseña = (EditText) findViewById(R.id.reg_contraseña);
        edit_correo = (EditText) findViewById(R.id.reg_correo);
        continuar = (TextView) findViewById(R.id.reg_aceptar);
        elige_avatar = (TextView) findViewById(R.id.tv_elige_avatar);
        initDatos();
        String hint = edit_nombre.getHint() + " ";
        edit_nombre.setHint(hint + getString(R.string.longitud));
        hint = edit_contraseña.getHint() + " ";
        edit_contraseña.setHint(hint + getString(R.string.longitud));
        setFont(continuar, edit_contraseña, edit_correo, edit_nombre, elige_avatar);
    }

    @Override
    protected void initDatos() {
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nombre = edit_nombre.getText().toString().trim();
                correo = edit_correo.getText().toString().trim();
                contraseña = edit_contraseña.getText().toString().trim();
                String restantes = sonDatosValidos();
                if(restantes.isEmpty()) {
                    registrado = new Usuario(nombre, correo, contraseña, avatar);
                    if (commit() && guardaSesion()) {
                        //dataState(Registrarse.this);
                        avanza();
                        finish();
                    } else {
                        String error = getError(), res = getResponse();
                        String msj = (error.contains("insertId") || res.contains("Ya existe"))?
                                "Ya existe otro usuario con el mismo nombre." : res + ", " + error;
                        Toast.makeText(Registrarse.this, msj, Toast.LENGTH_LONG).show();
                    }
                } else Toast.makeText(Registrarse.this, "Debe " + restantes + " para continuar", Toast.LENGTH_LONG).show();
            }
        }); defineLista();
        avatares.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    protected boolean commit() {
        return !online? registrado.insertar():
        registrado.insertar() && login(registrado.getNombre(), registrado.getContraseña());
    }

    protected void defineLista() {
        avatares = (GridView) findViewById(R.id.reg_avatar);
        adapter = new AvatarAdapter(this, avatar);
        avatares.setAdapter(adapter);
        final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        avatares.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                View nuevo = adapterView.getChildAt(i - adapterView.getFirstVisiblePosition());
                View previo = adapter.getSeleccionado();
                avatar = i;
                if (previo != null) previo.setBackgroundColor(Color.TRANSPARENT);
                if (nuevo != null) {
                    adapter.setSeleccionado(nuevo).setBackgroundColor
                    (getResources().getColor(R.color.amarillo_629));
                    fab.setVisibility(View.VISIBLE);
                    fab.setImageResource(AvatarAdapter.lista[avatar]);
                }
            }
        });
    }

    /**
     * Método que verifica los datos ingresados antes de registrarlos.
     *
     * @return Devuelve TRUE si los datos pueden ser registrados.
     */
    protected String sonDatosValidos() {
        boolean correoValido = validarCorreo();
        String nulos = ((nombre.isEmpty() || contraseña.length() < 3 || !correoValido)?
                "ingresar " : "") + (nombre.isEmpty()? "un nombre, ": "")
                + (contraseña.length() < 3? "una contraseña de al menos 3 caracteres, ": "")
                + ((!correoValido)? "un correo válido, ": "")
                + ((avatar < 0)? "elegir un avatar, ": "");
        if (!nulos.isEmpty()) nulos = nulos.substring(0, nulos.length() - 2);
        int last = nulos.lastIndexOf(", ");
        if (last < 0) return nulos;
        return nulos.substring(0, last) + " y " + nulos.substring(last + 2);
    }

    protected boolean validarCorreo() {
        if (correo.isEmpty()) return true;
        String partes[] = correo.split("@");
        if (partes.length != 2) return false;
        if (partes[0].trim().isEmpty()) return false;
        String dominio = partes[1];
        return dominio.equals("gmail.com") ||
                dominio.equals("ciencias.unam.mx") ||
                dominio.equals("outlook.com") ||
                dominio.equals("hotmail.com");
    }

    @Override
    public void onBackPressed() {
        if (cargaSesion()) super.onBackPressed();
        else startActivity(new Intent(Registrarse.this, IniciarSesion.class));
        finish();
    }

    protected void avanza() {
        for (int i= 0; i< guias.length; i++) {
            datos.edit().putBoolean("guía " + guias[i] + " no leida por " + getNombre(), true).commit();
        } startActivity(new Intent(Registrarse.this, Inicio.class));
    }

    @Override
    protected void initGuia() {
        if (!datos.getBoolean("guía registrar leida", false)) {
            datos.edit().putBoolean("guía registrar leida", true).commit();
            confirmar(getString(R.string.guia_registro));
        }
    }
}