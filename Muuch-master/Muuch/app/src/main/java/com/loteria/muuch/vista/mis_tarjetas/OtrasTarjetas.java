package com.loteria.muuch.vista.mis_tarjetas;

import android.view.View;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.vista.ingreso.AvatarAdapter;

import static com.loteria.muuch.vista.mis_tarjetas.OtrasCategorias.amigo;
import static com.loteria.muuch.vista.mis_tarjetas.MisCategorias.compartidas;

public class OtrasTarjetas extends MisTarjetas {

    @Override
    public void initDatos() {
        setListas(tarjetas = coleccionadasFiltradas(), otras = compartidas);
        initGridViews(gv_inferior, gv_superior);
        fab.setImageResource(AvatarAdapter.getAvatar(amigo));
        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return false;
            }
        });
        nombre_usuario.setText(amigo.getNombre());
        TextView tv = (TextView) findViewById(R.id.tv_otras_tarjetas);
        tv.setText("Mis Tarjetas");
        tv = (TextView) findViewById(R.id.tv_mis_tarjetas);
        tv.setText("Tarjetas de " + amigo.getNombre());
        setFontTitle("Tarjetas de tu amigo");
    }

    @Override
    protected void filtrar(String s) {
        super.filtrar(s);
        gv_superior.setAdapter(new TarjetaAdapter(OtrasTarjetas.this, font, consulta));
        gv_inferior.setAdapter(new TarjetaAdapter(OtrasTarjetas.this, font, edicion));
    }
}