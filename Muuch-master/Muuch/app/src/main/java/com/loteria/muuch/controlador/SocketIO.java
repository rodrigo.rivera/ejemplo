package com.loteria.muuch.controlador;

import android.app.Application;
import android.widget.Toast;
import io.socket.client.IO;
import io.socket.client.Socket;
import com.loteria.muuch.modelo.Persistencia;


/**
 * Created by hgome on 02/11/2017.
 */

public class SocketIO extends Application {

    private  Socket mSocket;{

        try{
            mSocket = IO.socket(Persistencia.serverURL);
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(), "Error en el socketIO", Toast.LENGTH_LONG).show();
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}
