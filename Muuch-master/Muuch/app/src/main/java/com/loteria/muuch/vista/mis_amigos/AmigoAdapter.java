package com.loteria.muuch.vista.mis_amigos;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.loteria.muuch.R;
import com.loteria.muuch.modelo.Usuario;
import com.loteria.muuch.vista.MenuActivity;
import com.loteria.muuch.vista.ingreso.AvatarAdapter;

import java.util.ArrayList;

/**
 * Created by PATLANIUNAM on 01/10/2017.
 */

public class AmigoAdapter extends BaseAdapter {


    Context contexto;
    ArrayList<Usuario> lista;
    Typeface font;

    public AmigoAdapter(Context contexto, Typeface font, ArrayList<Usuario> lista) {
        this.contexto = contexto;
        this.lista = lista;
        this.font = font;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null){
            LayoutInflater inflater = LayoutInflater.from(contexto);
            view  =inflater.inflate(R.layout.adapter_amigo, null);
        } Usuario actual = lista.get(i);
        TextView nombre = (TextView) view.findViewById(R.id.tv_adapter_nombre);
        nombre.setTypeface(font);
        ImageView avatar = (ImageView) view.findViewById(R.id.adapter_avatar);
        avatar.setPadding(1, 1, 1, 1);
        try {
            avatar.setImageResource(AvatarAdapter.getAvatar(actual));
        } catch (Exception e){avatar.setImageResource(R.drawable.ic_a_1);}
        nombre.setText(actual.getNombre());
        return view;
    }
}
