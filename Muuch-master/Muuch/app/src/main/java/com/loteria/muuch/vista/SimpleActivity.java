package com.loteria.muuch.vista;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.loteria.muuch.R;

import static com.loteria.muuch.vista.MenuActivity.monitorMode;

/**
 * Clase genérica para activities sin menú hamburguesa.
 */
public class SimpleActivity extends AppCompatActivity {

    public Toolbar toolbar;
    public FloatingActionButton fab;
    public Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void init(int layout) {
        setContentView(layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        font = Typeface.createFromAsset(getAssets(), "fonts/VAG Rounded Bold.ttf");
        Spannable stitle = new SpannableString(getTitle());
        stitle.setSpan(new MenuActivity.CustomTypefaceSpan("", font), 0, stitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTitle(stitle);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                monitorMode = !monitorMode;
                String msj = "Monitor " + ((monitorMode)? "activado":"desactivado");
                Snackbar.make(view, msj, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                TextView tv = (TextView)findViewById(R.id.tv_ayuda);
                if (monitorMode && tv != null) tv.setText(getString(R.string.txt_ayuda)
                + "\n\nAtajos especiales:"
                + "\nSe puede encender y apagar el monitor de persistencia con un toque sostendio en el avatar de esta pantalla"
                + "\nSe puede eliminar la base de datos con un toque sostenido sobre el logo de Muuch");
                return monitorMode;
            }
        });
    }

    public void setFont(TextView ... v) {for (TextView tv: v) tv.setTypeface(font);}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
        } return super.onOptionsItemSelected(item);
    }
}
