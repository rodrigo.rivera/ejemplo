use muuch;
-- Se consulta al usuario Chiquillo por nombre y contraseña.
SELECT * from usuario where nombre='Chiquillo' and contraseña='123';
-- Se consultan todos los datos de todos los usuarios.
SELECT * FROM usuario;
-- Se consulta la tarjeta con id igual a 1.
SELECT * from tarjeta where idtarjeta='1';
-- Se consultan los amigos del usuario Chiquillo con id igual a 1.
SELECT idusuario, nombre, contraseña, correo
FROM (SELECT idamigo FROM amigo WHERE idusuario='2') as amigo
INNER JOIN usuario ON amigo.idamigo = usuario.idusuario;
-- Se consultan todas las tarjetas.
SELECT * FROM tarjeta;
-- Se consultan las tarjetas del usuario Chiquillo con id igual a 1.
SELECT * FROM (SELECT * FROM tarjeta WHERE nombre = 'Luis' AND contraseña = '123')
INNER JOIN (SELECT idusuario, nombre, correo, avatar) as usuario
ON tarjeta.idusuario = usuario.idusuario;
-- Se inserta a un usuario de prueba.
INSERT INTO usuario
values ('Morro', 'morro@gmail.com', 'morropass');
-- Se inserta una tarjeta de prueba.
INSERT INTO tarjeta (nombreLen, nombreEsp, idusuario)
VALUES ('Tecoani', 'Jaguar', 1);
-- Se inserta una amistad de prueba.
INSERT INTO amigo VALUES (3, 1);
-- Se elimina una tarjeta de prueba
DELETE from tarjeta where idtarjeta = 2;
-- Se elimina a un amigo de prueba.
DELETE from amigo where idusuario = 3 and idamigo = 1;

SELECT idusuario, nombre, avatar from usuario where nombre='Luis' and contraseña='123';

SELECT idusuario, nombre, avatar
FROM (SELECT idamigo FROM (SELECT idusuario FROM usuario
WHERE nombre='Chiquilla' AND contraseña='456')
AS usuario INNER JOIN amigo ON usuario.idusuario = amigo.idusuario)
AS amigos INNER JOIN usuario ON amigos.idamigo = usuario.idusuario;

SELECT idusuario, nombre, avatar FROM usuario
WHERE nombre != 'Chiquilla' and idusuario NOT IN
(SELECT idamigo as idusuario FROM
(SELECT idusuario FROM usuario WHERE nombre='Chiquilla' AND contraseña='456')
AS u INNER JOIN amigo ON u.idusuario = amigo.idusuario);

SELECT idusuario, nombre, avatar FROM (SELECT idamigo FROM
(SELECT idusuario FROM usuario WHERE nombre='Chiquilla' AND contraseña='456')
AS u INNER JOIN amigo ON u.idusuario = amigo.idusuario)
AS a INNER JOIN usuario ON a.idamigo = usuario.idusuario;

INSERT INTO usuario (nombre, correo, contraseña, avatar)
SELECT 'Prueba', 'prueba@gmail.com', contraseña, avatar
FROM usuario WHERE nombre='Luis' and contraseña='123';

DELETE FROM amigo WHERE idamigo = 1 and idusuario IN
(SELECT idusuario FROM usuario
WHERE nombre='Prueba' and contraseña='123');

DELETE FROM amigo WHERE idamigo = 1 and idusuario IN
(SELECT idusuario FROM usuario
WHERE nombre='Prueba' and contraseña='123');

INSERT INTO amigo (idusuario, idamigo)
SELECT idusuario, 1 FROM usuario
where nombre = 'Luis' and contraseña = '123';

UPDATE usuario SET nombre = 'Luis', correo = 'pat@gmail.com',
contraseña = '123', avatar = 'avatar-04.jpg'
WHERE nombre = 'Luis' and contraseña = '123';

UPDATE tarjeta SET nombreLen = 'Malintzi', nombreEsp = 'Malinche', categoria = 'Clima'
WHERE idtarjeta = 1 and idusuario = (SELECT idusuario FROM usuario
WHERE nombre = 'Chiquillo' and contraseña = '123');

SELECT idtarjeta, nombreEsp, nombreLen, categoria, idusuario, nombre, correo, avatar
FROM (SELECT * FROM tarjeta WHERE idusuario = (SELECT idusuario FROM usuario 
WHERE nombre = 'Luis' and contraseña = '123')) AS t INNER JOIN (SELECT idusuario as 
id, nombre, correo, avatar FROM usuario) AS u ON u.id = t.idusuario;