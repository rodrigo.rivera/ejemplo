# Se pueden guardar los comandos en un script.sh y correrlo de la siguiente manera:
# chmod +x script.sh
# ./script.sh

# Para crear la base, cargar el script crearBase.sql en el editor de MySQL,
# o ejecutar el siguiente comando desde terminal:
mysql -u root -p < crearBase.sql

# Para eliminar el usuario y la base por alguna razón:
# mysql -u root -p < eliminarBase.sql