-- Creamos la base de datos
create database if not exists muuch;

-- Indicamos que usaremos la base de datos yumyum para crear las tablas
use muuch;

-- Borramos las siguientes tablas si ya existen en la BD
drop table if exists amigo cascade;
drop table if exists perfil cascade;
drop table if exists contenido cascade;
drop table if exists tarjeta cascade;
drop table if exists usuario cascade;

-- Creación de la tabla Usuario
create table if not exists usuario (
	idusuario serial,
	nombre varchar(32) not null,
	correo varchar(64),
	contraseña varchar(64) not null,
	avatar bigint(20) unsigned,
	primary key (idusuario),
    unique key (nombre)
) engine=InnoDB default charset=utf8;

-- Creación de la tabla Tarjeta
create table if not exists tarjeta (
	idtarjeta serial,
	nombreLen varchar(32) not null,
	nombreEsp varchar(32) not null,
	categoria integer not null,
	idusuario bigint(20) unsigned,
	primary key (idtarjeta),
	foreign key(idusuario)
	references usuario(idusuario)
	ON delete cascade
) engine=InnoDB default charset=utf8;

-- Creación de la tabla Contenido
create table if not exists contenido (
	idcontenido bigint(20) unsigned,
	original longblob not null,
	imagen longblob not null,
	nombreEsp longblob not null,
	nombreLen longblob not null,
	descEsp longblob not null,
	descLen longblob not null,
	primary key (idcontenido),
	foreign key (idcontenido)
	references tarjeta(idtarjeta)
	ON delete cascade
) engine=InnoDB default charset=utf8;

-- Creación de la tabla Perfil
create table if not exists perfil (
	idperfil bigint(20) unsigned,
	imagen longblob not null,
	primary key (idperfil),
	foreign key (idperfil)
	references usuario(idusuario)
	ON delete cascade
) engine=InnoDB default charset=utf8;

-- Creación de la tabla Amistad
create table if not exists amigo (
	idusuario bigint(20) unsigned not null,
	idamigo bigint(20) unsigned not null,
	foreign key (idusuario)
	references usuario(idusuario)
	ON delete cascade,
	foreign key (idamigo)
	references usuario(idusuario)
	ON delete cascade,
	primary key (idusuario, idamigo)
) engine=InnoDB default charset=utf8;

-- Inserciones de usuarios de ejemplo.
insert into usuario values (1, 'Uno', 'uno@gmail.com', '123', 1);
insert into usuario values (2, 'Dos', 'dos@gmail.com', '123', 2);
insert into usuario values (3, 'Tres', 'tres@gmail.com', '123', 3);
insert into usuario values (4, 'Cuatro', 'cuatro@gmail.com', '123', 4);
insert into usuario values (5, 'Cinco', 'cinco@gmail.com', '123', 5);
insert into usuario values (6, 'Seis', 'seis@gmail.com', '123', 6);

-- Inserciones de tarjetas de ejemplo.
insert into tarjeta values (1, '01', 'Uno', 10, 1);
insert into tarjeta values (2, '02', 'Dos', 10, 2);
insert into tarjeta values (3, '03', 'Tres', 10, 3);
insert into tarjeta values (4, '04', 'Cuatro', 10, 4);
insert into tarjeta values (5, '05', 'Cinco', 10, 5);
insert into tarjeta values (6, '06', 'Seis', 10, 6);
insert into tarjeta values (7, '07', 'Siete', 10, 1);
insert into tarjeta values (8, '08', 'Ocho', 10, 2);
insert into tarjeta values (9, '09', 'Nueve', 10, 3);
insert into tarjeta values (10, '10', 'Diez', 10, 4);

insert into tarjeta values (11, '11', 'Once', 10, 5);
insert into tarjeta values (12, '12', 'Doce', 10, 6);
insert into tarjeta values (13, '13', 'Trece', 10, 1);
insert into tarjeta values (14, '14', 'Catorce', 10, 2);
insert into tarjeta values (15, '15', 'Quince', 10, 3);
insert into tarjeta values (16, '16', 'Dieciseis', 10, 4);
insert into tarjeta values (17, '17', 'Diecisiete', 10, 5);
insert into tarjeta values (18, '18', 'Dieciocho', 10, 6);
insert into tarjeta values (19, '19', 'Diecinueve', 10, 1);
insert into tarjeta values (20, '20', 'Veinte', 10, 2);

insert into tarjeta values (21, '21', 'Veintiuno', 10, 3);
insert into tarjeta values (22, '22', 'Veintidos', 10, 4);
insert into tarjeta values (23, '23', 'Veintitres', 10, 5);
insert into tarjeta values (24, '24', 'Veinticuatro', 10, 6);
insert into tarjeta values (25, '25', 'Veinticinco', 10, 1);
insert into tarjeta values (26, '26', 'Veintiseis', 10, 2);
insert into tarjeta values (27, '27', 'Veintisiete', 10, 3);
insert into tarjeta values (28, '28', 'Veintiocho', 10, 4);
insert into tarjeta values (29, '29', 'Veintinueve', 10, 5);
insert into tarjeta values (30, '30', 'Treinta', 10, 6);

insert into tarjeta values (31, '31', 'Treinta y uno', 10, 1);
insert into tarjeta values (32, '32', 'Treinta y dos', 10, 2);
insert into tarjeta values (33, '33', 'Treinta y tres', 10, 3);
insert into tarjeta values (34, '34', 'Treinta y cuatro', 10, 4);
insert into tarjeta values (35, '35', 'Treinta y cinco', 10, 5);
insert into tarjeta values (36, '36', 'Treinta y seis', 10, 6);
insert into tarjeta values (37, '37', 'Treinta y siete', 10, 1);
insert into tarjeta values (38, '38', 'Treinta y ocho', 10, 2);
insert into tarjeta values (39, '39', 'Treinta y nueve', 10, 3);
insert into tarjeta values (40, '40', 'Cuarenta', 10, 4);

insert into tarjeta values (41, '01', 'Uno', 10, 5);
insert into tarjeta values (42, '02', 'Dos', 10, 6);
insert into tarjeta values (43, '03', 'Tres', 10, 1);
insert into tarjeta values (44, '04', 'Cuatro', 10, 2);
insert into tarjeta values (45, '05', 'Cinco', 10, 3);
insert into tarjeta values (46, '06', 'Seis', 10, 4);
insert into tarjeta values (47, '07', 'Siete', 10, 5);
insert into tarjeta values (48, '08', 'Ocho', 10, 6);
insert into tarjeta values (49, '09', 'Nueve', 10, 1);
insert into tarjeta values (50, '10', 'Diez', 10, 2);

insert into tarjeta values (51, '11', 'Once', 10, 3);
insert into tarjeta values (52, '12', 'Doce', 10, 4);
insert into tarjeta values (53, '13', 'Trece', 10, 5);
insert into tarjeta values (54, '14', 'Catorce', 10, 6);
insert into tarjeta values (55, '15', 'Quince', 10, 1);
insert into tarjeta values (56, '16', 'Dieciseis', 10, 2);
insert into tarjeta values (57, '17', 'Diecisiete', 10, 3);
insert into tarjeta values (58, '18', 'Dieciocho', 10, 4);
insert into tarjeta values (59, '19', 'Diecinueve', 10, 5);
insert into tarjeta values (60, '20', 'Veinte', 10, 6);

insert into tarjeta values (61, '21', 'Veintiuno', 10, 1);
insert into tarjeta values (62, '22', 'Veintidos', 10, 2);
insert into tarjeta values (63, '23', 'Veintitres', 10, 3);
insert into tarjeta values (64, '24', 'Veinticuatro', 10, 4);
insert into tarjeta values (65, '25', 'Veinticinco', 10, 5);
insert into tarjeta values (66, '26', 'Veintiseis', 10, 6);
insert into tarjeta values (67, '27', 'Veintisiete', 10, 1);
insert into tarjeta values (68, '28', 'Veintiocho', 10, 2);
insert into tarjeta values (69, '29', 'Veintinueve', 10, 3);
insert into tarjeta values (70, '30', 'Treinta', 10, 4);

insert into tarjeta values (71, '31', 'Treinta y uno', 10, 5);
insert into tarjeta values (72, '32', 'Treinta y dos', 10, 6);
insert into tarjeta values (73, '33', 'Treinta y tres', 10, 1);
insert into tarjeta values (74, '34', 'Treinta y cuatro', 10, 2);
insert into tarjeta values (75, '35', 'Treinta y cinco', 10, 3);
insert into tarjeta values (76, '36', 'Treinta y seis', 10, 4);
insert into tarjeta values (77, '37', 'Treinta y siete', 10, 5);
insert into tarjeta values (78, '38', 'Treinta y ocho', 10, 6);
insert into tarjeta values (79, '39', 'Treinta y nueve', 10, 1);
insert into tarjeta values (80, '40', 'Cuarenta', 10, 2);

-- Creamos el usuario con su contraseña.
CREATE USER if not exists 'loteria'@'localhost' IDENTIFIED BY 'L0T3R1A';
GRANT ALL PRIVILEGES ON muuch.* TO 'loteria'@'localhost';