Instalar node.js que se instala con npm
Desde terminal, dentro del directorio servidor:
Instalar los módulos del package.json la primera vez con: npm install
Levantar el servidor con: npm start

Y entonces estará corriendo el servidor en su respectiva IP, en el puerto 3000
Con el websocket del servidor inicializado, esperando mensajes de clientes.

Con la ruta '/query' para consultas MySQL, esperando que se encuentre levantado
un servicio de MySQL Server con el usuario y base especificados en query.js

Con las siguientes rutas públicas:

'/images/usuario/id-N.jpg' dedicada para almacenar imagenes de usuarios,
donde N es el identificador numérico del usuario.

'/images/tarjeta/id-N.jpg' dedicada para almacenar imagenes de tarjetas,
donde N es el identificador numérico de la tarjeta.

'/audio/len/nombre-N.jpg' dedicada para el audio del nombre original
de las tarjetas, donde N es el identificador numérico de la tarjeta.

'/audio/esp/nombre-N.jpg' dedicada para el audio del nombre en español
de las tarjetas, donde N es el identificador numérico de la tarjeta.

'/audio/len/desc-N.jpg' dedicada para el audio de la descripción original
de las tarjetas, donde N es el identificador numérico de la tarjeta.

'/audio/esp/desc-N.jpg' dedicada para el audio de la descripción en español
de las tarjetas, donde N es el identificador numérico de la tarjeta.
