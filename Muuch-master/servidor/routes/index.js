var express = require('express');
var router = express.Router();
var url = require('url');
/* Variables para conectarse a la base de datos mysql. */
// Se instala con: npm install mysql --save
var mysql = require('mysql');
function consulta(statement, recibe) {
	var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'loteria',
	password : 'L0T3R1A',
	database : 'muuch'
	});
	connection.connect();
	connection.query(statement, recibe);
	connection.end();
}

/* Variables para subir archivos. */
// Se instala con: npm install multer --save
var multer = require('multer');
var updir = './public/';
var muldir = './uploads/';
var upload = multer({ dest: muldir });
var fs = require('fs');

/* Variables para enviar correo. */
// Se instala con: npm install nodemailer --save
var nodemailer = require('nodemailer');
var soporte = {
    user: 'loteria.soporte.app@gmail.com',
    pass: 'l0ter1@123'
}


/* Bienvenida del servidor. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Consulta en la base de datos con parámetros recibidos en peticiones get. */
router.get('/query', function(req, res, next) {
	var data = url.parse(req.url, true).query;
	function devuelve(statement) {
		consulta(statement, function(error, results, fields) {
			if (error) res.json(error)
			else res.json(results)
		})
	};
	var query = data.statement;
	if (query.includes("u-")) { // Requiere nombre y contraseña.
		switch (query) {
			case "u-login":
				devuelve("SELECT idusuario, nombre, correo, avatar from usuario where nombre = "
	        	+ data.name + " and contraseña = " + data.password);
	        	break;
			case "u-id":
				devuelve("SELECT idusuario, nombre, correo, avatar from usuario where idusuario = " + data.iduser);
				break;
			case "u-collected":
				devuelve("SELECT * FROM tarjeta WHERE idusuario = " + data.iduser);
				break;
			case "u-notcollected":
				devuelve("SELECT idtarjeta, nombreEsp, nombreLen, categoria, idusuario, nombre, correo, avatar "
				+ "FROM (SELECT * FROM tarjeta WHERE idusuario IN (SELECT idamigo as idusuario FROM amigo "
				+ "WHERE idusuario = " + data.iduser + ")) AS t INNER JOIN (SELECT idusuario as "
				+ "id, nombre, correo, avatar FROM usuario) AS u ON u.id = t.idusuario");
				break;
			case "u-add":
				devuelve("INSERT INTO usuario (nombre, correo, contraseña, avatar) VALUES (" + data.name 
				+ ", " + data.mail + ", " + data.password + ", " + data.avatar + ")");
				break;
			case "u-update":
				devuelve("UPDATE usuario SET nombre = " + data.newname + ", correo = " + data.mail
				+ ", contraseña = " + data.newpassword + ", avatar = " + data.avatar
				+ " WHERE nombre = " + data.name + " and contraseña = " + data.password);
				break;
			case "u-remove":
				devuelve("DELETE from usuario where nombre = " + data.name + " and contraseña = " + data.password);
				break;
			default: devuelve("SELECT * FROM usuario");
		}
	} else if (query.includes("c-")){ // Para datos de tarjetas.
		switch (query) {
			case "c-id":
				devuelve("SELECT * from tarjeta where idtarjeta=" + data.idcard);
				break;
			case "c-category":
				devuelve("SELECT idtarjeta, nombreEsp, nombreLen, categoria, idusuario, nombre, correo, avatar "
				+ "FROM (SELECT * FROM tarjeta WHERE categoria = " + data.category + ") AS t INNER JOIN (SELECT idusuario as "
				+ "id, nombre, correo, avatar FROM usuario) AS u ON u.id = t.idusuario ");
				break;
			case "c-add":
				devuelve("INSERT INTO tarjeta (nombreLen, nombreEsp, categoria, idusuario) "
				+ "SELECT " + data.original + ", " + data.spanish + ", " + data.category + ", idusuario "
				+ "FROM usuario where nombre = " + data.name + " and contraseña = " + data.password);
				break;
			case "c-update":
				devuelve("UPDATE tarjeta SET nombreLen = " + data.original + ", nombreEsp = " + data.spanish
				+ ", categoria = " + data.category + " WHERE idtarjeta = " + data.idcard + " and idusuario = "
				+ "(SELECT idusuario FROM usuario WHERE nombre = " + data.name + " and contraseña = " + data.password + ")");
				break;
			case "c-remove":
	            devuelve("DELETE FROM tarjeta WHERE idtarjeta=" + data.idcard + " and idusuario IN "
	            + "(SELECT idusuario FROM usuario "
	            + "where nombre = " + data.name + " and contraseña = " + data.password + ")");
	            break;
			default: devuelve("SELECT * FROM tarjeta");
		}
	} else { // Para datos de amigos.
		switch (query) {
			case "a-added":
	            devuelve("SELECT idusuario, nombre, correo, avatar FROM (SELECT idamigo FROM amigo "
            	+ "WHERE idusuario = " + data.iduser + ") AS a INNER JOIN usuario ON a.idamigo = usuario.idusuario");
	            break;
			case "a-notadded":
	            devuelve("SELECT idusuario, nombre, correo, avatar FROM usuario WHERE idusuario != " + data.iduser 
            	+ " and idusuario NOT IN (SELECT idamigo as idusuario FROM (SELECT idamigo FROM amigo WHERE idusuario = " + data.iduser + ") as amigos)");
	            break;
			case "a-add":
				devuelve("INSERT INTO amigo (idusuario, idamigo) "
				+ "SELECT idusuario, " + data.idfriend + " FROM usuario "
				+ "where nombre = " + data.name + " and contraseña = " + data.password);
				break;
			case "a-remove":
	            devuelve("DELETE FROM amigo WHERE idamigo = " + data.idfriend + " and idusuario IN "
	            + "(SELECT idusuario FROM usuario where nombre = " + data.name + " and contraseña = " + data.password + ")");
	            break;
			default: devuelve("SELECT * FROM amigo");
		}
	}
});

/* Consulta para obtener la lista de todos los usuarios. */
router.get('/recovery', function(req, res, next) {
	var data = url.parse(req.url, true).query;
	function recibe(usuario) {
		var destino = (data.origen > 0)? usuario.correo: soporte.user;
		if (destino.length > 0) {
			var transporter = nodemailer.createTransport({service: 'gmail',auth: soporte});
			var mailOptions = {
			  from: soporte.user,
			  to: destino,
			  subject: 'Recuperación de cuenta Muuch',
			  text: '¡Hola ' + usuario.nombre + '!'
			  + '\nTu contraseña es: ' + usuario.contraseña
			  + '\n\nSi no pediste tu contraseña desde la app, te recomendamos cambiarla.'
			  + '\n\nSi tienes alguna duda, puedes responder a este correo.'
			};
			transporter.sendMail(mailOptions, function(error, info){
			  if (error) res.send(error);
			  else res.send('Correo enviado: ' + info.response);
			});
		} else {
			function agrega(tarjetas) {
				consulta("SELECT idusuario, nombre, correo, avatar "
				+ "FROM (SELECT idamigo as id FROM amigo WHERE idusuario = " + usuario.idusuario + ") as amigos "
				+ "INNER JOIN (SELECT * FROM usuario) as u ON u.idusuario = amigos.id",
				function(error, amigos, fields){
					if (error) res.json(error)
					else res.json([{"idusuario":usuario.idusuario,"nombre":usuario.nombre,
					"correo":usuario.correo,"avatar":usuario.avatar}, amigos, tarjetas]);
				});
			}
			consulta("SELECT * FROM tarjeta WHERE idusuario = " + usuario.idusuario,
				function(error, results, fields){
					if (error) res.json(error)
					else agrega(results)
				});
		}
	};
	consulta("SELECT * FROM usuario WHERE nombre = " + data.name,
		function (error, results, fields) {
			if (!error) {
				if (results.length > 0) recibe(results[0])
				else res.json(results)
			} else res.json(error)
		});
});

/* Accept an array of files, all with the name fieldname. Optionally error out if
 * more than maxCount files are uploaded. The array of files will be stored in req.files
 */
router.post('/subir', upload.array('archivo', 6), function(req, res, next) {
	//upload.single('archivo'): res.send(guardar(req.file.filename, req.file.originalname, "images/usuario/"));
	var names = "", subdir = "";
    for(var x=0; x<req.files.length; x++) {
        //copiamos el archivo a la carpeta definitiva de fotos
        var fname = req.files[x].filename, oname = req.files[x].originalname;
        if (oname.includes("tarjeta")) { subdir = "images/tarjeta/";
        } else if (oname.includes("nombre-esp")) { subdir = "audio/esp/";
        } else if (oname.includes("nombre-len")) { subdir = "audio/len/";
        } else if (oname.includes("desc-esp")) { subdir = "audio/esp/";
        } else if (oname.includes("desc-len")) subdir = "audio/len/";
        names = names + guardar(req.files[x].filename, req.files[x].originalname, subdir) + " ";
    } res.send(names);
});

router.get('/download', function (req, res, next) {
	var data = url.parse(req.url, true).query;
    res.download("public/" + data.filePath);
});

function guardar(fname, oname, subdir) {
	fs.rename(muldir + fname, updir + subdir + oname, function(error){
		if (error) console.log(error);
		else console.log(oname);
	});
	//fs.createReadStream(muldir + fname).pipe(fs.createWriteStream(updir + subdir + oname));
	//fs.unlink(muldir + fname);
	return fname;
}

module.exports = router;