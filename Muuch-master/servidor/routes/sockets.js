var io = require('socket.io');
exports.initialize = function(server) {

	io = io.listen(server);
	var usuarios = [];

	//io = io.listen(server);

	io.sockets.on("connection", function(socket){

		/*necesario
		Activity en el cual se usa es en "inicio".
		Metodo que se ejecuta una vez que el usuario ingreso a la pagina inicial de Muuch
		Recibe como parametro el id del usuario, el cual se utilizara para guardarlo en
		la lista de usuarios conectados.
		*/
		socket.on('Conectado', function(msg){
			for(var i = 0; i < usuarios.length; i++){
				if(usuarios[i].idCorreo == msg){
					console.log("el usuario se "+msg+" se trato de reconectar :(");
					return;
				}
			}
			var clienteInfo = new Object();
			clienteInfo.idCorreo = msg;
			clienteInfo.idPersona = socket.id;
			clienteInfo.bJugando = false;
			usuarios.push(clienteInfo);
			console.log("el usuario: "+msg+" se conecto.");
		});

		/*necesario
		Funcion que recibe las tarjetas de los usuarios y las envia al admin de la partida
		El parametro tiene la siguiente informacion: idAdmin-idUsuarioQueEnvioTarjetas-idDeLasTarjetas
		msg = idAdmin-idUser-listaTarjetasSeleccionadasPorUser
		*/
		socket.on('aceptoInvitacion', function(msg){
			var usuarioId = socket.id;
			var sep = msg.split("-");
			for(var i = 0; i< usuarios.length; i++){
				if(sep[0] == usuarios[i].idCorreo){
					console.log("Se acepta la invitacion para jugar por parte de: "+sep[1]+" se envia al "+
					"admin con id: "+sep[0]+ " sldmsd "+sep[2]);
						//idUserSocket-idUser-listaTarjetasSeleccionadasPorUser
						io.to(usuarios[i].idPersona).emit('usuarioAcepto', usuarioId+"-"+sep[1]+"-"+sep[2]);
				}
			}
		});

		/*necesario
		funcion que recibe los usuarios, el idioma, nivel y numero de jugadores de la
		partida.
		El mensaje de entrada es de la forma 1,2,3,4-primerIdioma, facil, 4
		La funcion envia el id del admin, el idioma, nivel y numero de jugadores de la
		partida
		El mensaje de salida es de la forma: adminId,idioma,nivel,numero de jugadores
		msg = jugadoresInvitados-idioma,nivel,jugadores
		ejemplo = 1,2,3,4-español,facil,2
		*/
		socket.on('invitoJugadores', function(msg){
			var adminId = socket.id;
			var jug = msg;
			var arrJug = jug.split("-");
			var jugadores = arrJug[0].split(",");
			var opciones = arrJug[1].split(",");
			var idAdmin;
			for(var i = 0; i<usuarios.length; i++){
				if(usuarios[i].idPersona==adminId){
					idAdmin = usuarios[i].idCorreo;
				}
			}
			/*El primer for recorre los jugadores que el admin envio. El segundo for
			recorre todos los usuarios conectados y si encuentra una coincidencia le envia
			mensaje*/
			for(var i = 0; i<jugadores.length; i++){
				for(var j = 0; j<usuarios.length; j++){
					if(jugadores[i].trim() == usuarios[j].idCorreo){
						//idAdmin,idioma,nivel,jugadores,categoria
						usuarios[j].bJugando = true;
						io.to(usuarios[j].idPersona).emit('LosJugadoresInvitados', idAdmin+","+opciones[0]+","+opciones[1]+","+opciones[2]+","+opciones[3]);
						console.log("Se invita a el jugador: "+usuarios[j].idCorreo);
					}
				}
			}
		});

		/*necesario
		Activity en el cual se usa es "ArmaLoteriaAdmin"
		Funcion que sirve para mandar al administrador de la partida todos los usuarios
		conectados y disponibles para jugar.
		No tiene parametros.
		*/
		socket.on('ObtieneJugadores', function(){
			var adminId = socket.id;
 			var jugadores = "";
			var userTemp = [];
			/*Se cambia el valor del administrador de activo para jugar a inactivo
			*/
			/*guarda en una lista temporal los usuarios disponibles para jugar excepto al admin de la
			partida*/
			for(var i = 0; i<usuarios.length; i++){
				if(usuarios[i].idPersona != adminId){
					if(usuarios[i].bJugando==false){
							userTemp.push(usuarios[i].idCorreo);
					}
				}else{
					usuarios[i].bJugando=true;
				}
			}
			/*Se envia al admin de la partida los jugadores disponibles para jugar*/
			var seEnvia = userTemp.toString();
			io.to(adminId).emit('LosUsuariosConectados', seEnvia);
			console.log("se envian los jugadores al administrador de la partida "+ seEnvia);
		});

		/*	necesario
		*	Envia la carta a el jugador que esta en el msg de entrada. Junto a el id del usuarios
		* tambien esta la carta en formato [1,2,3,4,5]. Todo el mensaje esta en formato
		* idAdmin-idUser-[1,2,3,4,5]	donde id es el id del usuario.
		*/
		socket.on('envioCarta', function(msg){
			var idCarta = msg.split("-");
			var envio = idCarta[0]+"-"+idCarta[2];
			console.log('La carta que se envia es: '+msg);
			for(var i = 0; i<usuarios.length; i++){
				if(idCarta[1]==usuarios[i].idCorreo){
					console.log('La carta que se envia es: '+i+" "+msg);
					io.to(usuarios[i].idPersona).emit('envioCartaUser', envio);
				}
			}
		});

		/*	necesario
		*	Envia al administrador de la partida que recibio correctamente la carta
		*	idAdmin-idUserAQueEnvio
		*/
		socket.on('cartaRecibida', function(msg){
			var mensLleg = msg.split("-");
			console.log('mensaje al usuario carta recibida p'+mensLleg[0]+" tamaño:"+usuarios.length);
			for(var i = 0; i<usuarios.length; i++){
				if(mensLleg[0] == usuarios[i].idCorreo){
					console.log('mensaje al usuario carta recibida '+ i +" tamaño:"+usuarios.length);
					io.to(usuarios[i].idPersona).emit('cartaRecibidaCorrectamente', mensLleg[1]);
				}
			}
		});

		/*	necesario
		*	Recibe los id de las personas a las que le llego la carta y se les envia el
		*	mensaje que todos la recibieron y pueden avanzar
		*/
		socket.on('todasCartasRecibidas', function(msg){
			var mensLleg = msg.split(",");

			for(var i = 0; i<mensLleg.length; i++){
				for(var j = 0; j<usuarios.length; j++){
					if( mensLleg[i]==usuarios[j].idCorreo ){
						console.log('mensaje al usuario todas las cartas recibidas '+msg);
						io.to(usuarios[j].idPersona).emit('iniciarPartida', "listo");

					}
				}
			}
		});

		/**
		*	LLega la primer tarjeta y esta se envia al usuario que esta jugando
		*/
		socket.on('envioPrimeraTarjeta', function(msg){
			var mensLleg = msg.split(",");
			var listaJugadores = mensLleg[0].split("-");
			for(var i = 0; i<listaJugadores.length; i++){
				for(var j = 0; j<usuarios.length; j++){
					if(listaJugadores[i]==usuarios[j].idCorreo){
						io.to(usuarios[j].idPersona).emit('envioPrimeraTarjetaU', mensLleg[1]);
					}
				}
			}
		});

		socket.on('terminarPartida', function(msg){
			var mensLleg = msg.split("-");
			for(var i = 0; i<mensLleg.length; i++){
				for(var j = 0; j<usuarios.length; j++){
					if(mensLleg[i]==usuarios[j].idCorreo){
						usuarios[j].bJugando = false;
						io.to(usuarios[j].idPersona).emit('terminarPartidaUsuario', "Terminar");
					}
				}
			}
		});

		/*
		*Metodo que funciona para avisar al admin de la partida que un jugador ya gano
		*	IdDelGanador-idDelAdmin
		*/
		socket.on('ganador', function(msg){
			var mensLleg = msg.split("-");
			var idGanador = mensLleg[0];
			var idAdmin = mensLleg[1];
			for(var i = 0; i<usuarios.length; i++){
				if(usuarios[i].idCorreo == idAdmin){
						console.log('Gano el usuario con id '+idGanador+' se envia mensaje al admin con id '+ idAdmin);
						io.to(usuarios[i].idPersona).emit('mensajeAdminGanador', idGanador);
				}
			}
		});

		socket.on('ganoAdmin', function(msg){
			var mensLleg = msg.split("-");
			for(var i = 0; i<mensLleg.length; i++){
				for(var j = 0; j<usuarios.length; j++){
					if(mensLleg[i] == usuarios[j].idCorreo){
						console.log('Gano el admin id del usuario perdedor: ');
						io.to(usuarios[j].idPersona).emit('ganoElAdmin', "jajaja pardiste");
					}
				}
			}
		});

		/*
		*	Funcion que le envia a los usuarios que perdieron la partida el idDelGanador,
		*	al mismo tiempo avisa que perdio.
		*/
		socket.on('jugadoresPerdedores', function(msg){
			var mensLleg = msg.split("-");
			var idPerdedor = mensLleg[0];
			var idGanador = mensLleg[1];
			for(var i = 0; i<usuarios.length; i++){
				if(usuarios[i].idCorreo==idPerdedor.trim()){
					console.log('id ganador: '+idGanador+' id perdedor:'+idPerdedor);
					io.to(usuarios[i].idPersona).emit('mensajeParaPerdedor', idGanador);
				}
			}
		});

		socket.on('disconnect', function () {
			var usuarioDesconectado;
			for(var i = 0; i<usuarios.length; i++){
				if(socket.id == usuarios[i].idPersona){
					usuarioDesconectado = usuarios[i].idCorreo;
					usuarios.splice(i, 1);
				}
			}
			console.log('El usuario '+usuarioDesconectado+' se desconecto.');

			console.log('Los usuarios que siguen conectados son:' + "-tamaño:"+usuarios.length);
			for(var i = 0; i<usuarios.length; i++){
				console.log(usuarios[i].idCorreo);
			}
		});

	});

};
